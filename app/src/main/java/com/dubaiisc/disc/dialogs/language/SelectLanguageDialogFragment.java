package com.dubaiisc.disc.dialogs.language;

import android.content.Intent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.dubaiisc.disc.R;
import simplifii.framework.fragments.BaseDialogFragment;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class SelectLanguageDialogFragment extends BaseDialogFragment {

    private RadioGroup radioLanguageGroup;

    public static SelectLanguageDialogFragment getInstance() {
        SelectLanguageDialogFragment selectLanguageDialogFragment = new SelectLanguageDialogFragment();
        return selectLanguageDialogFragment;
    }

    @Override
    protected int getViewID() {
        return R.layout.fragment_select_language;
    }

    @Override
    protected void initViews() {
        radioLanguageGroup = (RadioGroup) findView(R.id.radioLanguage);
        setOnClickListener(R.id.tv_cancel, R.id.tv_ok);
        setAutoSelectedLanguage();
    }

    private void setAutoSelectedLanguage() {
        String persistedData = LocalHelperUtility.getLanguage();
        RadioButton radioButtonEnglish = (RadioButton) findView(R.id.radioEnglish);
        RadioButton radioButtonArabic = (RadioButton) findView(R.id.radioArabic);
        RadioButton radioButtonFrench = (RadioButton) findView(R.id.radioFrench);
        RadioButton radioButtonSpenish = (RadioButton) findView(R.id.radioSpanish);
            switch (persistedData){
                case FrameworkConstants.LANGUAGE.EN:
                    radioButtonEnglish.setChecked(true);
                    return;
                case FrameworkConstants.LANGUAGE.AR:
                    radioButtonArabic.setChecked(true);
                    return;
                case FrameworkConstants.LANGUAGE.FR:
                    radioButtonFrench.setChecked(true);
                    return;
                case FrameworkConstants.LANGUAGE.ES:
                    radioButtonSpenish.setChecked(true);
                    return;
            }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_ok:
                getCheckedButton();
                break;
            case R.id.tv_cancel:
                getDialog().dismiss();
                break;
        }
    }

    private void getCheckedButton() {
        int selectedId = radioLanguageGroup.getCheckedRadioButtonId();
        String seletedLanguage = "";
        switch (selectedId){
            case R.id.radioEnglish:
                seletedLanguage = FrameworkConstants.LANGUAGE.EN;
                break;
            case R.id.radioArabic:
                seletedLanguage = FrameworkConstants.LANGUAGE.AR;
                break;
            case R.id.radioFrench:
                seletedLanguage = FrameworkConstants.LANGUAGE.FR;
                break;
            case R.id.radioSpanish:
                seletedLanguage = FrameworkConstants.LANGUAGE.ES;
                break;
        }
        if (!seletedLanguage.equalsIgnoreCase(LocalHelperUtility.getLanguage())){
            LocalHelperUtility.setLocale(getActivity(), seletedLanguage);
            //Need to restart the app again ......
//        recreate();
            restartApp();
        }else {
            getDialog().dismiss();
        }
    }
    private void restartApp() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }
}
