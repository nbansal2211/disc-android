package com.dubaiisc.disc.dialogs.gender;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.model.agenda.GenderModel;

import java.util.ArrayList;
import java.util.List;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.itemDecorators.CustomDividerItemDecorator;

public class GenderDialogFragment extends DialogFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private int title;
    private BaseRecycleAdapter.RecyclerClickInterface clickInterface;
    private TextView dialogTitle;
    private RecyclerView mRecyclerView;
    private Activity activity;
    private List<GenderModel> list;
    private int viewType;

    public static GenderDialogFragment newInstance(BaseRecycleAdapter.RecyclerClickInterface clickInterface,
                                                   ArrayList<GenderModel> list, int viewType, @StringRes int title) {

        GenderDialogFragment fragment = new GenderDialogFragment();
        fragment.clickInterface = clickInterface;
        fragment.title = title;
        fragment.viewType = viewType;
        fragment.list = list;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        activity = getActivity();
        return inflater.inflate(R.layout.dialog_gender, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.rv_list);
        dialogTitle = view.findViewById(R.id.tv_title);

        CustomDividerItemDecorator divider = new CustomDividerItemDecorator(activity, R.drawable.divider);
        mRecyclerView.addItemDecoration(divider);

        DialogListAdapter<GenderModel> adapter = new DialogListAdapter<>(activity, list, viewType);
        mRecyclerView.setAdapter(adapter);
        adapter.setClickInterface(clickInterface);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));

        dialogTitle.setText(title);
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        clickInterface.onItemClick(itemView, position, obj, actionType);
    }
}
