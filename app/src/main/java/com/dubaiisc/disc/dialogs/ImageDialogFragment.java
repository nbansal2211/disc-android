package com.dubaiisc.disc.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.gallery.Item;

import java.util.List;

public class ImageDialogFragment extends Dialog {

    public ImageDialogFragment(Context context, int pos, List<Item> newList) {
        super(context, R.style.MyAlertDialogStyle);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_images);
    }


}
