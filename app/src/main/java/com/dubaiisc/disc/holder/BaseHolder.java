package com.dubaiisc.disc.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.io.Serializable;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;


/**
 * Created by kartikeya on 4/8/17.
 */

public class BaseHolder extends RecyclerView.ViewHolder implements Serializable {

    protected Context context;
    protected BaseRecycleAdapter.RecyclerClickInterface clickListener;
    private int position;

    public BaseHolder(View itemView) {
        super(itemView);
        context = itemView.getContext();
    }

    public BaseRecycleAdapter.RecyclerClickInterface getClickListener() {
        return clickListener;
    }

    public void setClickListener(BaseRecycleAdapter.RecyclerClickInterface clickListener) {
        this.clickListener = clickListener;
    }

    protected void sendClickEvent(int actionType, Object model) {
        if (clickListener != null) {
            clickListener.onItemClick(itemView,getAdapterPosition(), model, actionType);
        }
    }

    protected View findView(int id) {
        return itemView.findViewById(id);
    }

    protected TextView findTv(int id) {
        return (TextView) findView(id);
    }

    public void onBind(int position, Object obj) {
        this.position = position;
    }

    protected void setTextOrGone(TextView tv, String text){
        if(!TextUtils.isEmpty(text)){
            tv.setVisibility(View.VISIBLE);
            tv.setText(text);
        }else{
            tv.setVisibility(View.GONE);
        }
    }

    protected void setTextOrInvisible(TextView tv, String text){
        if(!TextUtils.isEmpty(text)){
            tv.setVisibility(View.VISIBLE);
            tv.setText(text);
        }else{
            tv.setVisibility(View.INVISIBLE);
        }
    }

}
