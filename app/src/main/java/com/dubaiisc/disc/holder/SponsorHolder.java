package com.dubaiisc.disc.holder;

import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.dubaiisc.disc.model.sponsors.SponsorModel;
import com.dubaiisc.disc.utilities.AppConstants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.AnimUtils;

public class SponsorHolder extends BaseHolder {

    public SponsorHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);

        final SponsorModel model = (SponsorModel) obj;
        final ImageView sponsorThumb = itemView.findViewById(R.id.iv_sponsorThumb);
        Picasso.get().load(model.getImageUrl()).into(sponsorThumb, new Callback() {
            @Override
            public void onSuccess() {
                AnimUtils.fadeIn(500, 0, new DecelerateInterpolator(), sponsorThumb);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_SPONSOR, model);
            }
        });
    }
}
