package com.dubaiisc.disc.holder.country;

import android.view.View;
import android.widget.TextView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.holder.BaseHolder;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class CountryIsdHolder extends BaseHolder {

    private final TextView country;

    public CountryIsdHolder(View itemView) {
        super(itemView);
        country = findTv(R.id.tv_country);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        CountryModel model = (CountryModel) obj;

        //TODO get ISD
        StringBuilder text = new StringBuilder();
        text.append("(+").append(model.getIsd() == null ? 91 : model.getIsd()).append(")").append(" ").append(model.getName());
        country.setText(text);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_COUNTRY_ISD, obj);
            }
        });
    }
}
