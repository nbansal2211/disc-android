package com.dubaiisc.disc.holder;

import android.view.View;
import com.dubaiisc.disc.model.notification.NotificationModel;

import com.dubaiisc.disc.R;
import simplifii.framework.widgets.CustomFontTextView;

public class NotificationHolder extends BaseHolder {

    private CustomFontTextView date;
    private CustomFontTextView notification;

    public NotificationHolder(View itemView) {
        super(itemView);

        date = itemView.findViewById(R.id.tv_date);
        notification = itemView.findViewById(R.id.tv_notification);

    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        NotificationModel model = (NotificationModel) obj;

        date.setText(model.getTitle());
        notification.setText(model.getDescription());
    }
}
