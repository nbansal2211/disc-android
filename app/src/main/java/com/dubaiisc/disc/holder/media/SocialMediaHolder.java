package com.dubaiisc.disc.holder.media;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubaiisc.disc.holder.BaseHolder;
import com.dubaiisc.disc.model.media.FeedModel;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;

public class SocialMediaHolder extends BaseHolder {

    private ImageView mediaThumb;
    private TextView mediaTitle;
    private TextView mediaDescription;
    private TextView mediaTimeElapsed;

    public SocialMediaHolder(View itemView) {
        super(itemView);
        mediaThumb = (ImageView) findView(R.id.iv_mediaThumb);
        mediaTitle = (TextView) findView(R.id.tv_mediaTitle);
        mediaDescription = (TextView) findView(R.id.tv_mediaDescription);
        mediaTimeElapsed = (TextView) findView(R.id.tv_mediaTimeElapsed);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        final FeedModel model = (FeedModel) obj;
        Picasso.get().load(model.getImageUrl()).placeholder(R.drawable.ic_image_loading).fit().centerCrop().into(mediaThumb);
        mediaTitle.setText(model.getTitle());
        mediaDescription.setText(model.getShortText());
        mediaTimeElapsed.setText(model.getDate());
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_MEDIA, model);
            }
        });
    }
}
