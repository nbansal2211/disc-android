package com.dubaiisc.disc.holder;

import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.model.speakers.SpeakerModel;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.widgets.CustomFontTextView;

public class SpeakerHolder extends BaseHolder {

    private ImageView thumb;
    private CustomFontTextView name, designation;


    public SpeakerHolder(View itemView) {
        super(itemView);
        thumb = (ImageView) findView(R.id.iv_speakerThumb);
        name = (CustomFontTextView) findView(R.id.tv_speakerName);
        designation = (CustomFontTextView) findView(R.id.tv_speakerDesignation);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        final SpeakerModel speakerModel = (SpeakerModel) obj;

        Picasso.get().load(speakerModel.getImageURL()).fit().into(thumb);
        name.setText(speakerModel.getName());
        designation.setText(speakerModel.getDesignation());
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_SPEAKER, speakerModel);
            }
        });
    }
}
