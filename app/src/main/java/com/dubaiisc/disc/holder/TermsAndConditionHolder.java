package com.dubaiisc.disc.holder;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.model.termsAndConditions.TermsAndConditionModel;

import java.util.List;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.widgets.CustomFontTextView;

public class TermsAndConditionHolder extends BaseHolder {

    private CustomFontTextView tvTitle, tvDesc;
    private LinearLayout lnrLytSubDesc;

    public TermsAndConditionHolder(View itemView) {
        super(itemView);
        tvTitle = (CustomFontTextView) findView(R.id.tv_title);
        tvDesc = (CustomFontTextView) findView(R.id.tv_desc);
        lnrLytSubDesc = (LinearLayout) findView(R.id.lnrLyt_subDesc);

    }


    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        TermsAndConditionModel termsAndConditionModel = (TermsAndConditionModel) obj;
        tvTitle.setText(termsAndConditionModel.getTitle());
        tvDesc.setText(termsAndConditionModel.getDesc());
        if (CollectionUtils.isNotEmpty(termsAndConditionModel.getSubDesc()))
            onBindSubDescForChildItem(termsAndConditionModel.getSubDesc());
        else onRemoveBindDataForChildItems();
    }

    private void onBindSubDescForChildItem(List<String> subDesc) {
        if (lnrLytSubDesc.getChildCount() > 0) {
            lnrLytSubDesc.removeAllViews();
        }

        for (int i = 0; i < subDesc.size(); i++) {
            View childItemView = LayoutInflater.from(context).inflate(R.layout.row_bullet_text, lnrLytSubDesc, false);
            ((CustomFontTextView) childItemView.findViewById(R.id.tv_text)).setPaddingRelative(0, 5, 0, 0);
            ((CustomFontTextView) childItemView.findViewById(R.id.tv_text)).setText(subDesc.get(i));
            lnrLytSubDesc.addView(childItemView);
        }

    }

    private void onRemoveBindDataForChildItems() {
        //clear previous view from layout....
        if (lnrLytSubDesc.getChildCount() > 0) {
            lnrLytSubDesc.removeAllViews();
        }
    }
}
