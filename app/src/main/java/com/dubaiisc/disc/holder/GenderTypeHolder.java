package com.dubaiisc.disc.holder;

import android.view.View;
import android.widget.TextView;
import com.dubaiisc.disc.model.agenda.GenderModel;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;

public class GenderTypeHolder extends BaseHolder {

    private TextView gender;

    public GenderTypeHolder(View itemView) {
        super(itemView);
        gender = (TextView) findView(R.id.tv_generic);
    }


    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        GenderModel genderModel = (GenderModel) obj;
        gender.setText(genderModel.getGenderText());
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_GENDER,obj);
            }
        });
    }
}
