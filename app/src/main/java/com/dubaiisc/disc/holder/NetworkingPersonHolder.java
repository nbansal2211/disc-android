package com.dubaiisc.disc.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubaiisc.disc.model.networking.NetworkingPersonModel;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;

public class NetworkingPersonHolder extends BaseHolder {

    private ImageView personThumb;
    private TextView personName;
    private TextView personDesignation;
    private TextView personCompany;
    private TextView personCityCountry;

    public NetworkingPersonHolder(View itemView) {
        super(itemView);
        personThumb = (ImageView) findView(R.id.iv_personThumb);
        personName = (TextView) findView(R.id.tv_personName);
        personDesignation = (TextView) findView(R.id.tv_personDesignation);
        personCompany = (TextView) findView(R.id.tv_personCompany);
        personCityCountry = (TextView) findView(R.id.tv_personCityCountry);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);

        setHolderData((NetworkingPersonModel) obj);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_PERSON, obj);
            }
        });
    }

    private void setHolderData(NetworkingPersonModel model) {
        StringBuilder fullName = new StringBuilder(model.getFirstName());
        fullName.append(" ").append(TextUtils.isEmpty(model.getMiddleNamee()) ? "" : model.getMiddleNamee());
        fullName.append(" ").append(TextUtils.isEmpty(model.getLastName()) ? "" : model.getLastName());

        StringBuilder cityCountry = new StringBuilder();
        cityCountry.append(model.getCity()).append(", ").append(model.getCountry());

        if (TextUtils.isEmpty(model.getProfilePicLink()))
            Picasso.get().load(R.drawable.ic_default_profile_picture).placeholder(R.drawable.ic_default_profile_picture).into(personThumb);
        else
            Picasso.get().load(model.getProfilePicLink()).placeholder(R.drawable.ic_default_profile_picture).fit().into(personThumb);

        personName.setText(fullName);
        personDesignation.setText(model.getDesignation());
        personCompany.setText(model.getOrganization());
        personCityCountry.setText(cityCountry);
    }
}
