package com.dubaiisc.disc.holder;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewModel;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.StringUtility;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;
import simplifii.framework.widgets.CustomFontTextView;

import static com.dubaiisc.disc.adapter.BaseRecycleAdapter.selectItem;

public class GalleryEventHolder extends BaseHolder {

    private CustomFontTextView txt_event;
    private View line_active;
    private LinearLayout main;

    public GalleryEventHolder(View itemView) {
        super(itemView);
        txt_event = itemView.findViewById(R.id.txt_event);
        line_active = itemView.findViewById(R.id.line_active);
        main = itemView.findViewById(R.id.main);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final YearsInReviewModel model = (YearsInReviewModel) obj;
        onBindModelToViews(position, model);
    }

    private void onBindModelToViews(final int position, final YearsInReviewModel model) {
        if (LocalHelperUtility.getLanguage().equalsIgnoreCase(FrameworkConstants.LANGUAGE.EN)
                || LocalHelperUtility.getLanguage().equalsIgnoreCase(FrameworkConstants.LANGUAGE.AR)) {
            Spanned spanned = Html.fromHtml(model.getEditionTranslation());
            String s = spanned.toString();
            SpannableStringBuilder editionText = null;
            if (s.length() > 0) {
                if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0)))) {
                    editionText = StringUtility.getSuperScriptSupportedString(s, 1, 3);
                }
                if (s.length() > 1) {
                    if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(1)))) {
                        editionText = StringUtility.getSuperScriptSupportedString(s, 2, 4);
                    }
                }
                if (s.length() > 2) {
                    if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(1))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(2)))) {
                        editionText = StringUtility.getSuperScriptSupportedString(s, 3, 5);
                    }
                }
            }
            if (editionText != null) {
                txt_event.setText(editionText.append(" "));
            }
        } else {
            txt_event.setText(Html.fromHtml(model.getEditionTranslation()));
        }
        line_active.setVisibility(selectItem == position ? View.VISIBLE : View.GONE);
        txt_event.setTextColor(selectItem == position ? context.getResources().getColor(R.color.colorAccent_blue) : context.getResources().getColor(R.color.dark_gray));
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectItem = position;
                sendClickEvent(AppConstants.ACTION_TYPE.GALLERY_ACTION, model);
            }
        });

    }


}
