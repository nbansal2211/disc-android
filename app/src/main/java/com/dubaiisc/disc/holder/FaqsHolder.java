package com.dubaiisc.disc.holder;

import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.model.faqs.QuesAnsModel;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.AnimUtils;
import simplifii.framework.widgets.CustomFontTextView;

public class FaqsHolder extends BaseHolder {

    private ConstraintLayout row;
    private CustomFontTextView ques;
    private CustomFontTextView ans;
    private ImageView indicator;

    public FaqsHolder(View itemView) {
        super(itemView);
        row = (ConstraintLayout) findView(R.id.cl_row);
        ques = (CustomFontTextView) findView(R.id.tv_ques);
        ans = (CustomFontTextView) findView(R.id.tv_ans);
        indicator = (ImageView) findView(R.id.iv_indicator);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);

        final QuesAnsModel model = (QuesAnsModel) obj;
        ques.setText(model.getQues());
        ans.setText(model.getAns());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shouldExpandOrCollapse(model);
            }
        });
    }

    private void shouldExpandOrCollapse(QuesAnsModel model) {
        if (model.isExapanded()) {
            indicator.setImageResource(R.drawable.ic_plus);
            AnimUtils.slideUpClose(ans);
            model.setExapanded(false);
        } else {
            indicator.setImageResource(R.drawable.ic_minus);
            AnimUtils.slideDownOpen(ans);
            model.setExapanded(true);
        }
    }
}