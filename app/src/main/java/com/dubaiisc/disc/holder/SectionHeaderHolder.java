package com.dubaiisc.disc.holder;

import android.view.View;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.SectionHeaderModel;

import simplifii.framework.widgets.CustomFontTextView;

public class SectionHeaderHolder extends BaseHolder {

    public SectionHeaderHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);

        SectionHeaderModel model = (SectionHeaderModel) obj;
        CustomFontTextView sectionHeader = itemView.findViewById(R.id.tv_sectionHeader);
        sectionHeader.setText(model.getTitle());
    }
}
