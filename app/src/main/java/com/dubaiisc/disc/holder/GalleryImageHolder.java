package com.dubaiisc.disc.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.gallery.Item;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.ThumbnailExtractAsyncNewTask;
import com.squareup.picasso.Picasso;

public class GalleryImageHolder extends BaseHolder {

    private ImageView img_gallery;
    private ImageView iv_event_media_play;
    private RelativeLayout main;

    public GalleryImageHolder(View itemView) {
        super(itemView);
        img_gallery = itemView.findViewById(R.id.img_gallery);
        main = itemView.findViewById(R.id.main);
        iv_event_media_play = itemView.findViewById(R.id.iv_event_media_play);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final Item model = (Item) obj;
        onBindModelToViews(position, model);
    }

    private void onBindModelToViews(final int position, final Item model) {
        main.setVisibility(View.VISIBLE);
        if (!TextUtils.isEmpty(model.getUrl())) {
            if (model.getType().toUpperCase().equalsIgnoreCase("IMAGE")) {
                iv_event_media_play.setVisibility(View.GONE);
                Picasso.get().load(model.getUrl() != null ? model.getUrl() : "").placeholder(R.drawable.ic_thumb).fit().into(img_gallery);
            } else if (model.getType().toUpperCase().equalsIgnoreCase("VIDEO")) {
                iv_event_media_play.setVisibility(View.VISIBLE);
                ThumbnailExtractAsyncNewTask thumbnailExtract = new ThumbnailExtractAsyncNewTask(model.getUrl(), img_gallery, true);
                thumbnailExtract.execute();
            }
        }

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendClickEvent(AppConstants.ACTION_TYPE.GALLERY_IMAGE_ACTION, model);
            }
        });

    }


}
