package com.dubaiisc.disc.holder;

import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.gallery.Item;
import com.squareup.picasso.Picasso;

public class GalleryDilogeHolder extends BaseHolder {

    private ImageView img_zoom;

    public GalleryDilogeHolder(View itemView) {
        super(itemView);
        img_zoom = itemView.findViewById(R.id.img_zoom);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final Item model = (Item) obj;
        onBindModelToViews(position, model);
    }

    private void onBindModelToViews(final int position, final Item model) {
        img_zoom.setVisibility(View.VISIBLE);
        Picasso.get().load(model.getUrl() != null ? model.getUrl() : "")
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_thumb).fit().into(img_zoom);


    }
}
