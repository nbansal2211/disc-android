package com.dubaiisc.disc.holder.country;

import android.view.View;
import android.widget.TextView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.holder.BaseHolder;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class CountryNameHolder extends BaseHolder {

    private TextView country;

    public CountryNameHolder(View itemView) {
        super(itemView);
        country = findTv(R.id.tv_country);
    }

    @Override
    public void onBind(int position, final Object obj) {
        super.onBind(position, obj);
        CountryModel model = (CountryModel) obj;

        country.setText(model.getName());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_COUNTRY_NAME, obj);
            }
        });
    }
}
