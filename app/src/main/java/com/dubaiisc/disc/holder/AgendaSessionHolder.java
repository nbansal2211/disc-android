package com.dubaiisc.disc.holder;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.agenda.Moderator;
import com.dubaiisc.disc.model.agenda.SessionModel;
import com.dubaiisc.disc.model.agenda.Speaker;
import com.dubaiisc.disc.utilities.AppConstants;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.ConvertsUtils;
import simplifii.framework.widgets.CustomFontTextView;

public class AgendaSessionHolder extends BaseHolder {

    private CustomFontTextView tvTime, tvMainHeading, tvChildHeading;
    private LinearLayout lnrLytChildItems;

    public AgendaSessionHolder(View itemView) {
        super(itemView);
        tvTime = (CustomFontTextView) findView(R.id.tv_time);
        tvMainHeading = (CustomFontTextView) findView(R.id.tv_sessionMainHeading);
        tvChildHeading = (CustomFontTextView) findView(R.id.tv_sessionChildHeading);
        lnrLytChildItems = (LinearLayout) findView(R.id.lnrLyt_childItems);
    }

    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        SessionModel session = (SessionModel) obj;
        tvTime.setText(session.getStartTime());
        tvMainHeading.setText(session.getTitle());
        if (session.getType().equalsIgnoreCase(AppConstants.STRING_CONST.SEMINAR)) {
            if (session != null) {
                onBindDataForChildItems(session);
            }
        } else
            onRemoveBindDataForChildItems();
    }

    private void onBindDataForChildItems(SessionModel session) {
        tvChildHeading.setText(session.getSubTitle());
        //Remove previous view from layout....
        if (lnrLytChildItems.getChildCount() > 0) {
            lnrLytChildItems.removeAllViews();
        }

        //add new view in layout....
        if (CollectionUtils.isNotEmpty(session.getSpeakers())){
            for (final Speaker speaker : session.getSpeakers()) {
                View childItemView = LayoutInflater.from(context).inflate(R.layout.row_child_session_speakers, lnrLytChildItems, false);
                Picasso.get().load(speaker.getImageURL()).fit().into((ImageView) childItemView.findViewById(R.id.iv_profileImage));
                ((CustomFontTextView) childItemView.findViewById(R.id.tv_name)).setText(speaker.getName());
                ((CustomFontTextView) childItemView.findViewById(R.id.tv_designation)).setText(speaker.getDesignation());
                childItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendClickEvent(AppConstants.ACTION_TYPE.SELECT_AGENDA_SPEAKER_FEED, speaker);
                    }
                });
                lnrLytChildItems.addView(childItemView);
            }
        }

        boolean isModeratorLabel = true;
        if (CollectionUtils.isNotEmpty(session.getModerators())){
            for (final Moderator moderator : session.getModerators()) {
                if (isModeratorLabel) {
                    CustomFontTextView textView = new CustomFontTextView(context);
                    textView.setText(R.string.moderated_by);
                    textView.setTextSize(17);
                    textView.setPadding(0, ConvertsUtils.dp2px(context, 20), 0, 0);
                    textView.setCustomFont(context, "Campton_Medium.ttf");
                    textView.setTextColor(context.getResources().getColor(R.color.grey_1));
                    textView.setGravity(Gravity.START);
                    textView.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                    lnrLytChildItems.addView(textView);
                    isModeratorLabel = false;
                }

                View childItemView = LayoutInflater.from(context).inflate(R.layout.row_child_session_speakers, lnrLytChildItems, false);
                Picasso.get().load(moderator.getImageURL()).fit().into((ImageView) childItemView.findViewById(R.id.iv_profileImage));
                ((CustomFontTextView) childItemView.findViewById(R.id.tv_name)).setText(moderator.getName());
                ((CustomFontTextView) childItemView.findViewById(R.id.tv_designation)).setText(moderator.getDesignation());
                childItemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendClickEvent(AppConstants.ACTION_TYPE.SELECT_AGENDA_MODERATOR_FEED, moderator);
                    }
                });
                lnrLytChildItems.addView(childItemView);
            }
        }
    }

    private void onRemoveBindDataForChildItems() {
        //clear previous view from layout....
        if (lnrLytChildItems.getChildCount() > 0) {
            lnrLytChildItems.removeAllViews();
        }
    }

}
