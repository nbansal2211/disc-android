package com.dubaiisc.disc.holder.media;

import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.holder.BaseHolder;
import com.dubaiisc.disc.model.media.FeedModel;
import com.dubaiisc.disc.utilities.AppConstants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import simplifii.framework.widgets.CustomFontTextView;

public class NewsHolder extends BaseHolder {

    private View tint;
    private ImageView newsBackground;
    private ImageView newsShare;
    private CustomFontTextView newsDate;
    private CustomFontTextView newsContent;
    private int[] color;

    public NewsHolder(View itemView) {
        super(itemView);
        tint = findView(R.id.v_tint);
        newsBackground = (ImageView) findView(R.id.iv_newsBackground);
        newsShare = (ImageView) findView(R.id.iv_newsShare);
        newsDate = (CustomFontTextView) findView(R.id.tv_newsDate);
        newsContent = (CustomFontTextView) findView(R.id.tv_newsContent);

        color = new int[]{
                R.color.news_filter_1,
                R.color.news_filter_2,
                R.color.news_filter_3,
                R.color.news_filter_4
        };
    }

    @Override
    public void onBind(final int position, Object obj) {
        super.onBind(position, obj);
        final FeedModel model = (FeedModel) obj;

        final String imageUrl = model.getImageUrl();

        setBackgroundImage(position, imageUrl);

        newsDate.setText(model.getDate());
        newsContent.setText(model.getTitle());
        newsShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SHARE_NEWS, model);
            }
        });
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendClickEvent(AppConstants.ACTION_TYPE.SELECT_MEDIA, model);
            }
        });
    }

//    private String getTitle(String title) {
//        return StringUtility.isNotEmpty(title) ? title : context.getResources().getString(R.string.dubai_sport_council_page);
//    }

    private void setBackgroundImage(final int position, String imageUrl) {
        final int index = position % 4;
        newsBackground.setAlpha(1f);
        newsBackground.setBackgroundResource(color[index]);

        if (imageUrl != null) {
            Picasso.get().load(imageUrl).fit().centerCrop().into(newsBackground, new Callback() {
                @Override
                public void onSuccess() {
                    tint.setBackgroundResource(color[index]);
                    newsBackground.setAlpha(0.15f);
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
