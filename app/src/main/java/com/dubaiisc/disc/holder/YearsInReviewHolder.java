package com.dubaiisc.disc.holder;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewModel;
import com.dubaiisc.disc.utilities.StringUtility;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;
import simplifii.framework.widgets.CustomFontTextView;

public class YearsInReviewHolder extends BaseHolder {

    private CustomFontTextView tvEdition, tvYear, tvDate, tvTitle, tvAttendeessCount, tvSpeakersCount, tvTopicsCount, tvRecommdCount, tvDesc;
    private LinearLayout lytChildItems;

    public YearsInReviewHolder(View itemView) {
        super(itemView);
        tvEdition = itemView.findViewById(R.id.tv_mainTitle);
        lytChildItems = itemView.findViewById(R.id.lyt_childItems);
        tvYear = itemView.findViewById(R.id.tv_year);
        tvDate = itemView.findViewById(R.id.tv_date);
        tvTitle = itemView.findViewById(R.id.tv_title);
        tvAttendeessCount = itemView.findViewById(R.id.tv_attendeessCount);
        tvSpeakersCount = itemView.findViewById(R.id.tv_speakersCount);
        tvTopicsCount = itemView.findViewById(R.id.tv_topicCount);
        tvRecommdCount = itemView.findViewById(R.id.tv_recommdCount);
        tvDesc = itemView.findViewById(R.id.tv_desc);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBind(int position, Object obj) {
        super.onBind(position, obj);
        final YearsInReviewModel model = (YearsInReviewModel) obj;

        onBindModelToViews(model);
        checkVisibilityOfChildItems(model);
        handleTitleOnClickListener(model);
    }

    private void onBindModelToViews(YearsInReviewModel model) {

        if (LocalHelperUtility.getLanguage().equalsIgnoreCase(FrameworkConstants.LANGUAGE.EN)){
            Spanned spanned = Html.fromHtml(model.getEditionTranslation());
            String s = spanned.toString();
            SpannableStringBuilder editionText=null;
            if (s.length()>0){
                if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0)))){
                    editionText = StringUtility.getSuperScriptSupportedString(s, 1, 3);
                }
                if (s.length()>1) {
                    if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(1)))) {
                        editionText = StringUtility.getSuperScriptSupportedString(s, 2, 4);
                    }
                }
                if (s.length()>2) {
                    if (TextUtils.isDigitsOnly(String.valueOf(s.charAt(0))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(1))) && TextUtils.isDigitsOnly(String.valueOf(s.charAt(2)))) {
                        editionText = StringUtility.getSuperScriptSupportedString(s, 3, 5);
                    }
                }
            }
            if (editionText !=null){
                tvEdition.setText(editionText.append(" ").append(context.getString(R.string.edition)));
            }
        }else {
            tvEdition.setText(Html.fromHtml(model.getEditionTranslation()) + " " + context.getString(R.string.edition));
        }


//        tvDate.setText(model.getDateDisplayString());
        tvYear.setText(model.getEdition());
        tvDate.setText(model.getDateDisplayString());
        tvTitle.setText(model.getTitle());
        tvAttendeessCount.setText(String.valueOf(model.getAttendeesCount()));
        tvSpeakersCount.setText(String.valueOf(model.getSpeakersCount()));
        tvTopicsCount.setText(String.valueOf(model.getTopicsCount()));
        tvRecommdCount.setText(String.valueOf(model.getRecomendationsCount()));
        tvDesc.setText(model.getDescription());
    }

    private void checkVisibilityOfChildItems(YearsInReviewModel model) {
        if (model.isVisible()) {
            addVectorDrawableOnTextView(tvEdition, 0, 0, R.drawable.ic_minus, 0);
            lytChildItems.setVisibility(View.VISIBLE);
        } else {
            addVectorDrawableOnTextView(tvEdition, 0, 0, R.drawable.ic_plus, 0);
            lytChildItems.setVisibility(View.GONE);
        }
    }

    private void handleTitleOnClickListener(final YearsInReviewModel model) {
        tvEdition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.isVisible()) {
                    model.setVisible(false);
                    addVectorDrawableOnTextView(tvEdition, 0, 0, R.drawable.ic_plus, 0);
                    lytChildItems.setVisibility(View.GONE);
                } else {
                    model.setVisible(true);
                    addVectorDrawableOnTextView(tvEdition, 0, 0, R.drawable.ic_minus, 0);
                    lytChildItems.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void addVectorDrawableOnTextView(AppCompatTextView textView,
                                             @DrawableRes int start, @DrawableRes int top,
                                             @DrawableRes int end, @DrawableRes int bottom) {
        textView.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
    }

}
