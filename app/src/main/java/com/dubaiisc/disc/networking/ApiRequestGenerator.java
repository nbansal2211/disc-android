package com.dubaiisc.disc.networking;

import android.text.TextUtils;

import com.dubaiisc.disc.model.ProfilePictureUploadApiResponse;
import com.dubaiisc.disc.model.ProfileUpdateApiResponse;
import com.dubaiisc.disc.model.SaveUserDeviceResponse;
import com.dubaiisc.disc.model.agenda.AgendaApiResponse;
import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.dubaiisc.disc.model.country.CountryApiResponse;
import com.dubaiisc.disc.model.gallery.GalleryImageResponse;
import com.dubaiisc.disc.model.media.MediaApiResponse;
import com.dubaiisc.disc.model.networking.NetworkUserListApiResponse;
import com.dubaiisc.disc.model.notification.NotificationApiResponse;
import com.dubaiisc.disc.model.speakers.SpeakerApiResponse;
import com.dubaiisc.disc.model.sponsors.SponsorApiResponse;
import com.dubaiisc.disc.model.tickets.TicketApiResponse;
import com.dubaiisc.disc.model.user.DeviceModel;
import com.dubaiisc.disc.model.user.SignInModel;
import com.dubaiisc.disc.model.user.UserApiResponse;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewApiResponse;
import com.dubaiisc.disc.utilities.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.LinkedHashMap;

import simplifii.framework.asyncmanager.FileParamObject;
import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.JsonUtil;

public class ApiRequestGenerator {

    public static HttpParamObject onPostDeviceInfo(DeviceModel model) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.POST_UER_DEVICE_INFO);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BaseApiResponse.class);
        httpParamObject.setJson(JsonUtil.toJson(model));
        return httpParamObject;
    }

    public static HttpParamObject onPostRegistration(UserModel model) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.POST_USER_REGISTRATION);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(UserApiResponse.class);
        httpParamObject.setJson(JsonUtil.toJson(model));
        return httpParamObject;
    }

    public static HttpParamObject onPostSignIn(SignInModel model) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.POST_USER_SIGN_IN);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(UserApiResponse.class);
        httpParamObject.setJson(JsonUtil.toJson(model));
        return httpParamObject;
    }

    public static HttpParamObject onPostForgetPassword(String email) throws JSONException {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.POST_USER_FORGET_PASSWORD);
        httpParamObject.setPostMethod();
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(BaseApiResponse.class);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(ApiUrlConstants.PARAMS.EMAIL, email);
        httpParamObject.setJson(jsonObject.toString());
        return httpParamObject;
    }

    public static HttpParamObject onGetUserProfile(long id) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_USER_PROFILE + id);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(UserApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetEventDays() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_EVENT_DAYS);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(AgendaApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetEventDaysByEvenetId(int eventId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.EVENT_DAYS_WITH_EVENT_ID_URL + eventId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(AgendaApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetSpeakers() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_SPEAKERS);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(SpeakerApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetSpeakersByEventId(int eventId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_SPEAKERS_BY_EVENT_ID + eventId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(SpeakerApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetGalleryByEventId(int eventId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GALLERY_URL + eventId);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(GalleryImageResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetSponsor() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_SPONSOR);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(SponsorApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetTicketVerification(String ticketNumber) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_TICKET_NUMBER + ticketNumber);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(TicketApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetMediaFeeds() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_MEDIA_FEED);
        httpParamObject.addParameter("groupByType", "true");
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(MediaApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetNotification() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_NOTIFICATION);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(NotificationApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onGetCountries() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_COUNTRIES_LIST);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(CountryApiResponse.class);
        return httpParamObject;
    }

    public static HttpParamObject onPutUserProfile(UserModel mUserModel) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.PUT_USER_PROFLE + mUserModel.getId());
        httpParamObject.setJSONContentType();
        httpParamObject.setPutMethod();
        httpParamObject.setJson(JsonUtil.toJson(mUserModel));
        httpParamObject.setClassType(ProfileUpdateApiResponse.class);
        return httpParamObject;
    }

    public static FileParamObject onPostUserProfilePic(File file, String fileName) {
        FileParamObject fileParamObject = new FileParamObject(file, fileName, AppConstants.STRING_CONST.FILE_UPLOAD_KEY);
        fileParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.POST_FILES_UPLOAD);
        fileParamObject.setPostMethod();
        fileParamObject.setClassType(ProfilePictureUploadApiResponse.class);
        return fileParamObject;
    }

    public static HttpParamObject onGetUsers() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_USERS_LIST);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(NetworkUserListApiResponse.class);
        return httpParamObject;
    }

    public static Object onGetNetworkSearchResult(LinkedHashMap<String, String> params) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_NETWORK_SEARCH_RESULT);
        for (String key : params.keySet()) {
            httpParamObject.addParameter(key, params.get(key));
        }
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(NetworkUserListApiResponse.class);
        return httpParamObject;
    }

    public static Object onGetYearsInReview() {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.GET_YEARS_IN_REVIEW);
        httpParamObject.setJSONContentType();
        httpParamObject.setClassType(YearsInReviewApiResponse.class);
        return httpParamObject;
    }


    public static HttpParamObject sendUserDeviceInfos(String pushToken, String deviceId) {
        HttpParamObject httpParamObject = new HttpParamObject();
        httpParamObject.setUrl(ApiUrlConstants.WEB_SERVICE_URL.SEND_USER_DEVICE_INFO);
        httpParamObject.setJSONContentType();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("platform", "ANDROID");
//            jsonObject.put("userId", "");
            if (!TextUtils.isEmpty(deviceId)) {
                jsonObject.put("deviceId", deviceId);
            }
            if (!TextUtils.isEmpty(pushToken)) {
                jsonObject.put("pushToken", pushToken);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        httpParamObject.setPostMethod();
        httpParamObject.setJson(jsonObject.toString());
        httpParamObject.setClassType(SaveUserDeviceResponse.class);
        return httpParamObject;
    }
}
