package com.dubaiisc.disc.networking;

public interface ApiUrlConstants {

    interface WEB_SERVICE_URL {
        //App Live and Base URLs....
        String TILL_24_NOV_2018_BASE_URL = "https://discapi.azurewebsites.net/";
        String LIVE_BASE_URL = "https://api.dubaiisc.ae/";
        String TEST_BASE_URL = "http://api.2bcloudhost.com/";

        //App Common Web Services URLs.....
        String BASE_URL = LIVE_BASE_URL;
        String USER_URL = "api/users/";
        String DEVICE_INFO = "/api/userDeviceInfos/";
        String TICKET_URL = "api/tickets/";
        String EVENT_DAYS_URL = "api/eventDays/";
        String EVENTS_URL = "api/events/";
        String SPEAKERS_URL = "api/speakers/";
        String SPONSORS_URL = "api/sponsers/";
        String FEEDS_URL = "api/feeds/";
        String COUNTRIES_URL = "api/countries/";
        String FILES_UPLOAD_URL = "api/files/upload/";
        String NOTIFICATIONS_URL = "api/notifications/";

        //User Web Services...
        String POST_USER_REGISTRATION = BASE_URL + USER_URL;
        String POST_USER_SIGN_IN = BASE_URL + USER_URL + "login";
        String POST_USER_FORGET_PASSWORD = BASE_URL + USER_URL + "forgotPassword";
        String GET_USER_PROFILE = BASE_URL + USER_URL;
        String PUT_USER_PROFLE = BASE_URL + USER_URL;
        String POST_UER_DEVICE_INFO = BASE_URL + DEVICE_INFO;
        String GALLERY_URL = BASE_URL + "api/gallery?eventId=";

        //Events Web Services
        String GET_EVENT_DAYS = BASE_URL + EVENT_DAYS_URL;
        String GET_SPEAKERS = BASE_URL + SPEAKERS_URL;
        String GET_SPEAKERS_BY_EVENT_ID = GET_SPEAKERS + "?eventId=";
        String GET_YEARS_IN_REVIEW = BASE_URL + EVENTS_URL;

        //Sponsor web services
        String GET_SPONSOR = BASE_URL + SPONSORS_URL;

        //Ticket web Services
        String GET_TICKET_NUMBER = BASE_URL + TICKET_URL + "?ticketNumber=";

        //Media Feeds Web Services
        String GET_MEDIA_FEED = BASE_URL + FEEDS_URL;

        //Notification or Publication
        String GET_NOTIFICATION = BASE_URL + NOTIFICATIONS_URL;

        //Countries list web services
        String GET_COUNTRIES_LIST = BASE_URL + COUNTRIES_URL;

        //Users list web services
        String GET_USERS_LIST = BASE_URL + USER_URL;
        String GET_NETWORK_SEARCH_RESULT = BASE_URL + USER_URL;

        //Files upload web service
        String POST_FILES_UPLOAD = BASE_URL + FILES_UPLOAD_URL;
        String EVENT_DAYS_WITH_EVENT_ID_URL = BASE_URL + EVENT_DAYS_URL + "?eventId=";

        String SEND_USER_DEVICE_INFO = BASE_URL + "/api/userDeviceInfos/";
    }

    interface PARAMS {
        String EMAIL = "email";
    }

}
