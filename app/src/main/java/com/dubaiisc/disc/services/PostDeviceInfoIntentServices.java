package com.dubaiisc.disc.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.Nullable;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.dubaiisc.disc.model.user.DeviceModel;

import org.json.JSONException;

import java.io.IOException;
import java.sql.SQLException;

import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.asyncmanager.Service;
import simplifii.framework.asyncmanager.ServiceFactory;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.Preferences;

public class PostDeviceInfoIntentServices extends IntentService {

    public PostDeviceInfoIntentServices() {
        super(PostDeviceInfoIntentServices.class.getName());
    }

    @SuppressLint("HardwareIds")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        DeviceModel deviceModel = new DeviceModel();
        deviceModel.setDeviceId(Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        deviceModel.setPushToken(Preferences.getData(AppConstants.STRING_CONST.FCM_TOKEN, ""));
        deviceModel.setPlatform("Android");
        deviceModel.setUserId("");

        try {
            Service service = ServiceFactory.getInstance(this, FrameworkConstants.TASK_CODES.DEVICE_INFO);
            BaseApiResponse response = (BaseApiResponse) service.getData(ApiRequestGenerator.onPostDeviceInfo(deviceModel));
            if (!response.getError()) {
                //on successfully saved
                Preferences.saveData(AppConstants.STRING_CONST.DEVICE_INFO_UPLOADED, true);
            }
        } catch (JSONException | SQLException | RestException | IOException e) {
            Preferences.saveData(AppConstants.STRING_CONST.DEVICE_INFO_UPLOADED, false);
            e.printStackTrace();
        }
    }
}
