package com.dubaiisc.disc;

import android.app.Activity;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;

import com.esafirm.imagepicker.features.ImagePicker;

public class CustomImagePicker {

    public static void openImagePicker(Activity activity, int requestCode) {
        ImagePicker.
                create(activity).
                single().
                folderMode(true).
                toolbarImageTitle(activity.getString(com.dubaiisc.disc.R.string.image_picker_title)).
                toolbarFolderTitle(activity.getString(com.dubaiisc.disc.R.string.image_picker_title)).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }

    public static void openImagePicker(Fragment fragment, int requestCode) {
        ImagePicker.
                create(fragment).
                single().
                folderMode(true).
                toolbarImageTitle(fragment.getActivity().getString(com.dubaiisc.disc.R.string.image_picker_title)).
                toolbarFolderTitle(fragment.getActivity().getString(com.dubaiisc.disc.R.string.image_picker_title)).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }

    public static void openImagePicker(Activity activity, @StringRes int title, int requestCode) {
        ImagePicker.
                create(activity).
                single().
                folderMode(true).
                toolbarImageTitle(activity.getString(title)).
                toolbarFolderTitle(activity.getString(title)).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }

    public static void openImagePicker(Fragment fragment, @StringRes int title, int requestCode) {
        ImagePicker.
                create(fragment).
                single().
                folderMode(true).
                toolbarImageTitle(fragment.getActivity().getString(title)).
                toolbarFolderTitle(fragment.getActivity().getString(title)).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }

    public static void openImagePicker(Activity activity, String title, int requestCode) {
        ImagePicker.
                create(activity).
                single().
                folderMode(true).
                toolbarImageTitle(title).
                toolbarFolderTitle(title).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }

    public static void openImagePicker(Fragment fragment, String title, int requestCode) {
        ImagePicker.
                create(fragment).
                single().
                folderMode(true).
                toolbarImageTitle(title).
                toolbarFolderTitle(title).
                theme(com.dubaiisc.disc.R.style.ImagePickerTheme).
                start(requestCode);
    }
}
