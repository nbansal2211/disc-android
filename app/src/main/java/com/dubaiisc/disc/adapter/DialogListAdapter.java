package com.dubaiisc.disc.adapter;

import android.content.Context;

import java.util.List;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class DialogListAdapter<T extends BaseAdapterModel> extends BaseRecycleAdapter<T> {

    public static final int COUNTRY_ISD = AppConstants.VIEW_CODES.COUNTRY_ISD;
    public static final int COUNTRY_NAME = AppConstants.VIEW_CODES.COUNTRY_NAME;
    public static final int GENDER_TYPE = AppConstants.VIEW_CODES.GENDER_TYPE;
    private int viewType;

    /**
     * @param context
     * @param list
     * @param viewType {@link #COUNTRY_ISD}, or {@link #COUNTRY_NAME}
     */
    public DialogListAdapter(Context context, List list, int viewType) {
        super(context, list);
        this.viewType = viewType;
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }
}
