package com.dubaiisc.disc.adapter;

import android.content.Context;

import java.util.List;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class MediaFeedAdapter<T extends BaseAdapterModel> extends BaseRecycleAdapter<T> {

    public static final int NEWS = AppConstants.VIEW_CODES.NEWS;
    public static final int SOCIAL_MEDIA = AppConstants.VIEW_CODES.FEEDS;

    private int viewType;

    /**
     * @param context
     * @param list
     * @param viewType {@link #NEWS}, or {@link #SOCIAL_MEDIA}
     */
    public MediaFeedAdapter(Context context, List list, int viewType) {
        super(context, list);
        this.viewType = viewType;
    }

    @Override
    public int getItemViewType(int position) {
        return viewType;
    }
}
