package com.dubaiisc.disc.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import com.dubaiisc.disc.fragments.registration.BaseRegistrationFragment;

public class RegistrationViewPagerAdapter extends FragmentPagerAdapter {

    private List<FragmentModelHolder> mFragmentsList = new ArrayList<>();

    public RegistrationViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentsList.get(position).getFragment();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentsList.get(position).getTitle();
    }

    @Override
    public int getCount() {
        return mFragmentsList.size();
    }

    public void addFragments(FragmentModelHolder modelHolder) {
        mFragmentsList.add(modelHolder);
    }

    public static class FragmentModelHolder {
        private BaseRegistrationFragment fragment;
        private String title;

        public FragmentModelHolder(BaseRegistrationFragment fragment, String title) {
            this.fragment = fragment;
            this.title = title;
        }

        public BaseRegistrationFragment getFragment() {
            return fragment;
        }

        public String getTitle() {
            return title;
        }
    }

}
