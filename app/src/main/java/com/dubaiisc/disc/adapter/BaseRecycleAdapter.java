package com.dubaiisc.disc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.holder.AgendaSessionHolder;
import com.dubaiisc.disc.holder.BaseHolder;
import com.dubaiisc.disc.holder.FaqsHolder;
import com.dubaiisc.disc.holder.GalleryDilogeHolder;
import com.dubaiisc.disc.holder.GalleryEventHolder;
import com.dubaiisc.disc.holder.GalleryImageHolder;
import com.dubaiisc.disc.holder.GenderTypeHolder;
import com.dubaiisc.disc.holder.NetworkingPersonHolder;
import com.dubaiisc.disc.holder.NotificationHolder;
import com.dubaiisc.disc.holder.SectionHeaderHolder;
import com.dubaiisc.disc.holder.SpeakerHolder;
import com.dubaiisc.disc.holder.SponsorHolder;
import com.dubaiisc.disc.holder.TermsAndConditionHolder;
import com.dubaiisc.disc.holder.YearsInReviewHolder;
import com.dubaiisc.disc.holder.country.CountryIsdHolder;
import com.dubaiisc.disc.holder.country.CountryNameHolder;
import com.dubaiisc.disc.holder.media.NewsHolder;
import com.dubaiisc.disc.holder.media.SocialMediaHolder;
import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.List;


/**
 * Created by nbansal2211 on 25/08/16.
 */
public class BaseRecycleAdapter<T extends BaseAdapterModel> extends RecyclerView.Adapter<BaseHolder> {
    protected List<T> list;
    protected Context context;
    protected LayoutInflater inflater;
    private RecyclerClickInterface clickInterface;
    public static int selectItem = -1;

    public RecyclerClickInterface getClickInterface() {
        return clickInterface;
    }

    public void setClickInterface(RecyclerClickInterface clickInterface) {
        this.clickInterface = clickInterface;
    }

    public BaseRecycleAdapter(Context context, List<T> list) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public BaseHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case AppConstants.VIEW_CODES.SECTION_HEADER:
                itemView = inflater.inflate(R.layout.row_section_header, parent, false);
                return new SectionHeaderHolder(itemView);

            case AppConstants.VIEW_CODES.SPONSOR:
                itemView = inflater.inflate(R.layout.row_sponsor, parent, false);
                SponsorHolder sponsorHolder = new SponsorHolder(itemView);
                sponsorHolder.setClickListener(clickInterface);
                return sponsorHolder;

            case AppConstants.VIEW_CODES.SPEAKER:
                itemView = inflater.inflate(R.layout.row_speaker, parent, false);
                SpeakerHolder speakerHolder = new SpeakerHolder(itemView);
                speakerHolder.setClickListener(clickInterface);
                return speakerHolder;

            case AppConstants.VIEW_CODES.AGENDA_SESSION_TIMELINE:
                itemView = inflater.inflate(R.layout.row_agenda_timeline_session, parent, false);
                AgendaSessionHolder sessionHolder = new AgendaSessionHolder(itemView);
                sessionHolder.setClickListener(clickInterface);
                return sessionHolder;

            case AppConstants.VIEW_CODES.NETWORKING_PERSON:
                itemView = inflater.inflate(R.layout.row_networking_person, parent, false);
                NetworkingPersonHolder networkingPersonHolder = new NetworkingPersonHolder(itemView);
                networkingPersonHolder.setClickListener(clickInterface);
                return networkingPersonHolder;

            case AppConstants.VIEW_CODES.NOTIFICATION:
                itemView = inflater.inflate(R.layout.row_notification, parent, false);
                NotificationHolder notificationHolder = new NotificationHolder(itemView);
                return notificationHolder;

            case AppConstants.VIEW_CODES.NEWS:
                itemView = inflater.inflate(R.layout.row_news, parent, false);
                NewsHolder newsHolder = new NewsHolder(itemView);
                newsHolder.setClickListener(clickInterface);
                return newsHolder;

            case AppConstants.VIEW_CODES.FEEDS:
                itemView = inflater.inflate(R.layout.row_social_media, parent, false);
                SocialMediaHolder socialMediaHolder = new SocialMediaHolder(itemView);
                socialMediaHolder.setClickListener(clickInterface);
                return socialMediaHolder;

            case AppConstants.VIEW_CODES.FAQS:
                itemView = inflater.inflate(R.layout.row_faqs, parent, false);
                FaqsHolder faqsHolder = new FaqsHolder(itemView);
                return faqsHolder;

            case AppConstants.VIEW_CODES.COUNTRY_ISD:
                itemView = inflater.inflate(R.layout.row_country, parent, false);
                CountryIsdHolder countryIsdHolder = new CountryIsdHolder(itemView);
                countryIsdHolder.setClickListener(clickInterface);
                return countryIsdHolder;

            case AppConstants.VIEW_CODES.COUNTRY_NAME:
                itemView = inflater.inflate(R.layout.row_country, parent, false);
                CountryNameHolder countryNameHolder = new CountryNameHolder(itemView);
                countryNameHolder.setClickListener(clickInterface);
                return countryNameHolder;

            case AppConstants.VIEW_CODES.GENDER_TYPE:
                itemView = inflater.inflate(R.layout.row_tv, parent, false);
                GenderTypeHolder genderTypeHolder = new GenderTypeHolder(itemView);
                genderTypeHolder.setClickListener(clickInterface);
                return genderTypeHolder;

            case AppConstants.VIEW_CODES.YEAR_IN_REVIEW:
                itemView = inflater.inflate(R.layout.row_years_in_review, parent, false);
                YearsInReviewHolder yearsInReviewHolder = new YearsInReviewHolder(itemView);
                yearsInReviewHolder.setClickListener(clickInterface);
                return yearsInReviewHolder;

            case AppConstants.VIEW_CODES.GALLERY_YEAR_REVIEW:
                itemView = inflater.inflate(R.layout.row_gallery_years_review, parent, false);
                GalleryEventHolder galleryEventHolder = new GalleryEventHolder(itemView);
                galleryEventHolder.setClickListener(clickInterface);
                return galleryEventHolder;

            case AppConstants.VIEW_CODES.GALLERY_IMAGE:
                itemView = inflater.inflate(R.layout.gallery_image, parent, false);
                GalleryImageHolder galleryImageHolder = new GalleryImageHolder(itemView);
                galleryImageHolder.setClickListener(clickInterface);
                return galleryImageHolder;

            case AppConstants.VIEW_CODES.GALLERY_IMAGE_DIALOG:
                itemView = inflater.inflate(R.layout.gallery_dialog_image, parent, false);
                GalleryDilogeHolder galleryDilogeHolder = new GalleryDilogeHolder(itemView);
                galleryDilogeHolder.setClickListener(clickInterface);
                return galleryDilogeHolder;

            case AppConstants.VIEW_CODES.TERMS_CONDITION:
                itemView = inflater.inflate(R.layout.row_terms_conditions, parent, false);
                TermsAndConditionHolder termsAndConditionHolder = new TermsAndConditionHolder(itemView);
                termsAndConditionHolder.setClickListener(clickInterface);
                return termsAndConditionHolder;
        }
        throw new IllegalArgumentException("Invalid View Type");
    }

    @Override
    public void onBindViewHolder(BaseHolder holder, int position) {
        holder.onBind(position, this.list.get(position));
    }


    @Override
    public int getItemViewType(int position) {
        return list.get(position).getViewType();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface RecyclerClickInterface {
        void onItemClick(View itemView, int position, Object obj, int actionType);
    }

}