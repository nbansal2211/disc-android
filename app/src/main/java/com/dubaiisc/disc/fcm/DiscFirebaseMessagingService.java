package com.dubaiisc.disc.fcm;

import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.dubaiisc.disc.activities.SplashScreenActivity;
import com.dubaiisc.disc.services.PostDeviceInfoIntentServices;
import com.dubaiisc.disc.utilities.AppConstants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.notificationUtility.EasyNotification;

public class DiscFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String token) {
        Log.d("Device Token", "Refreshed token: " + token);
        Preferences.saveData(AppConstants.STRING_CONST.FCM_TOKEN, token);
        //Upload device token to server...
        Intent intent = new Intent(getApplicationContext(), PostDeviceInfoIntentServices.class);
        startService(intent);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("Notification From", "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {         // Check if message contains a data payload.

            //Get Unread Notification count....
            int previousCount = Preferences.getData(AppConstants.STRING_CONST.UNREAD_NOTIFICATION_COUNT, 0);
            //Update Unread Notification count....
            Preferences.saveData(AppConstants.STRING_CONST.UNREAD_NOTIFICATION_COUNT, ++previousCount);


            //Fetch Message details....
            Map<String, String> data = remoteMessage.getData();
            String title = data.containsKey(AppConstants.STRING_CONST.TITLE) ? data.get(AppConstants.STRING_CONST.TITLE) : "";
            String body = data.containsKey(AppConstants.STRING_CONST.BODY) ? data.get(AppConstants.STRING_CONST.BODY) : "";
            String message = data.containsKey(AppConstants.STRING_CONST.MESSAGE) ? data.get(AppConstants.STRING_CONST.MESSAGE) : "";

            //Build Notification Request
            Intent intent = new Intent(this, SplashScreenActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            //Add Notification Params...
            EasyNotification easyNotification = new EasyNotification.EasyNotificationBuilder(this, pendingIntent)
                    .setChannelId(getString(R.string.default_notification_channel_id))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setTitle(title)
                    .setContent(body)
                    .setBigMessage(message)
                    .setAutoCancel(true)
                    .build();
            //send notification...
            easyNotification.sendBigTextNotification();
        }
    }


}
