package com.dubaiisc.disc.fragments.agenda;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.agenda.Moderator;
import com.dubaiisc.disc.model.agenda.Speaker;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaDetailsFragment extends AppBaseFragment {

    private Speaker mSpeaker;
    private Moderator mModerator;

    public static AgendaDetailsFragment newInstance() {
        return new AgendaDetailsFragment();
    }

    public AgendaDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_agenda_details;
    }

    @Override
    public void initViews() {
        loadArgumentsFromBundle();
    }

    private void loadArgumentsFromBundle() {
        Bundle bundle = getArguments();
        if (bundle.getString(FrameworkConstants.BUNDLE_KEYS.MODEL_TYPE).equals(AppConstants.STRING_CONST.TYPE_SPEAKERS)) {
            mSpeaker = bundle.getParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL);
            applySpeakerModelToViews(mSpeaker);
        } else if (bundle.getString(FrameworkConstants.BUNDLE_KEYS.MODEL_TYPE).equals(AppConstants.STRING_CONST.TYPE_MODERATOR)) {
            mModerator = bundle.getParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL);
            applyModeratorModelToViews(mModerator);
        }
    }

    private void applyModeratorModelToViews(Moderator moderator) {
        Picasso.get().load(moderator.getImageURL()).fit().into((ImageView) findView(R.id.iv_speakerThumb));
        setText(moderator.getName(), R.id.tv_speakerName);
        setText(moderator.getDesignation(), R.id.tv_speakerDesignation);
        setText(moderator.getInfo(), R.id.tv_speakerInfo);
        if (CollectionUtils.isNotEmpty(moderator.getAgendaStrings())) {
            final int size = moderator.getAgendaStrings().size();
            StringBuilder agendas = new StringBuilder();
            for (int i = 0; i < size; i++) {
                agendas.append(moderator.getAgendaStrings().get(i));
                if (i < size - 1)
                    agendas.append("\n");
            }
            setText(agendas.toString(), R.id.tv_speakerAgendaStrings);
        } else
            hideVisibility(R.id.tv_speakerAgendaStrings, R.id.v_divider_1, R.id.v_divider_2);
    }

    private void applySpeakerModelToViews(Speaker speaker) {
        Picasso.get().load(speaker.getImageURL()).fit().into((ImageView) findView(R.id.iv_speakerThumb));
        setText(speaker.getName(), R.id.tv_speakerName);
        setText(speaker.getDesignation(), R.id.tv_speakerDesignation);
        setText(speaker.getInfo(), R.id.tv_speakerInfo);
        if (CollectionUtils.isNotEmpty(speaker.getAgendaStrings())) {
            final int size = speaker.getAgendaStrings().size();
            StringBuilder agendas = new StringBuilder();
            for (int i = 0; i < size; i++) {
                agendas.append(speaker.getAgendaStrings().get(i));
                if (i < size - 1)
                    agendas.append("\n");
            }
            setText(agendas.toString(), R.id.tv_speakerAgendaStrings);
        } else
            hideVisibility(R.id.tv_speakerAgendaStrings, R.id.v_divider_1, R.id.v_divider_2);
    }


}
