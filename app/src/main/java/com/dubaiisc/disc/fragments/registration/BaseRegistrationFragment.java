package com.dubaiisc.disc.fragments.registration;


import android.app.Fragment;
import android.view.View;

import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.user.UserModel;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseRegistrationFragment extends AppBaseFragment {

    public abstract boolean isValid();

    public abstract void showInvalidFieldError();

    public abstract void apply(UserModel registrationModel);

    public abstract void setClickListenerCallback(View.OnClickListener listenerCallback);

}
