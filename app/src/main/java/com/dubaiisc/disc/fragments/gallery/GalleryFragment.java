package com.dubaiisc.disc.fragments.gallery;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.ImageGalleryActivity;
import com.dubaiisc.disc.activities.MyVideoViewActivity;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.gallery.GalleryImageResponse;
import com.dubaiisc.disc.model.gallery.Item;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewApiResponse;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

import static com.dubaiisc.disc.utilities.AppConstants.VIEW_CODES.GALLERY_YEAR_REVIEW;

public class GalleryFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private LinearLayout btView;
    private ProgressBar progress;
    private RecyclerView mRvGalleryList;
    private RecyclerView mRvYearsInReview;
    private CustomFontTextView tvPhoto;
    private CustomFontTextView tvVideo;
    private BaseRecycleAdapter<YearsInReviewModel> mYearsInReviewAdapter;
    private BaseRecycleAdapter<Item> imageReviewAdapter;
    private List<YearsInReviewModel> mYearsInReviewModelList = new ArrayList<>();
    private List<Item> finalList = new ArrayList<>();
    private List<Item> filterList = new ArrayList<>();

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_gallery;
    }

    @Override
    public void initViews() {
        mRvGalleryList = (RecyclerView) findView(R.id.rv_galleryList);
        mRvYearsInReview = (RecyclerView) findView(R.id.rv_yearsInReview);
        tvPhoto = (CustomFontTextView) findView(R.id.tv_photo);
        tvVideo = (CustomFontTextView) findView(R.id.tv_video);
        btView = (LinearLayout) findView(R.id.bt_view);
        progress = (ProgressBar) findView(R.id.progress);
        setRvYearsInReviewAdapter();
        executeTask(FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW, ApiRequestGenerator.onGetYearsInReview());
    }


    private void setRvYearsInReviewAdapter() {
        BaseRecycleAdapter.selectItem = 0;
        mYearsInReviewAdapter = new BaseRecycleAdapter<>(activity, mYearsInReviewModelList);
        mRvYearsInReview.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        mYearsInReviewAdapter.setClickInterface(this);
        mRvYearsInReview.setAdapter(mYearsInReviewAdapter);
        imageReviewAdapter = new BaseRecycleAdapter<>(activity, filterList);
        mRvGalleryList.setLayoutManager(new GridLayoutManager(activity, 2));
        imageReviewAdapter.setClickInterface(this);
        mRvGalleryList.setAdapter(imageReviewAdapter);
        tvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setViewTxt(tvPhoto, tvVideo);
                filter("IMAGE");
            }
        });
        tvVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setViewTxt(tvVideo, tvPhoto);
                filter("VIDEO");
            }
        });
    }

    private void setViewTxt(CustomFontTextView txt1, CustomFontTextView txt2) {
        txt1.setBackgroundColor(getResources().getColor(R.color.brown_1));
        txt1.setTextColor(getResources().getColor(R.color.white));
        txt2.setTextColor(getResources().getColor(R.color.brown_1));
        txt2.setBackgroundResource(R.drawable.shape_rect_bkg_brown_btn);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.GALLERY:
                setResponseImage(response);
                progress.setVisibility(View.GONE);
                break;
            case FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW:
                onGetYearInReviewsDataResponse(response);
                break;
        }
    }

    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == FrameworkConstants.TASK_CODES.GALLERY) {
            progress.setVisibility(View.VISIBLE);
        } else super.onPreExecute(taskCode);
    }

    private void setResponseImage(Object response) {
        GalleryImageResponse apiGalleryResponse = (GalleryImageResponse) response;
        if (!apiGalleryResponse.getError() && CollectionUtils.isNotEmpty(apiGalleryResponse.getData()) && CollectionUtils.isNotEmpty(apiGalleryResponse.getData().get(0).getItems())) {
            updateImageList(apiGalleryResponse.getData().get(0).getItems());
        } else {
            updateImageList(new ArrayList<Item>());
        }
    }

    private void updateImageList(List<Item> imageList) {
        btView.setVisibility(View.VISIBLE);
        finalList.clear();
        finalList.addAll(imageList);
        setViewTxt(tvPhoto, tvVideo);
        filter("IMAGE");
    }

    private void filter(String type) {
        filterList.clear();
        for (Item i : finalList) {
            if (i.getType().toUpperCase().equalsIgnoreCase(type)) filterList.add(i);
        }
        imageReviewAdapter.notifyDataSetChanged();
    }

    private void onGetYearInReviewsDataResponse(Object response) {
        YearsInReviewApiResponse apiResponse = (YearsInReviewApiResponse) response;
        if (apiResponse.getError()) {
            StyleableToast.makeText(activity, "", Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }
        if (apiResponse.getYearsInReview() != null && CollectionUtils.isNotEmpty(apiResponse.getYearsInReview().getYearsInReviewList())) {
            updateRvYearsInReviewAdapter(setViewType(apiResponse.getYearsInReview().getYearsInReviewListEn()));
        }

    }

    private List<YearsInReviewModel> setViewType(List<YearsInReviewModel> modelList) {
        for (YearsInReviewModel model : modelList) {
            model.setTypeChange(GALLERY_YEAR_REVIEW);
        }
        return modelList;
    }

    private void updateRvYearsInReviewAdapter(List<YearsInReviewModel> list) {
        onGetGalleryByEventId(list.get(0).getId());
        mYearsInReviewModelList.clear();
        mYearsInReviewModelList.addAll(list);
        mYearsInReviewAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.GALLERY_ACTION:
                YearsInReviewModel data = (YearsInReviewModel) obj;
                mYearsInReviewAdapter.notifyDataSetChanged();
                onGetGalleryByEventId(data.getId());
                break;
            case AppConstants.ACTION_TYPE.GALLERY_IMAGE_ACTION:
                Item dataItem = (Item) obj;
                if (dataItem.getType().toUpperCase().equalsIgnoreCase("VIDEO")) {
                    Bundle bundle = new Bundle();
                    bundle.putString(FrameworkConstants.BUNDLE_KEYS.MEDIA_PATH, dataItem.getUrl());
                    startNextActivity(bundle, MyVideoViewActivity.class);
                } else if (dataItem.getType().toUpperCase().equalsIgnoreCase("IMAGE")) {
                    setImageDialog(position);
                }
                break;
        }
    }

    private void setImageDialog(int pos) {
        Bundle bundle = new Bundle();
        bundle.putInt(FrameworkConstants.BUNDLE_KEYS.POSITION, pos);
        bundle.putParcelableArrayList(FrameworkConstants.BUNDLE_KEYS.GALLERY_ITEM, (ArrayList<? extends Parcelable>) filterList);
        startNextActivity(bundle, ImageGalleryActivity.class);
    }


    private void onGetGalleryByEventId(int eventId) {
        executeTask(FrameworkConstants.TASK_CODES.GALLERY, ApiRequestGenerator.onGetGalleryByEventId(eventId));
    }
}
