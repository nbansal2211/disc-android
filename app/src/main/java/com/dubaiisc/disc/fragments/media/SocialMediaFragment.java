package com.dubaiisc.disc.fragments.media;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.MediaFeedAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.media.FeedModel;

import java.util.List;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.MediaFeedDetailActivity;
import com.dubaiisc.disc.itemDecorators.CustomDividerItemDecorator;
import simplifii.framework.utility.FrameworkConstants;

import static com.dubaiisc.disc.utilities.AppConstants.ACTION_TYPE.SELECT_MEDIA;

/**
 * A simple {@link Fragment} subclass.
 */
public class SocialMediaFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private List<FeedModel> list;
    private RecyclerView mRvMedialList;

    public static SocialMediaFragment newInstance(List<FeedModel> list) {
        SocialMediaFragment fragment = new SocialMediaFragment();
        fragment.list = list;
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_media_holder;
    }

    @Override
    public void initViews() {
        mRvMedialList = (RecyclerView) findView(R.id.rv_medialList);
        mRvMedialList.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseRecycleAdapter<FeedModel> adapter = new MediaFeedAdapter(activity, list, MediaFeedAdapter.SOCIAL_MEDIA);
        adapter.setClickInterface(this);
        mRvMedialList.setAdapter(adapter);
        mRvMedialList.addItemDecoration(new CustomDividerItemDecorator(activity, R.drawable.divider));
        mRvMedialList.setLayoutManager(new LinearLayoutManager(activity));
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        FeedModel model = (FeedModel) obj;

        switch (actionType) {
            case SELECT_MEDIA:
                Bundle bundle = new Bundle();
                bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.FEED, model);
                startNextActivity(bundle, MediaFeedDetailActivity.class);
                break;
        }
    }


}
