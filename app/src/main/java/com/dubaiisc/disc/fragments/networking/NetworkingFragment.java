package com.dubaiisc.disc.fragments.networking;


import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.NetworkingDetailsActivity;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.country.CountryDialogFragment;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.country.CountryApiResponse;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.networking.NetworkUserListApiResponse;
import com.dubaiisc.disc.model.networking.NetworkingPersonModel;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworkingFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private LinearLayout searchBox;
    private ArrayList<CountryModel> list = new ArrayList<>();
    private CountryDialogFragment dialog;
    private EditText country;
    private RecyclerView recyclerView;
    private BaseRecycleAdapter<NetworkingPersonModel> adapter;

    public static NetworkingFragment newInstance() {
        return new NetworkingFragment();
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_networking;
    }

    @Override
    public void initViews() {

        searchBox = (LinearLayout) findView(R.id.ll_searchBox);
        recyclerView = (RecyclerView) findView(R.id.rv_peopleList);
        setOnClickListener(R.id.iv_search, R.id.et_searchCountry, R.id.btn_search, R.id.ib_registerNow);
        country = (EditText) findView(R.id.et_searchCountry);
        addVectorDrawableOnEditText(country, 0, 0, R.drawable.ic_arrow_down, 0);
        setNetworkListAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllCountries();
    }

    private void setCountriesList(ArrayList<CountryModel> list) {
        if (CollectionUtils.isEmpty(list))
            list = new ArrayList<>();
        else {
            Collections.sort(list);
        }
        CountryModel model = new CountryModel();
        model.setId(-1);
        model.setName(getString(R.string.none));
        list.add(0, model);
        this.list = list;
    }

    private void setNetworkListAdapter() {
        adapter = new BaseRecycleAdapter<>(activity, new ArrayList<NetworkingPersonModel>());
        adapter.setClickInterface(this);
        recyclerView.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setTag(linearLayoutManager);
    }

    private void updateNetworkList(ArrayList<NetworkingPersonModel> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            Collections.sort(list);
            adapter = new BaseRecycleAdapter<>(activity, list);
            adapter.setClickInterface(this);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_search:
//                ((LinearLayoutManager) recyclerView.getTag()).scrollToPositionWithOffset(0, 10);
//                AnimUtils.showOrHideLayoutBySliding(searchBox);
                break;

            case R.id.et_searchCountry:
                dialog = CountryDialogFragment.newInstance(NetworkingFragment.this, list,
                        DialogListAdapter.COUNTRY_NAME, R.string.select_country);
                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY);
                break;

            case R.id.btn_search:
                if (Preferences.isUserLoggerIn()) {
                    UserModel userModel = (UserModel) JsonUtil.parseJson(Preferences.getData(Preferences.USER_DATA_KEY, ""), UserModel.class);
                    if (userModel.isConsentForNetworking()) {
                        getSearchResult();
                    } else {
                        showEnableNetworkingDialog();
                    }
                } else {
                    showRequiredSignInDialog();
                }
                break;
        }
    }

    private void getAllUsers() {
        executeTask(FrameworkConstants.TASK_CODES.USER_LIST, ApiRequestGenerator.onGetUsers());
    }

    private void getAllCountries() {
        executeTask(FrameworkConstants.TASK_CODES.COUNTRY_LIST, ApiRequestGenerator.onGetCountries());
    }

    private void getSearchResult() {
        String name = getEditText(R.id.et_searchName);
        String country = getEditText(R.id.et_searchCountry);
        String designation = getEditText(R.id.et_searchDesignation);
        String organization = getEditText(R.id.et_searchCompanyOrganisation);

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        if (!TextUtils.isEmpty(name))
            params.put(AppConstants.STRING_CONST.NAME, name);
        if (!TextUtils.isEmpty(country))
            params.put(AppConstants.STRING_CONST.COUNTRY, country);
        if (!TextUtils.isEmpty(designation))
            params.put(AppConstants.STRING_CONST.DESIGNATION, designation);
        if (!TextUtils.isEmpty(organization))
            params.put(AppConstants.STRING_CONST.ORGANIZATION, organization);

        if (params.isEmpty()) {
            StyleableToast.makeText(getContext(), "search cannot be empty", Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }

        executeTask(FrameworkConstants.TASK_CODES.NETWORK_SEARCH, ApiRequestGenerator.onGetNetworkSearchResult(params));
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {

        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_NAME:
                country.setText(position == 0 ? getString(R.string.empty) : list.get(position).getName());
                if (dialog != null)
                    dialog.dismiss();
                break;

            case AppConstants.ACTION_TYPE.SELECT_PERSON:
                Bundle bundle = new Bundle();
                NetworkingPersonModel model = (NetworkingPersonModel) obj;
                bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.NETWORK_MODEL, model);
                startNextActivity(bundle, NetworkingDetailsActivity.class);
                break;
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.COUNTRY_LIST:
                CountryApiResponse countryApiResponse = (CountryApiResponse) response;
                if (countryApiResponse.getError()) {
                    showToast(countryApiResponse.getMessage());
                    return;
                }
                setCountriesList(countryApiResponse.getData());
                break;

            case FrameworkConstants.TASK_CODES.USER_LIST:
//                NetworkUserListApiResponse networkUserListApiResponse = (NetworkUserListApiResponse) response;
//                if (networkUserListApiResponse.getError()) {
//                    showToast(networkUserListApiResponse.getMessage());
//                    return;
//                }
//                setNetworkList(networkUserListApiResponse.getGalleryData());

            case FrameworkConstants.TASK_CODES.NETWORK_SEARCH:
                NetworkUserListApiResponse apiResponse = (NetworkUserListApiResponse) response;
                if (apiResponse.getError()) {
                    showToast(apiResponse.getMessage());
                    return;
                }
                updateNetworkList(apiResponse.getData());
                break;

        }
    }

    private void showRequiredSignInDialog() {
        Util.createAlertDialog(getContext(), getString(R.string.reqiured_signin_msg), getString(R.string.signin_required), true, "Ok", new Util.DialogListener() {
            @Override
            public void onOKPressed(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

            @Override
            public void onCancelPressed(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void showEnableNetworkingDialog() {
        Util.createAlertDialog(getContext(), getString(R.string.unable_to_view_networking_msg), getString(R.string.cannot_access), true, "Ok", new Util.DialogListener() {
            @Override
            public void onOKPressed(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

            @Override
            public void onCancelPressed(DialogInterface dialog, int which) {

            }
        }).show();
    }

}
