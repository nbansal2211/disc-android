package com.dubaiisc.disc.fragments.agenda;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.agenda.Moderator;
import com.dubaiisc.disc.model.agenda.SessionModel;
import com.dubaiisc.disc.model.agenda.Speaker;

import java.util.List;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.ActivityContainer;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.utility.FrameworkConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private RecyclerView mRvAgendaList;

    private BaseRecycleAdapter<SessionModel> mAgendaListAdapter;
    private List<SessionModel> mListEventSessions;

    public static EventFragment newInstance(List<SessionModel> session) {
        EventFragment eventFragment = new EventFragment();
        eventFragment.mListEventSessions = session;
        return eventFragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_events;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        initAgendaRecyclerViewAdapter();
    }

    private void initFragmentViews() {
        mRvAgendaList = (RecyclerView) findView(R.id.rv_agendaTimeline);
        //have to do when using within view pager with Right to Left combination...
        mRvAgendaList.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
    }

    private void initAgendaRecyclerViewAdapter() {
        if (mListEventSessions != null) {
            mAgendaListAdapter = new BaseRecycleAdapter<>(getContext(), mListEventSessions);
            mRvAgendaList.setAdapter(mAgendaListAdapter);
            mRvAgendaList.setLayoutManager(new LinearLayoutManager(getContext()));
            mAgendaListAdapter.setClickInterface(this);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_AGENDA_SPEAKER_FEED:
                onAgendaSpeakerItemClicked((Speaker) obj);
                break;
            case AppConstants.ACTION_TYPE.SELECT_AGENDA_MODERATOR_FEED:
                onAgendaModeratorItemClicked((Moderator) obj);
                break;
        }
    }

    private void onAgendaSpeakerItemClicked(Speaker speaker) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL, speaker);
        bundle.putString(FrameworkConstants.BUNDLE_KEYS.MODEL_TYPE, AppConstants.STRING_CONST.TYPE_SPEAKERS);
        ActivityContainer.startActivity(getContext(), AppConstants.FRAGMENT_TYPE.AGENDA_DETAILS, bundle);
    }

    private void onAgendaModeratorItemClicked(Moderator moderator) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL, moderator);
        bundle.putString(FrameworkConstants.BUNDLE_KEYS.MODEL_TYPE, AppConstants.STRING_CONST.TYPE_MODERATOR);
        ActivityContainer.startActivity(getContext(), AppConstants.FRAGMENT_TYPE.AGENDA_DETAILS, bundle);
    }
}
