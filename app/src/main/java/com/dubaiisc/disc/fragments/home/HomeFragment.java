package com.dubaiisc.disc.fragments.home;


import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.toRadians;

/**app:srcCompat="@drawable/placeholder_sponsor"
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends AppBaseFragment {

    private ImageView bkg;
    private AppCompatImageView speakers;
    private AppCompatImageView sponsors;
    private AppCompatImageView about;
    private AppCompatImageView agenda;
    private AppCompatImageView why_attend;

    private HomeNavigator navigator;

    public static HomeFragment newInstance(HomeNavigator navigator) {
        HomeFragment fragment = new HomeFragment();
        fragment.navigator = navigator;
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_home;
    }


    @Override
    public void initViews() {
        bkg = (ImageView) findView(R.id.iv_bkg);
        speakers = (AppCompatImageView) findView(R.id.iv_speakers);
        sponsors = (AppCompatImageView) findView(R.id.iv_sponser);
        about = (AppCompatImageView) findView(R.id.iv_about);
        agenda = (AppCompatImageView) findView(R.id.iv_agenda);
        why_attend = (AppCompatImageView) findView(R.id.iv_why_attend);

        setOnClickListener(
                R.id.lyt_register,
                R.id.iv_speakers,
                R.id.iv_sponser,
                R.id.iv_about,
                R.id.iv_agenda,
                R.id.iv_why_attend);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Picasso.get().load(R.drawable.bkg_home).placeholder(R.drawable.bkg_home).fit().into(bkg);
        addLinearGradient(speakers, R.dimen.size_penta_speakers);
        addLinearGradient(sponsors, R.dimen.size_penta_sponsors);
        addLinearGradient(about, R.dimen.size_penta_about);
        addLinearGradient(agenda, R.dimen.size_penta_agenda);
        addLinearGradient(why_attend, R.dimen.size_penta_why_attend);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        navigator.onItemClickCallback(v);
    }


    private void addLinearGradient(ImageView imageView, @DimenRes int size) {

        // Start color of the gradient
        @ColorRes int startColor = R.color.penta_gradient_start;
        // End color of the gradient
        @ColorRes int endColor = R.color.penta_gradient_end;
        int angle = 97;

        // Get drawable from the image view
        Drawable drawable = imageView.getDrawable();
        Bitmap originalBitmap = Bitmap.createBitmap(activity.getResources().getDimensionPixelSize(size),
                activity.getResources().getDimensionPixelSize(size), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(originalBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        int width = activity.getResources().getDimensionPixelSize(size);
        int height = activity.getResources().getDimensionPixelSize(size);
        int start = getResources().getColor(startColor);
        int end = getResources().getColor(endColor);

        float r = (float) sqrt(width * width + height * height);
        float endX = Math.abs((float) (r * cos(toRadians(angle))));
        float endY = Math.abs((float) (r * sin(toRadians(angle))));

        // Create new bitmap
        Bitmap updatedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas newCanvas = new Canvas(updatedBitmap);
        newCanvas.drawBitmap(originalBitmap, 0, 0, null);
        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0, 0, endX, endY, start, end, Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        newCanvas.drawRect(0, 0, width, height, paint);

        imageView.setImageDrawable(new BitmapDrawable(getResources(), updatedBitmap));
    }
}
