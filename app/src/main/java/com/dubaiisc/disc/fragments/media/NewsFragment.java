package com.dubaiisc.disc.fragments.media;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.MediaFeedAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.media.FeedModel;

import java.util.List;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.MediaFeedDetailActivity;
import simplifii.framework.utility.FrameworkConstants;

import static com.dubaiisc.disc.utilities.AppConstants.ACTION_TYPE.SELECT_MEDIA;
import static com.dubaiisc.disc.utilities.AppConstants.ACTION_TYPE.SHARE_NEWS;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private List<FeedModel> list;
    private RecyclerView mRvMedialList;

    public static NewsFragment newInstance(List<FeedModel> list) {
        NewsFragment fragment = new NewsFragment();
        fragment.list = list;
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_media_holder;
    }

    @Override
    public void initViews() {
        mRvMedialList = (RecyclerView) findView(R.id.rv_medialList);
        mRvMedialList.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseRecycleAdapter<FeedModel> adapter = new MediaFeedAdapter(activity, list, MediaFeedAdapter.NEWS);
        mRvMedialList.setAdapter(adapter);
        adapter.setClickInterface(this);
        mRvMedialList.setLayoutManager(new GridLayoutManager(activity, 2));
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {

        FeedModel model = (FeedModel) obj;
        switch (actionType) {
            case SHARE_NEWS:
                String content = getShareContent(model);
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                startActivity(Intent.createChooser(sharingIntent, "Share using..."));
                break;

            case SELECT_MEDIA:
                Bundle bundle = new Bundle();
                bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.FEED, model);
                startNextActivity(bundle, MediaFeedDetailActivity.class);
                break;
        }
    }

    private String getShareContent(FeedModel model) {
        if (model.getTitle() != null)
            return model.getTitle();
        else {
            if (model.getShortText() != null)
                return model.getShortText();
        }
        return getResources().getString(R.string.empty);
    }
}
