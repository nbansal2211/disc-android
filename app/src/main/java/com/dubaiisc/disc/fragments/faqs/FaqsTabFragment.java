package com.dubaiisc.disc.fragments.faqs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.faqs.QuesAnsModel;

import java.util.List;

import com.dubaiisc.disc.R;

public class FaqsTabFragment extends AppBaseFragment {

    private List<QuesAnsModel> list;
    private RecyclerView mRvFaqsList;

    public static FaqsTabFragment newInstance(List<QuesAnsModel> list) {
        FaqsTabFragment faqsTabFragment = new FaqsTabFragment();
        faqsTabFragment.list = list;
        return faqsTabFragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_faqs_tab;
    }

    @Override
    public void initViews() {
        mRvFaqsList = (RecyclerView) findView(R.id.rv_faqslList);
        //have to do when using within view pager with Right to Left combination...
        mRvFaqsList.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BaseRecycleAdapter<QuesAnsModel> adapter = new BaseRecycleAdapter<>(activity, list);
        mRvFaqsList.setAdapter(adapter);
        mRvFaqsList.setLayoutManager(new LinearLayoutManager(activity));
    }
}
