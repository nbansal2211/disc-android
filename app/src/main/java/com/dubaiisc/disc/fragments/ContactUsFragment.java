package com.dubaiisc.disc.fragments;

import com.dubaiisc.disc.R;

public class ContactUsFragment extends AppBaseFragment {

    public static ContactUsFragment newInstance() {
        return new ContactUsFragment();
    }

    @Override
    public void initViews() {
        setOnClickListener(R.id.ib_registerNow);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_contact_us;
    }
}
