package com.dubaiisc.disc.fragments.registration;


import android.app.Fragment;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.country.CountryDialogFragment;
import com.dubaiisc.disc.model.country.CountryApiResponse;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dubaiisc.disc.CustomImagePicker;
import com.dubaiisc.disc.R;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.country.CountryDialogFragment;
import com.dubaiisc.disc.model.country.CountryApiResponse;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.ValidationHelper;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontEditText;
import simplifii.framework.widgets.CustomFontTextView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationGeneralFragment extends BaseRegistrationFragment implements AdapterView.OnItemSelectedListener, BaseRecycleAdapter.RecyclerClickInterface {

    private CountryDialogFragment dialog;
    private View.OnClickListener mListenerCallback;
    private CustomFontTextView fileName;

    private ArrayList<CountryModel> list;

    private File imageFile;

    public static RegistrationGeneralFragment newInstance() {
        RegistrationGeneralFragment generalFragment = new RegistrationGeneralFragment();
        return generalFragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_registration_general;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        setOnClickListener(
                R.id.btn_upload,
                R.id.iv_removeFile,
                R.id.edtTxt_country
        );
        getAllCountries();
    }

    private void initFragmentViews() {
        addVectorDrawableOnEditText((EditText) findView(R.id.edtTxt_country), 0, 0, R.drawable.ic_arrow_down, 0);
//        addVectorDrawableOnEditText((EditText) findView(R.id.edtTxt_countryCodeMobile), 0, 0, R.drawable.ic_arrow_down, 0);
//        addVectorDrawableOnEditText((EditText) findView(R.id.edtTxt_countryCodeWork), 0, 0, R.drawable.ic_arrow_down, 0);
    }

    private void getAllCountries() {
        if (CollectionUtils.isEmpty(list))
            executeTask(FrameworkConstants.TASK_CODES.COUNTRY_LIST, ApiRequestGenerator.onGetCountries());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.COUNTRY_LIST:
                CountryApiResponse apiResponse = (CountryApiResponse) response;
                if (apiResponse.getError()) {
                    showToast(apiResponse.getMessage());
                    return;
                }
                list = apiResponse.getData();
                if (CollectionUtils.isNotEmpty(list))
                    Collections.sort(list);
                initCountriesAdapter(list);
        }
    }

    private void initCountriesAdapter(ArrayList<CountryModel> list) {
        if (CollectionUtils.isEmpty(list))
            list = new ArrayList<>();
        else {
            Collections.sort(list);
        }
        this.list = list;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_upload:
                onUploadButtonClick();
                break;

            case R.id.iv_removeFile:
                findView(R.id.iv_removeFile).setVisibility(View.GONE);
                fileName.setVisibility(View.GONE);
                break;

            case R.id.edtTxt_countryCodeMobile:
//                dialog = CountryDialogFragment.newInstance(RegistrationGeneralFragment.this, list,
//                        DialogListAdapter.COUNTRY_ISD, R.string.select_country_code);
//                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY_CODE_MOBILE);
                break;

            case R.id.edtTxt_countryCodeWork:
//                dialog = CountryDialogFragment.newInstance(RegistrationGeneralFragment.this, list,
//                        DialogListAdapter.COUNTRY_ISD, R.string.select_country_code);
//                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY_CODE_WORK);
                break;

            case R.id.edtTxt_country:
                dialog = CountryDialogFragment.newInstance(RegistrationGeneralFragment.this, list,
                        DialogListAdapter.COUNTRY_NAME, R.string.select_country);
                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY);
                break;
        }
    }

    private void onUploadButtonClick() {
        CustomImagePicker.
                openImagePicker(this, AppConstants.REQUEST_TYPE.SELECT_PROFILE_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppConstants.REQUEST_TYPE.SELECT_PROFILE_IMAGE:
                if (resultCode == RESULT_OK) {
                    handleSelectedImage(data);
                }
                break;
        }
    }

    private void handleSelectedImage(Intent data) {
        if (fileName == null)
            fileName = (CustomFontTextView) findView(R.id.tv_fileName);

        List<Image> images = ImagePicker.getImages(data);

        if (CollectionUtils.isNotEmpty(images)) {
            imageFile = new File(images.get(0).getPath());
            fileName.setText(imageFile.getName());
            findView(R.id.iv_removeFile).setVisibility(View.VISIBLE);
            fileName.setVisibility(View.VISIBLE);
        }

        if (ValidationHelper.isFileSizeNotValid(imageFile)) {
            Toast.makeText(activity, getString(R.string.image_size_error), Toast.LENGTH_SHORT).show();
            imageFile = null;
        }
    }

    @Override
    public boolean isValid() {
        return ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_numberMobile).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_countryCodeMobile).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_city).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_country).trim());

//        return ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_numberMobile).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_countryCodeWork).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_countryCodeMobile).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_numberWork).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_extWork).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_city).trim())
//                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_country).trim());

    }

    @Override
    public void showInvalidFieldError() {
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_numberMobile).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_numberMobile)).setError(getString(R.string.field_cannot_be_blank));

//        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_numberWork).trim()))
//            ((CustomFontEditText) findView(R.id.edtTxt_numberWork)).setError(getString(R.string.field_cannot_be_blank));

//        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_countryCodeWork).trim()))
//            ((CustomFontEditText) findView(R.id.edtTxt_countryCodeWork)).setError(getString(R.string.field_cannot_be_blank));
//        else
//            ((CustomFontEditText) findView(R.id.edtTxt_countryCodeWork)).setError(null, null);

        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_countryCodeMobile).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_countryCodeMobile)).setError(getString(R.string.field_cannot_be_blank));
        else
            ((CustomFontEditText) findView(R.id.edtTxt_countryCodeMobile)).setError(null, null);

//        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_extWork).trim()))
//            ((CustomFontEditText) findView(R.id.edtTxt_extWork)).setError(getString(R.string.field_cannot_be_blank));
//
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_city).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_city)).setError(getString(R.string.field_cannot_be_blank));

        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_country).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_country)).setError(getString(R.string.field_cannot_be_blank));
        else
            ((CustomFontEditText) findView(R.id.edtTxt_country)).setError(null, null);
    }

    @Override
    public void apply(UserModel userModel) {
        userModel.setMobileCode(getEditText(R.id.edtTxt_countryCodeMobile));
        userModel.setMobile(getEditText(R.id.edtTxt_numberMobile));
        userModel.setPhoneCode(getEditText(R.id.edtTxt_countryCodeWork));
        userModel.setWork(getEditText(R.id.edtTxt_numberWork));
        userModel.setWorkExt(getEditText(R.id.edtTxt_extWork));
        userModel.setCountry(getEditText(R.id.edtTxt_country));
        userModel.setCity(getEditText(R.id.edtTxt_city).trim());
        userModel.setImageFile(imageFile);
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {
        mListenerCallback = listenerCallback;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        handleSpinnerBackground(parent.getId());

        if (position == 0)
            ((TextView) view).setTextColor(getContext().getResources().getColor(R.color.grey_1));
        else
            ((TextView) view).setTextColor(getContext().getResources().getColor(R.color.brown_1));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void handleSpinnerBackground(@IdRes int id) {
        switch (id) {

//            case R.id.s_countryCodeMobile:
//            case R.id.s_countryCodeWork:
//            case R.id.s_country:
//                Spinner spinner = (Spinner) findView(id);
//
////                if(position==0)
//                spinner.setBackgroundResource(R.drawable.bkg_spinner_search_country);
//                else
//                    spinner.setBackgroundResource(R.drawable.bkg_spinner_search_country_selected);
//                break;
        }
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_NAME:
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_ISD:
                if (dialog != null) {
                    CountryModel countryModel = (CountryModel) obj;
                    switch (dialog.getTag()) {
//                        case AppConstants.STRING_CONST.COUNTRY_CODE_MOBILE:
//                            setEditText(R.id.edtTxt_countryCodeMobile, String.valueOf(countryModel.getIsd()));
//                            break;
//                        case AppConstants.STRING_CONST.COUNTRY_CODE_WORK:
//                            setEditText(R.id.edtTxt_countryCodeWork, String.valueOf(countryModel.getIsd()));
//                            break;
                        case AppConstants.STRING_CONST.COUNTRY:
                            setEditText(R.id.edtTxt_country, countryModel.getName());
                    }
                    dialog.dismiss();
                }
        }
    }
}
