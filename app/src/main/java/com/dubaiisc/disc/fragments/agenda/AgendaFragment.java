package com.dubaiisc.disc.fragments.agenda;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.adapter.ViewPagerAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.agenda.AgendaApiResponse;
import com.dubaiisc.disc.model.agenda.AgendaModel;
import com.dubaiisc.disc.model.agenda.Event;
import com.dubaiisc.disc.networking.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgendaFragment extends AppBaseFragment {

    private TabLayout mTabFragContainer;
    private ViewPager mVpFragContainer;

    private List<ViewPagerAdapter.FragmentModelHolder> mListFragmentHolder = new ArrayList<>();

    public static AgendaFragment newInstance() {
        AgendaFragment fragment = new AgendaFragment();
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_agenda;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        setOnClickListener(R.id.ib_registerNow);
    }

    private void initFragmentViews() {
        mTabFragContainer = (TabLayout) findView(R.id.tab_fragContainer);
        mVpFragContainer = (ViewPager) findView(R.id.vp_agendaFragContainer);
        //have to do, when using view pager with Right to Left combination...
        mVpFragContainer.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0)); //right to left
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        getAllEventDays();
        getEventDaysByEvenetId();
    }

    private void onInitFragmentsAfterEventsDataReceived(AgendaModel agendaModel) {
        if (agendaModel != null) {
            initViewPagerFragments(agendaModel.getEvents());
            initViewPager();
            initViewPagerTabs(agendaModel.getEvents());
            setTabLayoutChangeListener();
        }
    }

    private void initViewPagerFragments(List<Event> dataList) {
        if (CollectionUtils.isNotEmpty(dataList)) {
            for (Event data : dataList)
                mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(EventFragment.newInstance(data.getSessions()), ""));
        }
    }

    private void initViewPager() {
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            pagerAdapter.addFragments(holder);
        }
        mVpFragContainer.setAdapter(pagerAdapter);
        mTabFragContainer.setupWithViewPager(mVpFragContainer);
    }

    private void initViewPagerTabs(List<Event> eventList) {
        if (CollectionUtils.isNotEmpty(eventList)) {
            for (int index = 0; index < eventList.size(); index++) {
                LinearLayout tab = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.tabs_custom_heading, null);
                CustomFontTextView heading = tab.findViewById(R.id.tv_heading);
                CustomFontTextView date = tab.findViewById(R.id.tv_date);
                AppCompatImageView indicator = tab.findViewById(R.id.iv_indicator);
                heading.setText(eventList.get(index).getDisplayDayNumber());
                date.setText(eventList.get(index).getDisplayDate());
                heading.setTextColor(index == 0 ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                date.setTextColor(index == 0 ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                indicator.setVisibility(index == 0 ? View.VISIBLE : View.INVISIBLE);
                mTabFragContainer.getTabAt(index).setCustomView(tab);
            }
        }
    }

    private void setTabLayoutChangeListener() {
        mTabFragContainer.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                for (int tabIndex = 0; tabIndex < mTabFragContainer.getTabCount(); tabIndex++) {
                    LinearLayout tabView = (LinearLayout) mTabFragContainer.getTabAt(tabIndex).getCustomView();
                    CustomFontTextView heading = tabView.findViewById(R.id.tv_heading);
                    CustomFontTextView date = tabView.findViewById(R.id.tv_date);
                    AppCompatImageView indicator = tabView.findViewById(R.id.iv_indicator);
                    heading.setTextColor(tab.getPosition() == tabIndex ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                    date.setTextColor(tab.getPosition() == tabIndex ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                    indicator.setVisibility(tab.getPosition() == tabIndex ? View.VISIBLE : View.INVISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getAllEventDays() {
        executeTask(FrameworkConstants.TASK_CODES.EVENT_DAYS, ApiRequestGenerator.onGetEventDays());
    }

    private void getEventDaysByEvenetId() {
        executeTask(FrameworkConstants.TASK_CODES.EVENT_DAYS_BY_ID, ApiRequestGenerator.onGetEventDaysByEvenetId(19));
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.EVENT_DAYS:
                onGetAllEventDaysResponse(response);
                break;
            case FrameworkConstants.TASK_CODES.EVENT_DAYS_BY_ID:
                onGetAllEventDaysResponse(response);
                break;
        }
    }

    private void onGetAllEventDaysResponse(Object response) {
        AgendaApiResponse apiResponse = (AgendaApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }
        //Now init all events fragment after GalleryData is successfully received...
        onInitFragmentsAfterEventsDataReceived(apiResponse.getData());
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        showToast(re != null ? re.getMessage() : getString(R.string.bkg_error));
    }
}

