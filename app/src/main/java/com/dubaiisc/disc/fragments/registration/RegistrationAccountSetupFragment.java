package com.dubaiisc.disc.fragments.registration;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.dubaiisc.disc.model.user.UserModel;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.ValidationHelper;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontCheckBox;
import simplifii.framework.widgets.CustomFontEditText;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationAccountSetupFragment extends BaseRegistrationFragment {

    private View.OnClickListener mListenerCallback;
    private CustomFontCheckBox mCbTermsCondition;

    @Override
    public int getViewID() {
        return R.layout.fragment_account_setup;
    }

    public static RegistrationAccountSetupFragment newInstance() {
        RegistrationAccountSetupFragment setupFragment = new RegistrationAccountSetupFragment();
        return setupFragment;
    }

    @Override
    public void initViews() {
        mCbTermsCondition = (CustomFontCheckBox) findView(R.id.cb_iAgree);
        setOnClickListener(
                R.id.btn_submit,
                R.id.tv_terms_and_conditions,
                R.id.tv_privacy_and_security);
        setonCheckedChangeListener();
    }

    private void setonCheckedChangeListener() {
        mCbTermsCondition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                findView(R.id.btn_submit).setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Util.underLineTextViews(((CustomFontTextView) findView(R.id.tv_terms_and_conditions)));
        Util.underLineTextViews(((CustomFontTextView) findView(R.id.tv_privacy_and_security)));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        mListenerCallback.onClick(v);
    }

    @Override
    public boolean isValid() {
        return ValidationHelper.isEmailValid(getEditText(R.id.edtTxt_email).trim())
                && ValidationHelper.isPasswordValid(getEditText(R.id.edtTxt_password).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_retypePassword).trim())
                && ValidationHelper.isEquals(getEditText(R.id.edtTxt_password).trim(), getEditText(R.id.edtTxt_retypePassword).trim(), false);
    }

    @Override
    public void showInvalidFieldError() {
        if (ValidationHelper.isEmailNotValid(getEditText(R.id.edtTxt_email)))
            ((CustomFontEditText) findView(R.id.edtTxt_email)).setError(getString(R.string.email_is_invalid));//This field cannot be blank
        if (ValidationHelper.isPasswordNotValid(getEditText(R.id.edtTxt_password)))
            ((CustomFontEditText) findView(R.id.edtTxt_password)).setError(getString(R.string.password_is_invalid_must_be));
        if (ValidationHelper.isNotEquals(getEditText(R.id.edtTxt_password).trim(), getEditText(R.id.edtTxt_retypePassword).trim(), false)) {
            ((CustomFontEditText) findView(R.id.edtTxt_retypePassword)).setError(getString(R.string.password_do_not_match));
        }
    }

    @Override
    public void apply(UserModel userModel) {
        userModel.setEmail(getEditText(R.id.edtTxt_email).trim());
        userModel.setPassword(getEditText(R.id.edtTxt_password).trim());
        userModel.setConsentForNetworking(((CheckBox) findView(R.id.cb_consentForNetworking)).isChecked());
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {
        mListenerCallback = listenerCallback;
    }


}
