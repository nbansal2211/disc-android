package com.dubaiisc.disc.fragments.media;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.adapter.ViewPagerAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.media.FeedModel;
import com.dubaiisc.disc.model.media.MediaApiResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MediaFragment extends AppBaseFragment {

    private TabLayout mTabFragContainer;
    private ViewPager mVpFragContainer;

    private List<ViewPagerAdapter.FragmentModelHolder> mListFragmentHolder = new ArrayList<>();

    public static MediaFragment newInstance() {
        return new MediaFragment();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_media;
    }


    @Override
    public void initViews() {
        initFragmentViews();
        setOnClickListener(R.id.ib_registerNow);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onGetAllMedia();
    }

    private void initFragmentViews() {
        mTabFragContainer = (TabLayout) findView(R.id.tab_fragContainer);
        mVpFragContainer = (ViewPager) findView(R.id.vp_mediaFragContainer);
        //have to do, when using view pager with Right to Left combination...
        mVpFragContainer.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0)); //right to left
    }

    private void initViewPagerFragments(Map<String, List<FeedModel>> feed) {
        if (CollectionUtils.isEmpty(mListFragmentHolder) && feed != null) {
            for (String key : feed.keySet()) {
                String title = getTitleBasedOnKey(key);
                if (key.equals(AppConstants.STRING_CONST.NEWS))
                    mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(
                            NewsFragment.newInstance(feed.get(key)), title));

                else {
                    mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(
                            SocialMediaFragment.newInstance(feed.get(key)), title));
                }
            }
        }
    }

    private void initViewPager() {
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            pagerAdapter.addFragments(holder);
        }
        mVpFragContainer.setAdapter(pagerAdapter);
        mTabFragContainer.setupWithViewPager(mVpFragContainer);
    }

    private void initViewPagerTabs(HashMap<String, List<FeedModel>> feeds) {
        if (feeds != null) {
            int index = 0;
            for (String key : feeds.keySet()) {
                LinearLayout tab = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.tabs_custom_heading, null);
                CustomFontTextView heading = tab.findViewById(R.id.tv_heading);
                CustomFontTextView date = tab.findViewById(R.id.tv_date);
                date.setVisibility(View.GONE);
                AppCompatImageView indicator = tab.findViewById(R.id.iv_indicator);
                heading.setText(getTitleBasedOnKey(key));
                heading.setTextColor(index == 0 ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                indicator.setVisibility(index == 0 ? View.VISIBLE : View.INVISIBLE);
                mTabFragContainer.getTabAt(index).setCustomView(tab);
                ++index;
            }
        }
    }

    private String getTitleBasedOnKey(String key) {
        switch (key) {
            case AppConstants.STRING_CONST.NEWS:
                return getString(R.string.news_pr);

            case AppConstants.STRING_CONST.FACEBOOK:
                return getString(R.string.facebook);

            case AppConstants.STRING_CONST.TWITTER:
                return getString(R.string.twitter);

            case AppConstants.STRING_CONST.INSTAGRAM:
                return getString(R.string.instagram);

            case AppConstants.STRING_CONST.LINKEDIN:
                return getString(R.string.linkedin);

        }
        return key;
    }

    private void setTabLayoutChangeListener() {
        mTabFragContainer.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                for (int tabIndex = 0; tabIndex < mTabFragContainer.getTabCount(); tabIndex++) {
                    LinearLayout tabView = (LinearLayout) mTabFragContainer.getTabAt(tabIndex).getCustomView();
                    CustomFontTextView heading = tabView.findViewById(R.id.tv_heading);
                    AppCompatImageView indicator = tabView.findViewById(R.id.iv_indicator);
                    heading.setTextColor(tab.getPosition() == tabIndex ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                    indicator.setVisibility(tab.getPosition() == tabIndex ? View.VISIBLE : View.INVISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void onGetAllMedia() {
        executeTask(FrameworkConstants.TASK_CODES.MEDIA, ApiRequestGenerator.onGetMediaFeeds());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        MediaApiResponse apiResponse = (MediaApiResponse) response;

        initViewPagerFragments(apiResponse.getData().getMap());
        initViewPager();
        initViewPagerTabs(apiResponse.getData().getMap());
        setTabLayoutChangeListener();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
    }

}
