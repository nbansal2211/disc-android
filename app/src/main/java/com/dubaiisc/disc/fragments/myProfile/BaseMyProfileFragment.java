package com.dubaiisc.disc.fragments.myProfile;

import android.view.View;

import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;

import java.util.ArrayList;

public abstract class BaseMyProfileFragment extends AppBaseFragment {

    public abstract boolean isValid();

    public abstract void showInvalidFieldError();

    public abstract void apply(UserModel userModel);

    public abstract void setModelToViews(UserModel userModel);



    public abstract void setClickListenerCallback(View.OnClickListener listenerCallback);

    public abstract void onCountryListFetched(ArrayList<CountryModel> list);
}
