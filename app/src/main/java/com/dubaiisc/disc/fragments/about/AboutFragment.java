package com.dubaiisc.disc.fragments.about;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dubaiisc.disc.fragments.AppBaseFragment;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.AnimUtils;

public class AboutFragment extends AppBaseFragment {
    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    private LinearLayout firstLayout, secondLayout, thirdLayout, fourthLayout;
    private ImageView firstButton, secondButton, thirdButton, fourthButton;

    @Override
    public void initViews() {
        setOnClickListener(
                R.id.ib_registerNow,
                R.id.cl_first,
                R.id.cl_second,
                R.id.cl_third,
                R.id.cl_fourth
        );

        firstLayout = (LinearLayout) findView(R.id.ll_first);
        secondLayout = (LinearLayout) findView(R.id.ll_second);
        thirdLayout = (LinearLayout) findView(R.id.ll_third);
        fourthLayout = (LinearLayout) findView(R.id.ll_fourth);

        firstButton = (ImageView) findView(R.id.iv_first);
        secondButton = (ImageView) findView(R.id.iv_second);
        thirdButton = (ImageView) findView(R.id.iv_third);
        fourthButton = (ImageView) findView(R.id.iv_fourth);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_about_app;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cl_first:
                showOrHideLayout(firstLayout, firstButton);
                break;

            case R.id.cl_second:
                showOrHideLayout(secondLayout, secondButton);
                break;

            case R.id.cl_third:
                showOrHideLayout(thirdLayout, thirdButton);
                break;

            case R.id.cl_fourth:
                showOrHideLayout(fourthLayout, fourthButton);
                break;

        }
    }

    private void showOrHideLayout(final View layout, ImageView button) {
        switch (layout.getVisibility()) {
            case View.VISIBLE:
                button.setImageResource(R.drawable.ic_plus);
                AnimUtils.slideUpClose(layout);
                break;

            case View.INVISIBLE:
            case View.GONE:
                button.setImageResource(R.drawable.ic_minus);
                AnimUtils.slideDownOpen(layout);
                break;
        }
    }


}
