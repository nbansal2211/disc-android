package com.dubaiisc.disc.fragments.myProfile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.country.CountryDialogFragment;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;

import java.util.ArrayList;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConnectFragment extends BaseMyProfileFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private CountryDialogFragment dialog;
    private View.OnClickListener mListenerCallback;

    private EditText countryCodeMobile;
    private EditText numberMobile;
    private EditText countryCodeWork;
    private EditText numberWork;
    private EditText extWork;
    private EditText website;
    private EditText linkedInProfile;
    private EditText facebookProfile;
    private EditText twitterProfile;
    //    private UserModel model;
    private ArrayList<CountryModel> list;

    public static ConnectFragment newInstance() {
        ConnectFragment fragment = new ConnectFragment();
        return fragment;
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_connect;
    }

    @Override
    public void initViews() {
        findView(R.id.fragment_connect_mainLayout).setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
        countryCodeMobile = (EditText) findView(R.id.edtTxt_countryCodeMobile);
        numberMobile = (EditText) findView(R.id.edtTxt_numberMobile);
        countryCodeWork = (EditText) findView(R.id.edtTxt_countryCodeWork);
        numberWork = (EditText) findView(R.id.edtTxt_numberWork);
        extWork = (EditText) findView(R.id.edtTxt_extWork);
        website = (EditText) findView(R.id.edtTxt_website);
        linkedInProfile = (EditText) findView(R.id.edtTxt_linkedInProfile);
        facebookProfile = (EditText) findView(R.id.edtTxt_facebookProfile);
        twitterProfile = (EditText) findView(R.id.edtTxt_twitterProfile);

        setOnClickListener(
                R.id.btn_save_connect_changes
        );
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void showInvalidFieldError() {

    }

    @Override
    public void apply(UserModel model) {
        model.setMobileCode(countryCodeMobile.getText().toString());
        model.setPhoneCode(countryCodeWork.getText().toString());
        model.setMobile(numberMobile.getText().toString());
        model.setWork(numberWork.getText().toString());
        model.setWorkExt(extWork.getText().toString());
        model.setWebsiteLink(website.getText().toString());
        model.setLinkedInLink(linkedInProfile.getText().toString());
        model.setFacebookLink(facebookProfile.getText().toString());
        model.setTwitterLink(twitterProfile.getText().toString());
    }

    @Override
    public void setModelToViews(UserModel model) {
//        this.model = model;
        if (model != null) {
            countryCodeMobile.setText(model.getMobileCode());
            numberMobile.setText(model.getMobile());
            countryCodeWork.setText(model.getPhoneCode());
            numberWork.setText(model.getWork());
            extWork.setText(model.getWorkExt());
            website.setText(model.getWebsiteLink());
            linkedInProfile.setText(model.getLinkedInLink());
            facebookProfile.setText(model.getFacebookLink());
            twitterProfile.setText(model.getTwitterLink());
        }
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {
        mListenerCallback = listenerCallback;
    }

    @Override
    public void onCountryListFetched(final ArrayList<CountryModel> list) {
        this.list = list;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btn_save_connect_changes:
//                model.setMobileCode(countryCodeMobile.getText().toString());
//                model.setPhoneCode(countryCodeWork.getText().toString());
//                model.setMobile(numberMobile.getText().toString());
//                model.setWork(numberWork.getText().toString());
//                model.setWorkExt(extWork.getText().toString());
//                model.setWebsiteLink(website.getText().toString());
//                model.setLinkedInLink(linkedInProfile.getText().toString());
//                model.setFacebookLink(facebookProfile.getText().toString());
//                model.setTwitterLink(twitterProfile.getText().toString());
                mListenerCallback.onClick(v);
                break;

            case R.id.edtTxt_countryCodeMobile:
//                dialog = CountryDialogFragment.newInstance(ConnectFragment.this, list,
//                        DialogListAdapter.COUNTRY_ISD, R.string.select_country_code);
//                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY_CODE_MOBILE);
                break;

            case R.id.edtTxt_countryCodeWork:
//                dialog = CountryDialogFragment.newInstance(ConnectFragment.this, list,
//                        DialogListAdapter.COUNTRY_ISD, R.string.select_country_code);
//                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY_CODE_WORK);
                break;

            case R.id.edtTxt_country:
                dialog = CountryDialogFragment.newInstance(ConnectFragment.this, list,
                        DialogListAdapter.COUNTRY_NAME, R.string.select_country);
                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY);
                break;
        }
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_ISD:
                if (dialog != null) {
                    // TODO Country ISD
                    CountryModel countryModel = (CountryModel) obj;
                    switch (dialog.getTag()) {
                        case AppConstants.STRING_CONST.COUNTRY_CODE_MOBILE:
                            countryCodeMobile.setText("+91");
                            break;
                        case AppConstants.STRING_CONST.COUNTRY_CODE_WORK:
                            countryCodeWork.setText("+91");
                    }
                    dialog.dismiss();
                }

        }
    }
}
