package com.dubaiisc.disc.fragments.speakers;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.SpeakerDetailsActivity;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.speakers.SpeakerApiResponse;
import com.dubaiisc.disc.model.speakers.SpeakerModel;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewApiResponse;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

import static com.dubaiisc.disc.utilities.AppConstants.VIEW_CODES.GALLERY_YEAR_REVIEW;

public class SpeakersFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private RecyclerView mRvSpeakersList;
    private RecyclerView mRvYearsInReview;
    private ProgressBar progress;
    private BaseRecycleAdapter<YearsInReviewModel> mYearsInReviewAdapter;
    private BaseRecycleAdapter<SpeakerModel> adapterSpeakers;
    private List<YearsInReviewModel> mYearsInReviewModelList = new ArrayList<>();
    private List<SpeakerModel> speakerModelList = new ArrayList<>();

    public static SpeakersFragment newInstance() {
        SpeakersFragment fragment = new SpeakersFragment();
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_speakers;
    }

    @Override
    public void initViews() {
        mRvYearsInReview = (RecyclerView) findView(R.id.rv_yearsInReview);
        mRvSpeakersList = (RecyclerView) findView(R.id.rv_speakerList);
        progress = (ProgressBar) findView(R.id.progress);
        setOnClickListener(R.id.ib_registerNow);
        setRvYearsInReviewAdapter();
        executeTask(FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW, ApiRequestGenerator.onGetYearsInReview());
    }

    private void setRvYearsInReviewAdapter() {
        BaseRecycleAdapter.selectItem = 0;
        mYearsInReviewAdapter = new BaseRecycleAdapter<>(activity, mYearsInReviewModelList);
        mRvYearsInReview.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        mYearsInReviewAdapter.setClickInterface(this);
        mRvYearsInReview.setAdapter(mYearsInReviewAdapter);

        adapterSpeakers = new BaseRecycleAdapter<>(getContext(), speakerModelList);
        adapterSpeakers.setClickInterface(this);
        mRvSpeakersList.setAdapter(adapterSpeakers);
        mRvSpeakersList.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    private void initSpeakersAdapter(List<SpeakerModel> list) {
        speakerModelList.clear();
        speakerModelList.addAll(list);
        adapterSpeakers.notifyDataSetChanged();
    }


    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_SPEAKER:
                onSpeakersItemSelected(obj);
                break;
            case AppConstants.ACTION_TYPE.GALLERY_ACTION:
                YearsInReviewModel data = (YearsInReviewModel) obj;
                mYearsInReviewAdapter.notifyDataSetChanged();
                onGetSpeakersByEventId(data.getId());
                break;
        }
    }

    private void onSpeakersItemSelected(Object obj) {
        SpeakerModel speakerModel = (SpeakerModel) obj;
        Bundle bundle = new Bundle();
        bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL, speakerModel);
        startNextActivity(bundle, SpeakerDetailsActivity.class);
    }

    private void onGetAllSpeakers() {
        executeTask(FrameworkConstants.TASK_CODES.SPEAKERS, ApiRequestGenerator.onGetSpeakers());
    }

    private void onGetSpeakersByEventId(int evenetId) {
        executeTask(FrameworkConstants.TASK_CODES.SPEAKERS, ApiRequestGenerator.onGetSpeakersByEventId(evenetId));
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.SPEAKERS:
                onGetAllSpeakersResponse(response);
                progress.setVisibility(View.GONE);
                break;
            case FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW:
                onGetYearInReviewsDataResponse(response);
                break;
        }
    }


    @Override
    public void onPreExecute(int taskCode) {
        if (taskCode == FrameworkConstants.TASK_CODES.SPEAKERS) {
            progress.setVisibility(View.VISIBLE);
        } else super.onPreExecute(taskCode);
    }

    private void onGetYearInReviewsDataResponse(Object response) {
        YearsInReviewApiResponse apiResponse = (YearsInReviewApiResponse) response;
        if (apiResponse.getError()) {
            StyleableToast.makeText(activity, "", Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }
        if (apiResponse.getYearsInReview() != null && CollectionUtils.isNotEmpty(apiResponse.getYearsInReview().getYearsInReviewList())) {
            updateRvYearsInReviewAdapter(setViewType(getLastTwoItem(apiResponse.getYearsInReview().getYearsInReviewListEn())));
        }

    }

    private List<YearsInReviewModel> getLastTwoItem(List<YearsInReviewModel> list) {
        if (list.size() > 2) return list.subList(Math.max(list.size() - 2, 0), list.size());
        else return list;
    }

    private List<YearsInReviewModel> setViewType(List<YearsInReviewModel> modelList) {
        for (YearsInReviewModel model : modelList) {
            model.setTypeChange(GALLERY_YEAR_REVIEW);
        }
        return modelList;
    }

    private void updateRvYearsInReviewAdapter(List<YearsInReviewModel> list) {
        onGetSpeakersByEventId(list.get(0).getId());
        mYearsInReviewModelList.clear();
        mYearsInReviewModelList.addAll(list);
        mYearsInReviewAdapter.notifyDataSetChanged();
    }

    private void onGetAllSpeakersResponse(Object response) {
        SpeakerApiResponse apiResponse = (SpeakerApiResponse) response;
        if (!apiResponse.getError() && apiResponse.getSpeaker() != null && CollectionUtils.isNotEmpty(apiResponse.getSpeaker().getSpeakersList())) {
            initSpeakersAdapter(apiResponse.getSpeaker().getSpeakersList());
        } else {
            initSpeakersAdapter(new ArrayList<SpeakerModel>());
        }
    }
}
