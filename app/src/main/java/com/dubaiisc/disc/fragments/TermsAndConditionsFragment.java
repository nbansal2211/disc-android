package com.dubaiisc.disc.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.model.termsAndConditions.TermsAndConditionApiResponse;
import com.dubaiisc.disc.model.termsAndConditions.TermsAndConditionModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;

public class TermsAndConditionsFragment extends AppBaseFragment {

    private RecyclerView rv_tc;

    private BaseRecycleAdapter<TermsAndConditionModel> mTcAdapter;
    private List<TermsAndConditionModel> mTcList = new ArrayList<>();

    public static TermsAndConditionsFragment newInstance() {
        return new TermsAndConditionsFragment();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_tnc_and_privacy;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        setTcListAdapter();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getTermsAndConditions();
    }

    private void initFragmentViews() {
        rv_tc = (RecyclerView) findView(R.id.rv_tc);
    }

    private void setTcListAdapter() {
        mTcAdapter = new BaseRecycleAdapter<>(getContext(), mTcList);
        rv_tc.setAdapter(mTcAdapter);
        rv_tc.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void updateTcListAdapter(List<TermsAndConditionModel> list) {
        if (CollectionUtils.isNotEmpty(list)) {
            mTcList.clear();
            mTcList.addAll(list);
            mTcAdapter.notifyDataSetChanged();
        }
    }


    private void getTermsAndConditions() {
        String faqsJson = Util.getStringFromInputStream(activity.getResources().openRawResource(R.raw.t_and_c));
        TermsAndConditionApiResponse apiResponse = new Gson().fromJson(faqsJson, TermsAndConditionApiResponse.class);
        onGetTermsAndConditionResponse(apiResponse);
    }

    private void onGetTermsAndConditionResponse(TermsAndConditionApiResponse apiResponse) {
        updateTcListAdapter(apiResponse.getTermsAndCondition().getTcList());
    }


}
