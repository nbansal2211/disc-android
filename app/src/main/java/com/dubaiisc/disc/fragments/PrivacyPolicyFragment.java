package com.dubaiisc.disc.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import com.dubaiisc.disc.R;
import simplifii.framework.widgets.CustomFontTextView;

public class PrivacyPolicyFragment extends AppBaseFragment {

    public static PrivacyPolicyFragment newInstance() {
        return new PrivacyPolicyFragment();
    }

    private LinearLayout privacyPolicy;
    private CustomFontTextView title;


    @Override
    public void initViews() {
        privacyPolicy = (LinearLayout) findView(R.id.ll_tnc_and_privacy);
        title = (CustomFontTextView) findView(R.id.tv_title);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        title.setText(R.string.privacy_policy);
        populateTermsAndConditions();

    }

    private void populateTermsAndConditions() {
        List<String> list = new ArrayList<>();
        list.add(getString(R.string.lorem_para));
        list.add(getString(R.string.lorem_para));
        list.add(getString(R.string.lorem_para));

        for (int i = 0; i < list.size(); i++) {
            LinearLayout layout = (LinearLayout)
                    getLayoutInflater().inflate(R.layout.row_bullet_text, null);
            CustomFontTextView text = layout.findViewById(R.id.tv_text);
            text.setText(list.get(i));
            privacyPolicy.addView(layout);
        }
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_tnc_and_privacy;
    }
}
