package com.dubaiisc.disc.fragments.myProfile;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;

import java.util.ArrayList;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.StringUtility;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordFragment extends BaseMyProfileFragment {

    private View.OnClickListener mListenerCallback;

    public static PasswordFragment newInstance() {
        PasswordFragment fragment = new PasswordFragment();
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_password;
    }

    @Override
    public void initViews() {
        findView(R.id.fragment_password_mainLayout).setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
        setOnClickListener(R.id.btn_save_connect_changes);
    }

    @Override
    public boolean isValid() {
        String newPassword = getEditText(R.id.edtTxt_password);
        String confirmPassword = getEditText(R.id.edtTxt_retypePassword);
        if (StringUtility.isNotEmpty(newPassword) && StringUtility.isNotEmpty(confirmPassword))
            return StringUtility.isEquals(newPassword, confirmPassword, false);
        else
            return StringUtility.isEmpty(newPassword) && StringUtility.isEmpty(confirmPassword);
    }

    @Override
    public void showInvalidFieldError() {
        StyleableToast.makeText(getContext(), getString(R.string.password_do_not_match), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
//        String newPassword = getEditText(R.id.edtTxt_password);
//        String confirmPassword = getEditText(R.id.edtTxt_retypePassword);
//        if (StringUtility.isNotEmpty(newPassword) && StringUtility.isNotEmpty(confirmPassword) && StringUtility.isNotEquals(newPassword, confirmPassword, false)) {
//            StyleableToast.makeText(getContext(), getString(R.string.password_do_not_match), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
//        }
    }

    @Override
    public void apply(UserModel model) {
        if (model != null) {
            String newPassword = getEditText(R.id.edtTxt_password);
            String confirmPassword = getEditText(R.id.edtTxt_retypePassword);
            if (StringUtility.isNotEmpty(newPassword) && StringUtility.isNotEmpty(confirmPassword) && StringUtility.isEquals(newPassword, confirmPassword, false)) {
                model.setPassword(confirmPassword);
            }
        }
    }

    @Override
    public void setModelToViews(UserModel model) {
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {
        mListenerCallback = listenerCallback;
    }

    @Override
    public void onCountryListFetched(ArrayList<CountryModel> list) {

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        mListenerCallback.onClick(v);
    }
}
