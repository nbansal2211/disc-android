package com.dubaiisc.disc.fragments;


import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.provider.Browser;
import android.view.View;

import com.dubaiisc.disc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WhileInDubaiFragment extends AppBaseFragment {

    @Override
    public void initViews() {
        setText(getString(R.string.things_to_do)+" & "+getString(R.string.places_to_stay), R.id.tv_thingsToDO);
        setOnClickListener(R.id.tv_thingsToDO, R.id.tv_dubaiSportsCal, R.id.tv_gettingAround, R.id.ib_registerNow);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_while_in_dubai;
    }

    public static WhileInDubaiFragment newInstance() {
        WhileInDubaiFragment fragment = new WhileInDubaiFragment();
        return fragment;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_thingsToDO:
                openLinkOnBrowser(getString(R.string.things_to_do_url));
                break;
//            case R.id.tv_placeToStay:
//                openLinkOnBrowser("https://www.visitdubai.com/en-in");
//                break;
            case R.id.tv_dubaiSportsCal:
                openLinkOnBrowser(getString(R.string.dubai_sports_club_url));
                break;
            case R.id.tv_gettingAround:
                openLinkOnBrowser(getString(R.string.getting_arrorung_url));
                break;
        }
    }
}
