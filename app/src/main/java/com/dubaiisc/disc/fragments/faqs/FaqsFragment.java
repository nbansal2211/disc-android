package com.dubaiisc.disc.fragments.faqs;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.dubaiisc.disc.adapter.ViewPagerAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.faqs.FaqsApiResponse;
import com.dubaiisc.disc.model.faqs.QuesAnsModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

public class FaqsFragment extends AppBaseFragment {

    private TabLayout mTabFragContainer;
    private ViewPager mVpFragContainer;

    private List<ViewPagerAdapter.FragmentModelHolder> mListFragmentHolder = new ArrayList<>();

    public static FaqsFragment newInstance() {
        return new FaqsFragment();
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_faqs;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        setOnClickListener(R.id.ib_registerNow);
    }

    private void initFragmentViews() {
        mTabFragContainer = (TabLayout) findView(R.id.tab_fragContainer);
        mVpFragContainer = (ViewPager) findView(R.id.vp_faqsFragContainer);
        //have to do, when using view pager with Right to Left combination...
        mVpFragContainer.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0)); //right to left
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllFaqs();
    }

    private void initViewPagerFragments(Map<String, List<QuesAnsModel>> feed) {
        if (CollectionUtils.isEmpty(mListFragmentHolder)) {
            for (String key : feed.keySet()) {
                String title = getTitleBasedOnKey(key);
                mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(
                        FaqsTabFragment.newInstance(feed.get(key)), title));
            }
        }
    }

    private void getAllFaqs() {
        String faqsJson = Util.getStringFromInputStream(activity.getResources().openRawResource(R.raw.faqs));
        FaqsApiResponse apiResponse = new Gson().fromJson(faqsJson, FaqsApiResponse.class);
        onGetAllFaqsResponse(apiResponse);
    }

    private void onGetAllFaqsResponse(Object response) {
        FaqsApiResponse apiResponse = (FaqsApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }

        initViewPagerFragments(apiResponse.getData().getMap());
        initViewPager();
        initViewPagerTabs(apiResponse.getData().getMap());
        setTabLayoutChangeListener();
    }

    private String getTitleBasedOnKey(String key) {
        switch (key) {
            case AppConstants.STRING_CONST.GENERAL:
                return getString(R.string.general);

            case AppConstants.STRING_CONST.REGISTRATION:
                return getString(R.string.registration);
        }
        return key;
    }

    private void initViewPager() {
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            pagerAdapter.addFragments(holder);
        }
        mVpFragContainer.setAdapter(pagerAdapter);
        mTabFragContainer.setupWithViewPager(mVpFragContainer);
    }

    private void initViewPagerTabs(HashMap<String, List<QuesAnsModel>> feeds) {
        int index = 0;
        for (String key : feeds.keySet()) {
            LinearLayout tab = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.tabs_custom_heading, null);
            CustomFontTextView heading = tab.findViewById(R.id.tv_heading);
            CustomFontTextView date = tab.findViewById(R.id.tv_date);
            date.setVisibility(View.GONE);
            AppCompatImageView indicator = tab.findViewById(R.id.iv_indicator);
            heading.setText(getTitleBasedOnKey(key));
            heading.setTextColor(index == 0 ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
            indicator.setVisibility(index == 0 ? View.VISIBLE : View.INVISIBLE);
            mTabFragContainer.getTabAt(index).setCustomView(tab);
            ++index;
        }
    }

    private void setTabLayoutChangeListener() {
        mTabFragContainer.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                for (int tabIndex = 0; tabIndex < mTabFragContainer.getTabCount(); tabIndex++) {
                    LinearLayout tabView = (LinearLayout) mTabFragContainer.getTabAt(tabIndex).getCustomView();
                    CustomFontTextView heading = tabView.findViewById(R.id.tv_heading);
                    AppCompatImageView indicator = tabView.findViewById(R.id.iv_indicator);
                    heading.setTextColor(tab.getPosition() == tabIndex ? getResourceColor(R.color.brown_1) : getResourceColor(R.color.grey_1));
                    indicator.setVisibility(tab.getPosition() == tabIndex ? View.VISIBLE : View.INVISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
