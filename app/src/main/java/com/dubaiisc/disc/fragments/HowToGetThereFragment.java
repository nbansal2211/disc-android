package com.dubaiisc.disc.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.utility.AnimUtils;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontTextView;

public class HowToGetThereFragment extends AppBaseFragment {

    private CustomFontTextView taxi, bus, metro;
    private ImageView map;
    private Button viewGoogleMap;

    public static HowToGetThereFragment newInstance() {
        return new HowToGetThereFragment();
    }

    @Override
    public void initViews() {
        viewGoogleMap = (Button) findView(R.id.btn_view_on_google_maps);
        taxi = (CustomFontTextView) findView(R.id.tv_taxi);
        bus = (CustomFontTextView) findView(R.id.tv_bus);
        metro = (CustomFontTextView) findView(R.id.tv_metro);
        map = (ImageView) findView(R.id.iv_map);

        setOnClickListener(R.id.iv_map, R.id.btn_view_on_google_maps, R.id.ib_registerNow);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Util.underLineTextViews(taxi, bus, metro);
        showMap(AppConstants.STRING_CONST.latitude, AppConstants.STRING_CONST.longitude);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_how_to_get_there;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_map:
            case R.id.btn_view_on_google_maps:
                openNativeMap(AppConstants.STRING_CONST.latitude, AppConstants.STRING_CONST.longitude);
                break;
        }
    }

    private void showMap(String latitude, String longitude) {
        String url = "https://maps.googleapis.com/maps/api/staticmap?center=" +
                "Johara+Ballroom" +
                "&zoom=16" +
                "&size=720x480" +
                "&maptype=roadmap" +
                "&markers=color:red%7C" +
                latitude + "," + longitude +
                "&key=" + AppConstants.STRING_CONST.KEY;
        Picasso.get().load(url).into(map, new Callback() {
            @Override
            public void onSuccess() {
                AnimUtils.fadeIn(500, 0, new DecelerateInterpolator(), map, viewGoogleMap);
            }

            @Override
            public void onError(Exception e) {
                Log.e("asdasd", "" + e.getMessage());
            }
        });
    }

    private void openNativeMap(String latitude, String longitude) {
        Uri gmmIntentUri = Uri.parse("geo:" + latitude + "," + longitude + "?q=Johara Ballroom" + "&z=18");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getContext().getPackageManager()) != null) {
            startActivity(mapIntent);
        }
//        String uri = String.format(Locale.ENGLISH, "geo:%s,%s", latitude, longitude);
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
//        startActivity(intent);
    }
}
