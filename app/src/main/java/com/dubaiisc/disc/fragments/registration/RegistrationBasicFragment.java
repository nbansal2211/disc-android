package com.dubaiisc.disc.fragments.registration;


import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.gender.GenderDialogFragment;
import com.dubaiisc.disc.model.agenda.GenderModel;
import com.dubaiisc.disc.model.user.UserModel;

import java.util.ArrayList;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.ValidationHelper;
import simplifii.framework.widgets.CustomFontEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationBasicFragment extends BaseRegistrationFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private GenderDialogFragment dialog;

    private ArrayList<GenderModel> list;

    public static RegistrationBasicFragment newInstance() {
        RegistrationBasicFragment basicFragment = new RegistrationBasicFragment();
        return basicFragment;
    }


    @Override
    public int getViewID() {
        return R.layout.fragment_registration_basic;
    }

    @Override
    public void initViews() {
        initFragmentViews();
        setOnClickListener(R.id.edtTxt_gender);
        createAllGenderTypes();
    }

    private void initFragmentViews() {
        addVectorDrawableOnEditText((EditText) findView(R.id.edtTxt_gender), 0, 0, R.drawable.ic_arrow_down, 0);
    }

    private void createAllGenderTypes() {
        list = new ArrayList<>();
        list.add(new GenderModel("Male", "Male"));
        list.add(new GenderModel("Female", "Female"));
        list.add(new GenderModel("Other", "Other"));
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.edtTxt_gender:
                dialog = GenderDialogFragment.newInstance(RegistrationBasicFragment.this, list, DialogListAdapter.GENDER_TYPE, R.string.select_gender);
                dialog.show(getFragmentManager(), AppConstants.STRING_CONST.GENDER);
                break;
        }
    }


    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_GENDER:
                if (dialog != null) {
                    GenderModel genderModel = (GenderModel) obj;
                    switch (dialog.getTag()) {
                        case AppConstants.STRING_CONST.GENDER:
                            setEditText(R.id.edtTxt_gender, genderModel.getGenderText());
                            break;
                    }
                    dialog.dismiss();
                }
                break;
        }
    }

    @Override
    public boolean isValid() {
        return ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_firstName).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_designation).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_gender).trim())
                && ValidationHelper.isNotEmpty(getEditText(R.id.edtTxt_org).trim());
    }

    @Override
    public void showInvalidFieldError() {
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_firstName).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_firstName)).setError(getString(R.string.field_cannot_be_blank));
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_gender).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_gender)).setError(getString(R.string.field_cannot_be_blank));
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_designation).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_designation)).setError(getString(R.string.field_cannot_be_blank));
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_org).trim()))
            ((CustomFontEditText) findView(R.id.edtTxt_org)).setError(getString(R.string.field_cannot_be_blank));
    }

    @Override
    public void apply(UserModel userModel) {
        userModel.setFirstName(getEditText(R.id.edtTxt_firstName).trim());
        userModel.setLastName(getEditText(R.id.edtTxt_lastName).trim());
        userModel.setGender(getEditText(R.id.edtTxt_gender).trim().toUpperCase()); //Note: This should be in uppercase according to API...
        userModel.setDesignation(getEditText(R.id.edtTxt_designation).trim());
        userModel.setOrganization(getEditText(R.id.edtTxt_org).trim());
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {

    }

}
