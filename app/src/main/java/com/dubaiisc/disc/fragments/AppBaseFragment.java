package com.dubaiisc.disc.fragments;


import android.content.Intent;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.users.registration.TicketRegistrationActivity;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.fragments.BaseFragment;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class AppBaseFragment extends BaseFragment {

    @Override
    public void onResume() {
        super.onResume();
        toggleVisibilityOfUserSessionViews();
    }

    private void toggleVisibilityOfUserSessionViews() {
//        updateVisibilityOfViewsById(!Preferences.isUserLoggerIn(), R.id.ib_registerNow, R.id.lyt_register);
        updateVisibilityOfViewsById(false, R.id.ib_registerNow, R.id.lyt_register);
    }

    public void addVectorDrawableOnTextView(TextView textView,
                                            @DrawableRes int left, @DrawableRes int top,
                                            @DrawableRes int right, @DrawableRes int bottom) {
        textView.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    public void addVectorDrawableOnEditText(EditText editText,
                                            @DrawableRes int left, @DrawableRes int top,
                                            @DrawableRes int right, @DrawableRes int bottom) {
        editText.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    public void updateVisibilityOfViewsById(boolean visible, @IdRes int... ids) {
        for (@IdRes int id : ids) {
            if (findView(id) != null) {
                findView(id).setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        }
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ib_registerNow:
                startNextActivity(TicketRegistrationActivity.class);
                break;
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        StyleableToast.makeText(getContext(), re != null ? re.getMessage() : getString(R.string.bkg_error), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
    }

    protected void changeLanguage() {
        LocalHelperUtility.setLocale(getActivity(), LocalHelperUtility.getLanguage()
                .equalsIgnoreCase(FrameworkConstants.LANGUAGE.AR)
                ? FrameworkConstants.LANGUAGE.EN
                : FrameworkConstants.LANGUAGE.AR);
        //Need to restart the app again ......
//        recreate();
        restartApp();
    }

    private void restartApp() {
        Intent intent = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();
    }


}
