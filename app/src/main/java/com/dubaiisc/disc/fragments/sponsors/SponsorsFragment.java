package com.dubaiisc.disc.fragments.sponsors;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.fragments.AppBaseFragment;
import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.model.SectionHeaderModel;
import com.dubaiisc.disc.model.sponsors.SponsorApiResponse;
import com.dubaiisc.disc.model.sponsors.SponsorModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import simplifii.framework.utility.FrameworkConstants;

public class SponsorsFragment extends AppBaseFragment implements BaseRecycleAdapter.RecyclerClickInterface {

    private RecyclerView mRvSponsorList;

    public static Fragment newInstance() {
        SponsorsFragment fragment = new SponsorsFragment();
        return fragment;
    }

    @Override
    public void initViews() {
        mRvSponsorList = (RecyclerView) findView(R.id.rv_sponsorList);
        setOnClickListener(R.id.ib_registerNow);
    }

    private void initSponsorAdapter(List<BaseAdapterModel> sponsorModelList) {
        final BaseRecycleAdapter<BaseAdapterModel> adapter = new BaseRecycleAdapter<>(activity, sponsorModelList);
        mRvSponsorList.setAdapter(adapter);
        adapter.setClickInterface(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case AppConstants.VIEW_CODES.SECTION_HEADER:
                        return 2;
                    case AppConstants.VIEW_CODES.SPONSOR:
                        return 1;
                    default:
                        return -1;
                }
            }
        });
        mRvSponsorList.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllSponsors();
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_sponsors;
    }


    private void getAllSponsors() {
        executeTask(FrameworkConstants.TASK_CODES.SPONSOR, ApiRequestGenerator.onGetSponsor());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);

        SponsorApiResponse apiResponse = (SponsorApiResponse) response;

        if (apiResponse.getError()) {
            Toast.makeText(activity, apiResponse.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        List<BaseAdapterModel> list = new ArrayList<>();
        HashMap<String, List<SponsorModel>> allSponsersDataMap = new HashMap<>();
        for (String key : apiResponse.getMap().keySet()) {

            List<SponsorModel> sponsorModelsList = apiResponse.getMap().get(key);
            for (SponsorModel s : sponsorModelsList) {
                String localeTranslatedType = s.getLocaleTranslatedType(getActivity());
                if (allSponsersDataMap.containsKey(localeTranslatedType)) {
                    List<SponsorModel> sponsorModelList = allSponsersDataMap.get(localeTranslatedType);
                    sponsorModelList.add(s);
                    allSponsersDataMap.put(localeTranslatedType, sponsorModelList);
                } else {
                    List<SponsorModel> sponsorModelList = new ArrayList<>();
                    sponsorModelList.add(s);
                    allSponsersDataMap.put(localeTranslatedType, sponsorModelList);
                }
            }
        }

        ArrayList<String> keys = new ArrayList<>(allSponsersDataMap.keySet());
        for (int i = keys.size() - 1; i >= 0; i--) {
            String key = keys.get(i);
            list.add(new SectionHeaderModel(key));
            list.addAll(allSponsersDataMap.get(key));
        }
//        for (String key : allSponsersDataMap.keySet()) {
//            list.add(new SectionHeaderModel(key));
//            list.addAll(allSponsersDataMap.get(key));
//
//        }
        initSponsorAdapter(list);
    }

    private HashMap<String, List<SponsorModel>> setNewList(HashMap<String, List<SponsorModel>> list) {
        List<List<SponsorModel>> valuesTMP = new ArrayList<>(list.values());
        List<String> keysTMP = new ArrayList<>(list.keySet());
        Collections.reverse(valuesTMP);
        Collections.reverse(keysTMP);
        int index = 0;
        HashMap<String, List<SponsorModel>> newList = new HashMap<>();
        for (String key : keysTMP) {
            newList.put(key, valuesTMP.get(index));
            index++;
        }
        return newList;
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_SPONSOR:
                onSponcorItemSelected(obj);
                break;
        }
    }

    private void onSponcorItemSelected(Object obj) {
        SponsorModel sponsorModel = (SponsorModel) obj;
        if (sponsorModel != null && !TextUtils.isEmpty(sponsorModel.getWebsiteURL())) {
            openLinkOnBrowser(sponsorModel.getWebsiteURL());
        }
    }

}
