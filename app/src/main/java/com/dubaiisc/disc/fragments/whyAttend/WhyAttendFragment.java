package com.dubaiisc.disc.fragments.whyAttend;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.dubaiisc.disc.fragments.AppBaseFragment;

import com.dubaiisc.disc.R;

public class WhyAttendFragment extends AppBaseFragment {

    public static WhyAttendFragment newInstance() {
        return new WhyAttendFragment();
    }

    @Override
    public void initViews() {
        setOnClickListener(R.id.ib_registerNow);
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_why_attend;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
