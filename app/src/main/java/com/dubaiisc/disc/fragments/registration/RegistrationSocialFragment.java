package com.dubaiisc.disc.fragments.registration;


import android.app.Fragment;
import android.view.View;

import com.dubaiisc.disc.model.user.UserModel;

import com.dubaiisc.disc.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationSocialFragment extends BaseRegistrationFragment {

    @Override
    public void initViews() {

    }


    @Override
    public int getViewID() {
        return R.layout.fragment_registration_social;
    }


    public static RegistrationSocialFragment newInstance() {
        RegistrationSocialFragment socialFragment = new RegistrationSocialFragment();
        return socialFragment;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void showInvalidFieldError() {

    }

    @Override
    public void apply(UserModel userModel) {
        userModel.setWebsiteLink(getEditText(R.id.edtTxt_website).trim());
        userModel.setLinkedInLink(getEditText(R.id.edtTxt_linkedInProfile).trim());
        userModel.setFacebookLink(getEditText(R.id.edtTxt_facebookProfile).trim());
        userModel.setTwitterLink(getEditText(R.id.edtTxt_twitterProfile).trim());
        userModel.setAdditionalLink("");
        userModel.setAbout(getEditText(R.id.edtTxt_aboutMe).trim());
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {

    }
}
