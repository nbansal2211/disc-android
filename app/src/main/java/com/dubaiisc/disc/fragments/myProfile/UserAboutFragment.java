package com.dubaiisc.disc.fragments.myProfile;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.adapter.DialogListAdapter;
import com.dubaiisc.disc.dialogs.country.CountryDialogFragment;
import com.dubaiisc.disc.dialogs.gender.GenderDialogFragment;
import com.dubaiisc.disc.model.agenda.GenderModel;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserModel;

import java.util.ArrayList;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.StringUtility;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserAboutFragment extends BaseMyProfileFragment implements BaseRecycleAdapter.RecyclerClickInterface {


    //    private EditText firstName, lastName, designation, org, country, city, aboutMe;
    private View.OnClickListener mListenerCallback;
    //    private UserModel model;
    private ArrayList<CountryModel> countryList = new ArrayList<>();
    private ArrayList<GenderModel> genderlist = new ArrayList<>();
    private CountryDialogFragment countryDialog;
    private GenderDialogFragment genderDialog;

    public static UserAboutFragment newInstance() {
        UserAboutFragment fragment = new UserAboutFragment();
        return fragment;
    }

    @Override
    public int getViewID() {
        return R.layout.fragment_user_about;
    }

    @Override
    public void initViews() {
        findView(R.id.fragment_user_mainLayout).setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0));//right to left
        addVectorDrawableOnEditText((EditText) findView(R.id.edtTxt_country), 0, 0, R.drawable.ic_arrow_down, 0);
        setOnClickListener(R.id.btn_save_connect_changes, R.id.edtTxt_country, R.id.edtTxt_gender);
        createAllGenderTypes();
    }

    private void createAllGenderTypes() {
        genderlist.add(new GenderModel("Male", "Male"));
        genderlist.add(new GenderModel("Female", "Female"));
        genderlist.add(new GenderModel("Other", "Other"));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void showInvalidFieldError() {

    }

    @Override
    public void apply(UserModel model) {
        if (model != null) {
            model.setFirstName(getEditText(R.id.edtTxt_firstName));
            model.setMiddleName(getEditText(R.id.edtTxt_middleName));
            model.setLastName(getEditText(R.id.edtTxt_lastName));
            model.setGender(getEditText(R.id.edtTxt_gender).toUpperCase());
            model.setDesignation(getEditText(R.id.edtTxt_designation));
            model.setOrganization(getEditText(R.id.edtTxt_org));
            model.setCountry(getEditText(R.id.edtTxt_country));
            model.setCity(getEditText(R.id.edtTxt_city));
            model.setAbout(getEditText(R.id.edtTxt_aboutMe));
            model.setConsentForNetworking(((CheckBox) findView(R.id.cb_consentForNetworking)).isChecked());
        }
    }

    @Override
    public void setModelToViews(UserModel model) {
//        this.model = model;
        if (model != null) {
            setEditText(R.id.edtTxt_firstName, model.getFirstName());
            setEditText(R.id.edtTxt_middleName, model.getMiddleName());
            setEditText(R.id.edtTxt_lastName, model.getLastName());
            setEditText(R.id.edtTxt_gender, StringUtility.getFirstCharUppercase(model.getGender()));
            setEditText(R.id.edtTxt_designation, model.getDesignation());
            setEditText(R.id.edtTxt_org, model.getOrganization());
            setEditText(R.id.edtTxt_country, model.getCountry());
            setEditText(R.id.edtTxt_city, model.getCity());
            setEditText(R.id.edtTxt_aboutMe, model.getAbout());
            ((CheckBox) findView(R.id.cb_consentForNetworking)).setChecked(model.isConsentForNetworking());
        }
    }

    @Override
    public void setClickListenerCallback(View.OnClickListener listenerCallback) {
        mListenerCallback = listenerCallback;
    }

    @Override
    public void onCountryListFetched(final ArrayList<CountryModel> list) {
        this.countryList = list;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.edtTxt_country:
                if (countryList != null) {
                    countryDialog = CountryDialogFragment.newInstance(UserAboutFragment.this, countryList,
                            DialogListAdapter.COUNTRY_NAME, R.string.select_country);
                    countryDialog.show(getFragmentManager(), AppConstants.STRING_CONST.COUNTRY);
                }
                break;
            case R.id.edtTxt_gender:
                genderDialog = GenderDialogFragment.newInstance(UserAboutFragment.this, genderlist, DialogListAdapter.GENDER_TYPE, R.string.select_gender);
                genderDialog.show(getFragmentManager(), AppConstants.STRING_CONST.GENDER);
                break;

            case R.id.btn_save_connect_changes:
//                model.setFirstName(getEditText(R.id.edtTxt_firstName));
//                model.setMiddleName(getEditText(R.id.edtTxt_middleName));
//                model.setLastName(getEditText(R.id.edtTxt_lastName));
//                model.setGender(getEditText(R.id.edtTxt_gender).toUpperCase());
//                model.setDesignation(getEditText(R.id.edtTxt_designation));
//                model.setOrganization(getEditText(R.id.edtTxt_org));
//                model.setCountry(getEditText(R.id.edtTxt_country));
//                model.setCity(getEditText(R.id.edtTxt_city));
//                model.setAbout(getEditText(R.id.edtTxt_aboutMe));
//                model.setConsentForNetworking(((CheckBox) findView(R.id.cb_consentForNetworking)).isChecked());
                mListenerCallback.onClick(v);
                break;
        }
    }

    @Override
    public void onItemClick(View itemView, int position, Object obj, int actionType) {
        switch (actionType) {
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_NAME:
            case AppConstants.ACTION_TYPE.SELECT_COUNTRY_ISD:
                if (countryDialog != null) {
                    CountryModel countryModel = (CountryModel) obj;
                    switch (countryDialog.getTag()) {
                        case AppConstants.STRING_CONST.COUNTRY:
                            setEditText(R.id.edtTxt_country, countryModel.getName());
//                            model.setCountry(countryModel.getName());
                    }
                    countryDialog.dismiss();
                }
            case AppConstants.ACTION_TYPE.SELECT_GENDER:
                if (genderDialog != null) {
                    GenderModel genderModel = (GenderModel) obj;
                    switch (genderDialog.getTag()) {
                        case AppConstants.STRING_CONST.GENDER:
                            setEditText(R.id.edtTxt_gender, genderModel.getGenderText());
                            break;
                    }
                    genderDialog.dismiss();
                }
                break;
        }
    }
}
