package com.dubaiisc.disc.fragments.home;

import android.view.View;

public interface HomeNavigator {
    void onItemClickCallback(View view);
}
