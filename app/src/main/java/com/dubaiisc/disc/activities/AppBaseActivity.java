package com.dubaiisc.disc.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.users.registration.TicketRegistrationActivity;
import com.dubaiisc.disc.utilities.AppConstants;
import simplifii.framework.activity.BaseActivity;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class AppBaseActivity extends BaseActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocalHelperUtility.onAttach(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//Support Full screen view...
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true); //Support Vector Drawable...
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        toggleVisibilityOfUserSessionViews(); //Update User base session views...
        setToolbarNotificationCount();
    }

    /*
     * This Method should only use for toggle the view which are dependent on user signIn or singOut
     * */

    protected void toggleVisibilityOfUserSessionViews() {
//        updateVisibilityOfViewsById(!Preferences.isUserLoggerIn(), R.id.ib_registerNow, R.id.lyt_register);
        updateVisibilityOfViewsById(false, R.id.ib_registerNow, R.id.lyt_register);
    }

    public void addVectorDrawableOnTextView(AppCompatTextView textView,
                                            @DrawableRes int left, @DrawableRes int top,
                                            @DrawableRes int right, @DrawableRes int bottom) {
        textView.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
    }

    public void updateVisibilityOfViewsById(boolean shouldVisible, @IdRes int... ids) {
        for (@IdRes int id : ids) {
            if (findViewById(id) != null) {
                findViewById(id).setVisibility(shouldVisible ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void setToolbarNotificationCount() {
        int unreadNotificationCount = Preferences.getData(AppConstants.STRING_CONST.UNREAD_NOTIFICATION_COUNT, 0);
        if (toolbar != null) {
            try {
                toolbar.findViewById(R.id.iv_bkgNotificationCount).setVisibility(unreadNotificationCount > 0 ? View.VISIBLE : View.INVISIBLE);
                toolbar.findViewById(R.id.tv_notificationCount).setVisibility(unreadNotificationCount > 0 ? View.VISIBLE : View.INVISIBLE);
                ((TextView) toolbar.findViewById(R.id.tv_notificationCount)).setText(unreadNotificationCount > 0 ? "" + unreadNotificationCount : "");
            } catch (NullPointerException ignored) {
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ib_registerNow:
                startNextActivity(TicketRegistrationActivity.class);
                break;
        }
    }

    protected void changeLanguage() {
        LocalHelperUtility.setLocale(this, LocalHelperUtility.getLanguage()
                .equalsIgnoreCase(FrameworkConstants.LANGUAGE.AR)
                ? FrameworkConstants.LANGUAGE.EN
                : FrameworkConstants.LANGUAGE.AR);
        //Need to restart the app again ......
//        recreate();
        restartApp();
    }

    private void restartApp() {
        Intent intent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
        StyleableToast.makeText(this, re != null ? re.getMessage() : getString(R.string.bkg_error), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
    }
}
