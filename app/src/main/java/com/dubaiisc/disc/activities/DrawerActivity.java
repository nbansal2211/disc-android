package com.dubaiisc.disc.activities;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.users.registration.WebViewActivity;
import com.dubaiisc.disc.dialogs.language.SelectLanguageDialogFragment;
import com.dubaiisc.disc.model.drawer.DrawerChildData;
import com.dubaiisc.disc.model.drawer.DrawerDataHelper;
import com.dubaiisc.disc.model.drawer.DrawerMainData;
import com.dubaiisc.disc.utilities.AppConstants;
import com.yarolegovich.slidingrootnav.SlideGravity;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.ListAdapters.CustomExListAdapter;
import simplifii.framework.ListAdapters.CustomExpListInterface;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;

public abstract class DrawerActivity extends AppBaseActivity implements CustomExpListInterface {

    private SlidingRootNav mSlidingRootNav;
    private ExpandableListView mRootNavExpListView;

    private CustomExListAdapter mRootNavListAdapter;
    private List<DrawerMainData> drawerDataList = new ArrayList<>();
//    private boolean sessionChanged;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        initDrawerData();
        initSlidingView(savedInstanceState);
        initDrawerList();
        setOnClickListener(R.id.iv_close, R.id.tv_toggleLoginLogout, R.id.tv_toggleLangauge);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        checkForUserSessionViews(Preferences.isUserLoggerIn());
        updateDrawerDataListOnSessionChanged();
        toggleVisibilityOfUserSessionViews();
    }

    private void checkForUserSessionViews(boolean userLoggerIn) {
//        ((CustomFontTextView) findViewById(R.id.tv_toggleLoginLogout)).setText(userLoggerIn ? R.string.log_out : R.string.sign_in);
    }

    private void initDrawerData() {
        drawerDataList.addAll(new DrawerDataHelper().getSlideNavigationDrawerItemList(this));
    }

    private void initSlidingView(Bundle savedInstanceState) {
        mSlidingRootNav = new SlidingRootNavBuilder(this)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(true)
                .withGravity((getResources().getBoolean(R.bool.isRightToLeft) ? SlideGravity.RIGHT : SlideGravity.LEFT))//Check if right to left, the change gravity accordingly...
                .withSavedState(savedInstanceState)
                .withContentClickableWhenMenuOpened(false)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();
    }

    private void initDrawerList() {
        mRootNavExpListView = findViewById(R.id.expand_list_view);
        mRootNavListAdapter = new CustomExListAdapter(drawerDataList, this);
        mRootNavExpListView.setAdapter(mRootNavListAdapter);
    }

    private void updateDrawerDataListOnSessionChanged() {
        drawerDataList.clear();
        initDrawerData();
        mRootNavListAdapter.notifyDataSetChanged();
    }

    protected boolean isMenuDrawerOpened() {
        return mSlidingRootNav != null && mSlidingRootNav.isMenuOpened();
    }

    protected void onCloseMenuDrawer() {
        if (isMenuDrawerOpened())
            mSlidingRootNav.closeMenu();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_close:
                onCloseMenuDrawer();
                break;
            case R.id.tv_toggleLoginLogout:
                onToggleLoginLogoutClicked();
                break;
            case R.id.tv_toggleLangauge:
                onToggleLanguageClicked();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mSlidingRootNav.isMenuClosed())
                mSlidingRootNav.openMenu();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onToggleLoginLogoutClicked() {
        startWebViewActivity();
//        if (Preferences.isUserLoggerIn()) {
//            Preferences.saveData(LOGIN_KEY, false);
//            updateDrawerDataListOnSessionChanged();
//            checkForUserSessionViews(isUserLoggerIn());
//            toggleVisibilityOfUserSessionViews();
//            mSlidingRootNav.closeMenu();
//        } else {
//            mSlidingRootNav.closeMenu(false);
//            startNextActivity(SignInActivity.class);
//        }
    }

    private void startWebViewActivity() {
        mSlidingRootNav.closeMenu(false);
        Bundle bundle = new Bundle();
        bundle.putString(FrameworkConstants.BUNDLE_KEYS.EXTRA_VALUE, AppConstants.EXTRAS.REGISTER_NOW);
        startNextActivity(bundle, WebViewActivity.class);
    }

    private void onToggleLanguageClicked() {
        openDailougeForSelectLanguage();
//        changeLanguage();
    }

    private void openDailougeForSelectLanguage() {
        SelectLanguageDialogFragment instance = SelectLanguageDialogFragment.getInstance();
//        instance.setStyle( DialogFragment.STYLE_NORMAL, R.style.You_Dialog );
        instance.show(getSupportFragmentManager(), " ");
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ChildHolder holder;
        LayoutInflater layoutInflater = getLayoutInflater();
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_drawer_child, parent, false);
            holder = new ChildHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ChildHolder) convertView.getTag();
        }
        final DrawerChildData drawerChildData = drawerDataList.get(groupPosition).getChildData().get(childPosition);
        holder.tvTitle.setText(drawerChildData.getTitle());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildItemClicked(drawerChildData);
                //Now close the menu drawer...
                onCloseMenuDrawer();
            }
        });
        return convertView;
    }


    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
        final GroupHolder holder;
        LayoutInflater layoutInflater = getLayoutInflater();
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_drawer_group, parent, false);
            holder = new GroupHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }
        final DrawerMainData drawerMainData = drawerDataList.get(groupPosition);
        holder.tvTitle.setText(drawerMainData.getTitle());
        if (CollectionUtils.isEmpty(drawerMainData.getChildData())) {
            holder.ivArrow.setVisibility(View.GONE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onGroupItemClicked(drawerMainData);
                    //Now close the menu drawer...
                    onCloseMenuDrawer();
                }
            });
        } else {
            holder.ivArrow.setVisibility(View.VISIBLE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isExpanded) mRootNavExpListView.collapseGroup(groupPosition);
                    else mRootNavExpListView.expandGroup(groupPosition);
                }
            });
        }
        return convertView;
    }

    @Override
    public List<DrawerChildData> getChildList(int groupPosition) {
        if (CollectionUtils.isNotEmpty(drawerDataList.get(groupPosition).getChildData())) {
            return drawerDataList.get(groupPosition).getChildData();
        }
        return null;
    }

    private class GroupHolder {
        TextView tvTitle;
        ImageView ivArrow;

        GroupHolder(View convertView) {
            tvTitle = convertView.findViewById(R.id.tv_title);
            ivArrow = convertView.findViewById(R.id.iv_arrow);
        }
    }

    private class ChildHolder {
        TextView tvTitle;

        ChildHolder(View convertView) {
            tvTitle = convertView.findViewById(R.id.tv_title);
        }
    }

    public abstract void onGroupItemClicked(DrawerMainData drawerMainData);

    public abstract void onChildItemClicked(DrawerChildData drawerChildData);


}
