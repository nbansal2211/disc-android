package com.dubaiisc.disc.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewApiResponse;
import com.dubaiisc.disc.model.yearInReview.YearsInReviewModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class YearsInReviewActivity extends AppBaseActivity {

    private RecyclerView mRvYearsInReview;

    private BaseRecycleAdapter<YearsInReviewModel> mYearsInReviewAdapter;
    private List<YearsInReviewModel> mYearsInReviewModelList = new ArrayList<>();

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_years_in_review);

        initViews();
        initToolBar("");
        setRvYearsInReviewAdapter();
        setOnClickListener(R.id.ib_registerNow);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        onGetYearInReviewsData();
    }

    private void initViews() {
        mRvYearsInReview = findViewById(R.id.rv_yearsInReview);
    }

    private void setRvYearsInReviewAdapter() {
        mYearsInReviewAdapter = new BaseRecycleAdapter<>(YearsInReviewActivity.this, mYearsInReviewModelList);
        mRvYearsInReview.setLayoutManager(new LinearLayoutManager(YearsInReviewActivity.this));
        mRvYearsInReview.setAdapter(mYearsInReviewAdapter);
    }

    private void updateRvYearsInReviewAdapter(List<YearsInReviewModel> list) {
        mYearsInReviewModelList.clear();
        mYearsInReviewModelList.addAll(list);
        mYearsInReviewAdapter.notifyDataSetChanged();
    }

    private void onGetYearInReviewsData() {
        executeTask(FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW, ApiRequestGenerator.onGetYearsInReview());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.YEAR_IN_REVIEW:
                onGetYearInReviewsDataResponse(response);
                break;
        }
    }

    private void onGetYearInReviewsDataResponse(Object response) {
        YearsInReviewApiResponse apiResponse = (YearsInReviewApiResponse) response;
        if (apiResponse.getError()) {
            StyleableToast.makeText(YearsInReviewActivity.this, "", Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }
//        List<YearsInReviewModel> list = new ArrayList<>();
        if (apiResponse.getYearsInReview() != null && CollectionUtils.isNotEmpty(apiResponse.getYearsInReview().getYearsInReviewList())) {
//            list.add(apiResponse.getYearsInReview().getYearsInReviewList().get(apiResponse.getYearsInReview().getYearsInReviewList().size()-1));
//            updateRvYearsInReviewAdapter(list);
            updateRvYearsInReviewAdapter(removeLastItem(apiResponse.getYearsInReview().getYearsInReviewList()));
        }

    }

    private List<YearsInReviewModel> removeLastItem(List<YearsInReviewModel> list) {
        if (list != null && list.size() > 0) list.remove(list.size() - 1);
        return list;
    }
}
