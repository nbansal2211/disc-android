package com.dubaiisc.disc.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.users.ProfileActivity;
import com.dubaiisc.disc.activities.users.registration.TicketRegistrationActivity;
import com.dubaiisc.disc.fragments.ContactUsFragment;
import com.dubaiisc.disc.fragments.HowToGetThereFragment;
import com.dubaiisc.disc.fragments.TermsAndConditionsFragment;
import com.dubaiisc.disc.fragments.WhileInDubaiFragment;
import com.dubaiisc.disc.fragments.about.AboutFragment;
import com.dubaiisc.disc.fragments.agenda.AgendaFragment;
import com.dubaiisc.disc.fragments.gallery.GalleryFragment;
import com.dubaiisc.disc.fragments.home.HomeFragment;
import com.dubaiisc.disc.fragments.home.HomeNavigator;
import com.dubaiisc.disc.fragments.media.MediaFragment;
import com.dubaiisc.disc.fragments.networking.NetworkingFragment;
import com.dubaiisc.disc.fragments.speakers.SpeakersFragment;
import com.dubaiisc.disc.fragments.sponsors.SponsorsFragment;
import com.dubaiisc.disc.fragments.whyAttend.WhyAttendFragment;
import com.dubaiisc.disc.model.SaveUserDeviceResponse;
import com.dubaiisc.disc.model.drawer.DrawerChildData;
import com.dubaiisc.disc.model.drawer.DrawerMainData;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.services.PostDeviceInfoIntentServices;
import com.dubaiisc.disc.utilities.AppConstants;
import com.google.firebase.iid.FirebaseInstanceId;

import simplifii.framework.asyncmanager.HttpParamObject;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;

public class MainActivity extends DrawerActivity implements HomeNavigator {

    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolBar("");
        setDefaultFragment();
        setOnClickListener(R.id.cl_notification);
        sendDeviceInfoToServer();
        setStatusBarColor(getResources().getColor(R.color.white));
        if (!Preferences.isUserDeviceInfoSaveed()){
            sendNotificationId();
        }
    }

    private void sendNotificationId() {
        if (FirebaseInstanceId.getInstance().getToken() == null) {
            return;
        }
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("NotificationToken", pushToken);
        HttpParamObject httpParamObject = ApiRequestGenerator.sendUserDeviceInfos(pushToken, Util.getAndroidId(this));
        executeTask(FrameworkConstants.TASK_CODES.SEND_USER_DEVICE_TOKEN, httpParamObject);
    }

    @Override
    public void onPreExecute(int taskCode) {
        super.onPreExecute(taskCode);
        hideProgressBar();
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        if (response ==null){
            return;
        }
        switch (taskCode){
            case FrameworkConstants.TASK_CODES.SEND_USER_DEVICE_TOKEN:
                SaveUserDeviceResponse saveUserDeviceResponse = (SaveUserDeviceResponse) response;
                if (!saveUserDeviceResponse.getError()){
                    Preferences.saveData(Preferences.SAVE_USER_DEVICE_INFO, true);
                }
                break;
        }
    }

    private void sendDeviceInfoToServer() {
        if (!TextUtils.isEmpty(Preferences.getData(AppConstants.STRING_CONST.FCM_TOKEN, ""))
                && !Preferences.getData(AppConstants.STRING_CONST.DEVICE_INFO_UPLOADED, false)) {
            Intent intent = new Intent(getApplicationContext(), PostDeviceInfoIntentServices.class);
            startService(intent);
        }
    }

    private void setDefaultFragment() {
        addFragmentReplace(R.id.container, HomeFragment.newInstance(this), false);
    }

    @Override
    protected int getHomeIcon() {

        return R.drawable.ic_open_menu_t;
    }

    @Override
    protected void onHomePressed() {
//        drawerOperation();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;
        }

    }

    @Override
    public void onGroupItemClicked(DrawerMainData drawerMainData) {
        switch (drawerMainData.getEventType()) {
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.HOME:
                addFragmentReplace(R.id.container, HomeFragment.newInstance(this), false);
                break;
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.SPONSOR:
                addFragmentReplace(R.id.container, SponsorsFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.NETWORKING:
                addFragmentReplace(R.id.container, NetworkingFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.MEDIA:
                addFragmentReplace(R.id.container, MediaFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.CONTACT_US:
                addFragmentReplace(R.id.container, ContactUsFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_GROUP_EVENT_TYPE.MY_PROFILE:
                startNextActivity(ProfileActivity.class);
                break;
        }
    }

    @Override
    public void onChildItemClicked(DrawerChildData drawerChildData) {
        switch (drawerChildData.getEventType()) {
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.ABOUT:
                addFragmentReplace(R.id.container, AboutFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.WHY_ATTEND:
                addFragmentReplace(R.id.container, WhyAttendFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.AGENDA:
                addFragmentReplace(R.id.container, AgendaFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.SPEAKERS:
                addFragmentReplace(R.id.container, SpeakersFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.GALLERY:
                addFragmentReplace(R.id.container, GalleryFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.YEARS_IN_REVIEW:
                startNextActivity(YearsInReviewActivity.class);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.HOW_TO_GET_THERE:
                addFragmentReplace(R.id.container, HowToGetThereFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.WHILE_IN_DUBAI:
                addFragmentReplace(R.id.container, WhileInDubaiFragment.newInstance(), false);
                break;
            case AppConstants.DRAWER_CHILD_EVENT_TYPE.FAQS:
//                addFragmentReplace(R.id.container, FaqsFragment.newInstance(), false);
                addFragmentReplace(R.id.container, TermsAndConditionsFragment.newInstance(), false);
                break;
        }
    }

    @Override
    public void onItemClickCallback(View view) {
        switch (view.getId()) {
            case R.id.lyt_register:
                startNextActivity(TicketRegistrationActivity.class);
                break;
            case R.id.iv_speakers:
                addFragmentReplace(R.id.container, SpeakersFragment.newInstance(), true);
                break;

            case R.id.iv_sponser:
                addFragmentReplace(R.id.container, SponsorsFragment.newInstance(), true);
                break;

            case R.id.iv_about:
                addFragmentReplace(R.id.container, AboutFragment.newInstance(), true);
                break;

            case R.id.iv_agenda:
                addFragmentReplace(R.id.container, AgendaFragment.newInstance(), true);
                break;

            case R.id.iv_why_attend:
                addFragmentReplace(R.id.container, WhyAttendFragment.newInstance(), true);
                break;
        }
    }

    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (isMenuDrawerOpened()) {
            onCloseMenuDrawer();
            return;
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        Fragment fragmentHome = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fragmentHome != null) {
            if (fragmentHome instanceof HomeFragment){
                checkDoubleClick();
                return;
            }else {
                getSupportFragmentManager().beginTransaction().remove(fragmentHome).commit();
                setDefaultFragment();
                return;
            }

        }

        checkDoubleClick();
    }

    private void checkDoubleClick() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }

        doubleBackToExitPressedOnce = true;
        showToast(R.string.back_press_text);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 5000);
    }

    //    @Override
//    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            finish();
//            return;
//        }
////        if (mSlidingRootNav != null && mSlidingRootNav.isMenuOpened()) {
////            mSlidingRootNav.closeMenu();
////            return;
////        } else if (mSlidingRootNav != null && !mSlidingRootNav.isMenuOpened()) {
////            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
////                super.onBackPressed();
////                return;
////            }
////        }
//
//        this.doubleBackToExitPressedOnce = true;
//        showToast("Please click BACK again to exit");
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 5000);
//    }


}
