package com.dubaiisc.disc.activities.users.registration;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.ActivityContainer;
import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.NotificationActivity;
import com.dubaiisc.disc.adapter.RegistrationViewPagerAdapter;
import com.dubaiisc.disc.customViews.NonSwipeableViewPager;
import com.dubaiisc.disc.fragments.registration.RegistrationAccountSetupFragment;
import com.dubaiisc.disc.fragments.registration.RegistrationBasicFragment;
import com.dubaiisc.disc.fragments.registration.RegistrationGeneralFragment;
import com.dubaiisc.disc.fragments.registration.RegistrationSocialFragment;
import com.dubaiisc.disc.model.ProfilePictureUploadApiResponse;
import com.dubaiisc.disc.model.tickets.TicketModel;
import com.dubaiisc.disc.model.user.UserApiResponse;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class RegistrationActivity extends AppBaseActivity {
    private final int MIN_VIEW_PAGER_COUNT = 0;

    private NonSwipeableViewPager mVpFragContainer;

    private List<RegistrationViewPagerAdapter.FragmentModelHolder> mListFragmentHolder = new ArrayList<>();
    private UserModel mUserModel = new UserModel();
    private TicketModel mTicketModel;
    private int mViewPagerCurrentItemIndex = MIN_VIEW_PAGER_COUNT;

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getTicketModelFromBundle(getIntent());

        initToolBar("");
        initViews();
        initViewPagerFragments();
        initViewPager();
        setOnClickListener(R.id.vw_next, R.id.vw_back, R.id.cl_notification);
    }

    private void getTicketModelFromBundle(Intent intent) {
        if (intent != null && intent.hasExtra(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL)) {
            mTicketModel = intent.getParcelableExtra(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL);
            mUserModel.setTicketNumber(mTicketModel.getNumber()); //save ticket number to model....
        }
    }

    private void initViews() {
        mVpFragContainer = findViewById(R.id.vp_registrationFragContainer);
        setNavTextsDrawables();
    }

    //Set drawable start and end to the back and next text views
    private void setNavTextsDrawables() {
        CustomFontTextView back, next;
        back = findViewById(R.id.vw_back);
        next = findViewById(R.id.vw_next);
        if (back != null)
            addVectorDrawableOnTextView(back, R.drawable.ic_backward_arrow, 0, 0, 0);
        if (next != null)
            addVectorDrawableOnTextView(next, 0, 0, R.drawable.ic_forward_arrow, 0);
    }

    //Add fragments to list...
    private void initViewPagerFragments() {
        mListFragmentHolder.add(new RegistrationViewPagerAdapter.FragmentModelHolder(RegistrationBasicFragment.newInstance(), ""));
        mListFragmentHolder.add(new RegistrationViewPagerAdapter.FragmentModelHolder(RegistrationGeneralFragment.newInstance(), ""));
        mListFragmentHolder.add(new RegistrationViewPagerAdapter.FragmentModelHolder(RegistrationSocialFragment.newInstance(), ""));
        mListFragmentHolder.add(new RegistrationViewPagerAdapter.FragmentModelHolder(RegistrationAccountSetupFragment.newInstance(), ""));
    }

    //Set fragments to view pager....
    private void initViewPager() {
        RegistrationViewPagerAdapter pagerAdapter = new RegistrationViewPagerAdapter(getSupportFragmentManager());
        for (RegistrationViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            holder.getFragment().setClickListenerCallback(this);
            pagerAdapter.addFragments(holder);
        }
        mVpFragContainer.setAdapter(pagerAdapter);
        mVpFragContainer.setOffscreenPageLimit(4);
    }

    private void swipeViewPager(boolean toNext) {
        if (toNext && mViewPagerCurrentItemIndex < mListFragmentHolder.size() - 1)
            mVpFragContainer.setCurrentItem(++mViewPagerCurrentItemIndex);
        else if (mViewPagerCurrentItemIndex > MIN_VIEW_PAGER_COUNT)
            mVpFragContainer.setCurrentItem(--mViewPagerCurrentItemIndex);
        //Show-hide change buttons...
        findViewById(R.id.vw_next).setVisibility(mViewPagerCurrentItemIndex == mListFragmentHolder.size() - 1 ? View.INVISIBLE : View.VISIBLE);
        findViewById(R.id.vw_back).setVisibility(mViewPagerCurrentItemIndex == 0 ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.vw_next:
                onNextClicked();
                break;
            case R.id.vw_back:
                onBackClicked();
                break;
            case R.id.btn_submit:
                onSubmitButtonClicked();
                break;

            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;

            case R.id.tv_privacy_and_security:
            case R.id.tv_terms_and_conditions:
                ActivityContainer.startActivity(this,
                        AppConstants.FRAGMENT_TYPE.TERMS_AND_CONDITIONS, null);
                break;
        }
    }

    private void onNextClicked() {
        if (mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().isValid()) {
            mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().apply(mUserModel);
            swipeViewPager(true);
        } else {
            mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().showInvalidFieldError();
        }
    }

    private void onBackClicked() {
        swipeViewPager(false);
    }

    private void onSubmitButtonClicked() {
        //Registration Fragment callback will get here....
        if (mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().isValid()) {
            mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().apply(mUserModel);
            if (mUserModel.getImageFile() != null)
                executeTask(FrameworkConstants.TASK_CODES.PROFILE_PIC_UPLOAD, ApiRequestGenerator.onPostUserProfilePic(mUserModel.getImageFile(), mUserModel.getImageFile().getName()));
            else
                onPostUserRegistration(mUserModel);
        } else {
            mListFragmentHolder.get(mViewPagerCurrentItemIndex).getFragment().showInvalidFieldError();
        }
    }

    private void onPostUserRegistration(UserModel userModel) {
        executeTask(FrameworkConstants.TASK_CODES.REGISTRATION, ApiRequestGenerator.onPostRegistration(userModel));
    }

//    private void onPostUserSignIn(UserModel userModel) {
//        SignInModel signInModel = new SignInModel();
//        signInModel.setEmail(userModel.getEmail());
//        signInModel.setPassword(userModel.getPassword());
//        executeTask(FrameworkConstants.TASK_CODES.SIGN_IN, ApiRequestGenerator.onSignIn(signInModel));
//    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.REGISTRATION:
                postUserRegistrationResponse(response);
                break;
//            case FrameworkConstants.TASK_CODES.SIGN_IN:
//                postUserSignInResponse(response);
//                break;
            case FrameworkConstants.TASK_CODES.PROFILE_PIC_UPLOAD:
                onPostUserProfilePictureResponse(response);
                break;
        }
    }

    private void onPostUserProfilePictureResponse(Object response) {
        ProfilePictureUploadApiResponse apiResponse = (ProfilePictureUploadApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }
        mUserModel.setProfilePicLink(apiResponse.getData().getUrls().get(0));
        onPostUserRegistration(mUserModel);
    }

    private void postUserRegistrationResponse(Object response) {
        UserApiResponse apiResponse = (UserApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }

        //TODO check response, fetch ticket details if required...
        if (!apiResponse.getStatusCode().equalsIgnoreCase(FrameworkConstants.API_STATUS_CODE.OK)) {
            showToast(apiResponse.getMessage());
            return;
        }

        onHandleRegistrationSuccess(apiResponse);
    }

//    private void postUserSignInResponse(Object response) {
//        UserApiResponse apiResponse = (UserApiResponse) response;
//        if (apiResponse.getError()) {
//            showToast(apiResponse.getMessage());
//            return;
//        }
//
//        if (!apiResponse.getStatusCode().equalsIgnoreCase(FrameworkConstants.API_STATUS_CODE.OK)) {
//            showToast(apiResponse.getMessage());
//            return;
//        }
//
//        //If user registerd and signIn successfully....
//        onHandleSignInSuccess(apiResponse);
//    }

    private void onHandleRegistrationSuccess(UserApiResponse apiResponse) {
        //start Email Verification activity
        Bundle bundle = new Bundle();
        bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL, mUserModel);
        startNextActivityAndFinishCurrent(bundle, EmailVerificationActivity.class);
        //onPostUserSignIn(mUserModel);
    }

//    private void onHandleSignInSuccess(UserApiResponse apiResponse) {
//        saveUserDetailsToLocal(apiResponse.getGalleryData());
//        finish();
//    }
//
//    private void saveUserDetailsToLocal(UserModel data) {
//        Preferences.saveData(Preferences.LOGIN_KEY, true);
//        Preferences.saveData(Preferences.USER_DATA_KEY, JsonUtil.toJson(data));
//    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
//        showToast(re != null ? re.getMessage() : getString(R.string.bkg_error));
        StyleableToast.makeText(RegistrationActivity.this, re != null ? re.getMessage() : getString(R.string.bkg_error), Toast.LENGTH_LONG, R.style.ErrorToast).show();
    }

    @Override
    public void onBackPressed() {
        if (mViewPagerCurrentItemIndex != MIN_VIEW_PAGER_COUNT) {
            onBackClicked();
            return;
        }
        super.onBackPressed();
    }
}
