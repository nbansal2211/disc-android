package com.dubaiisc.disc.activities.users.signIn;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.NotificationActivity;
import com.dubaiisc.disc.activities.users.registration.TicketRegistrationActivity;
import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.dubaiisc.disc.model.user.SignInModel;
import com.dubaiisc.disc.model.user.UserApiResponse;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.ValidationHelper;

import org.json.JSONException;

import com.dubaiisc.disc.R;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.utility.Util;
import simplifii.framework.widgets.CustomFontEditText;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class SignInActivity extends AppBaseActivity {

    private ViewFlipper mVfToggleLayout;


    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        initToolBar("");
        initViews();
        setOnClickListener(R.id.lyt_register, R.id.btn_signIn, R.id.tv_forgetPassword, R.id.btn_send, R.id.cl_notification);
    }

    private void initViews() {
        mVfToggleLayout = findViewById(R.id.vf_layout);
    }

    private void toggleLayout(boolean isNext) {
        if (isNext) mVfToggleLayout.showNext();
        else mVfToggleLayout.showPrevious();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.lyt_register:
                onRegisterButtonClicked();
                break;
            case R.id.btn_signIn:
                onSignInButtonClicked();
                break;
            case R.id.tv_forgetPassword:
                onForgetPasswordClicked();
                break;
            case R.id.btn_send:
                onSendButtonClicked();
                break;
            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;

        }
    }

    private void onRegisterButtonClicked() {
        startNextActivityAndFinishCurrent(TicketRegistrationActivity.class);
    }

    private void onSignInButtonClicked() {
        if (isSignInFieldValid()) {
            SignInModel signInModel = new SignInModel();
            signInModel.setEmail(getEditText(R.id.edtTxt_email));
            signInModel.setPassword(getEditText(R.id.edtTxt_password));
            onPostUserSignIn(signInModel);
        } else {
            showInvalidSignInFieldError();
        }
    }

    private void onForgetPasswordClicked() {
        toggleLayout(true);
    }

    private void onSendButtonClicked() {
        if (isForgetPasswordFieldValid()) {
            onPostUserForgetPassword(getEditText(R.id.edtTxt_forgetPasswordEmail));
        } else {
            showInvalidForgetPasswordFieldError();
        }
    }

    private boolean isSignInFieldValid() {
        return ValidationHelper.isEmailValid(getEditText(R.id.edtTxt_email)) && ValidationHelper.isPasswordValid(getEditText(R.id.edtTxt_password));
    }

    private boolean isForgetPasswordFieldValid() {
        return ValidationHelper.isEmailValid(getEditText(R.id.edtTxt_forgetPasswordEmail));
    }

    private void showInvalidSignInFieldError() {
        if (ValidationHelper.isEmailNotValid(getEditText(R.id.edtTxt_email)))
            ((CustomFontEditText) findViewById(R.id.edtTxt_email)).setError(getString(R.string.email_is_invalid));//This field cannot be blank
        if (ValidationHelper.isPasswordNotValid(getEditText(R.id.edtTxt_password)))
            ((CustomFontEditText) findViewById(R.id.edtTxt_password)).setError(getString(R.string.password_is_invalid));
    }

    private void showInvalidForgetPasswordFieldError() {
        if (ValidationHelper.isEmailNotValid(getEditText(R.id.edtTxt_forgetPasswordEmail)))
            ((CustomFontEditText) findViewById(R.id.edtTxt_forgetPasswordEmail)).setError(getString(R.string.email_is_invalid));//This field cannot be blank
    }

    private void onPostUserSignIn(SignInModel signInModel) {
        executeTask(FrameworkConstants.TASK_CODES.SIGN_IN, ApiRequestGenerator.onPostSignIn(signInModel));
    }

    private void onPostUserForgetPassword(String email) {
        try {
            executeTask(FrameworkConstants.TASK_CODES.FORGET_PASSWORD, ApiRequestGenerator.onPostForgetPassword(email));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.SIGN_IN:
                postUserSignInResponse(response);
                break;
            case FrameworkConstants.TASK_CODES.FORGET_PASSWORD:
                postUserForgetPassword(response);
                break;
        }

    }

    private void postUserForgetPassword(Object response) {
        BaseApiResponse apiResponse = (BaseApiResponse) response;
        if (apiResponse.getError()) {
            StyleableToast.makeText(SignInActivity.this, apiResponse.getMessage(), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }
        showAlertDialog();
        //StyleableToast.makeText(SignInActivity.this, getString(R.string.forget_password_success_message), Toast.LENGTH_LONG, R.style.SuccessToast).show();
    }

    private void showAlertDialog() {
        Util.createAlertDialog(SignInActivity.this, getString(R.string.forget_password_success_message), getString(R.string.forget_password), false, "OK", new Util.DialogListener() {
            @Override
            public void onOKPressed(DialogInterface dialog, int which) {
                finish();
            }

            @Override
            public void onCancelPressed(DialogInterface dialog, int which) {

            }
        }).show();
    }

    private void postUserSignInResponse(Object response) {
        UserApiResponse apiResponse = (UserApiResponse) response;
        if (apiResponse.getError() || !apiResponse.getStatusCode().equalsIgnoreCase(FrameworkConstants.API_STATUS_CODE.OK)) {
            StyleableToast.makeText(SignInActivity.this, apiResponse.getMessage(), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }

        onHandleSignInSuccess(apiResponse);
    }

    private void onHandleSignInSuccess(UserApiResponse apiResponse) {
        saveUserDetailsToLocal(apiResponse.getData());
        finish();
    }

    private void saveUserDetailsToLocal(UserModel data) {
        Preferences.saveData(Preferences.LOGIN_KEY, true);
        Preferences.saveData(Preferences.USER_DATA_KEY, JsonUtil.toJson(data));
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
//        showToast(re != null ? re.getMessage() : getString(R.string.bkg_error));
        StyleableToast.makeText(SignInActivity.this, re != null ? re.getMessage() : getString(R.string.bkg_error), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
    }
}


