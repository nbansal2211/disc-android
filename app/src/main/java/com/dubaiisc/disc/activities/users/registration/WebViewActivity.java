package com.dubaiisc.disc.activities.users.registration;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.NotificationActivity;

import simplifii.framework.utility.FrameworkConstants;

public class WebViewActivity extends AppBaseActivity {

    private WebView mWebView;

    private String mWebContentUrl = "";

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_web_view);
        setOnClickListener(R.id.cl_notification);
        initToolBar("");
        initWebView();
    }

    @Override
    protected void loadBundle(Bundle bundle) {
        super.loadBundle(bundle);
        mWebContentUrl = bundle.getString(FrameworkConstants.BUNDLE_KEYS.EXTRA_VALUE, "");
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        mWebView = findViewById(R.id.webVw_main);
        mWebView.loadUrl(mWebContentUrl);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressDialog();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideProgressBar();
            }
        });
    }

    @Override
    public void showProgressDialog() {
        findViewById(R.id.pb).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        findViewById(R.id.pb).setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;
        }

    }
}
