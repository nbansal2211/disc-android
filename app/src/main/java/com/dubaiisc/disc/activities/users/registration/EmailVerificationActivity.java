package com.dubaiisc.disc.activities.users.registration;

import android.os.Bundle;
import android.view.View;
import android.widget.ViewFlipper;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.MainActivity;
import com.dubaiisc.disc.activities.users.signIn.SignInActivity;

public class EmailVerificationActivity extends AppBaseActivity {

    private ViewFlipper mVpEmailVerification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_verification);

        initViews();
        setOnClickListener(R.id.btn_signIn, R.id.btn_done);
    }

    private void initViews() {
        mVpEmailVerification = findViewById(R.id.vf_emailVerification);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.btn_done:
                startMainActivity();
//                mVpEmailVerification.showNext();
                break;
            case R.id.btn_signIn:
                startSignInActivity();
                break;
        }
    }

    private void startMainActivity() {
        startNextActivityAndFinishCurrent(MainActivity.class);
    }

    private void startSignInActivity() {
        startNextActivityAndFinishCurrent(SignInActivity.class);
    }
}
