package com.dubaiisc.disc.activities;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.model.media.FeedModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;

import java.io.ByteArrayOutputStream;
import simplifii.framework.asyncmanager.ThumbnailExtractAsyncTask;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;

public class MediaFeedDetailActivity extends AppBaseActivity {

    private FeedModel model;
    private View tint;
    private ImageView background;
    private CustomFontTextView title;
    private CustomFontTextView newsDate;
    private CustomFontTextView newsContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_feed_detail);
        initViews();
        initToolBar("");
        getModel();
        setModelData();
        setOnClickListener(R.id.iv_share, R.id.ib_registerNow);
    }

    private void setModelData() {
        final String title = model.getTitle();
        final String date = model.getDate();
        final String longText = model.getLongText();
        ImageView eventMediaPlay = (ImageView) findViewById(R.id.iv_event_media_play);
        if (title != null)
            this.title.setText(title);
        else {
            final String shortText = model.getShortText();
            if (shortText != null) {
                this.title.setText(shortText);
            }
        }

        if (date != null)
            newsDate.setText(date);

        if (longText != null)
            newsContent.setText(longText);
        final String media = model.getImageUrl();
        if (!TextUtils.isEmpty(media)) {
            if (!TextUtils.isEmpty(media) && media.contains(".jpg") || media.contains(".jpeg") || media.contains(".png") || media.contains(".gif")) {
                hideVisibility(R.id.fl_border);
                setBackgroundImage(media);
            } else {
                showVisibility(R.id.fl_border);
                ThumbnailExtractAsyncTask thumbnailExtract = new ThumbnailExtractAsyncTask(media, background, true, true, this);
                thumbnailExtract.execute();
                eventMediaPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle = new Bundle();
                        bundle.putString(FrameworkConstants.BUNDLE_KEYS.MEDIA_PATH, media);
                        startNextActivity(bundle, MyVideoViewActivity.class);
                    }
                });
            }
        } else {
            hideVisibility(R.id.fl_border);
        }

    }

    private void getModel() {
        try {
            model = getIntent().getExtras().getParcelable(FrameworkConstants.BUNDLE_KEYS.FEED);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        tint = findViewById(R.id.v_tint);
        background = findViewById(R.id.iv_background);
        title = findViewById(R.id.tv_title);
        newsDate = findViewById(R.id.tv_newsDate);
        newsContent = findViewById(R.id.tv_newsContent);
//        play_video = findViewById(R.id.play_video);
    }

    private void setBackgroundImage(String imageUrl) {
        if (imageUrl != null) {
            Picasso.get().load(imageUrl).fit().centerCrop().into(background, new Callback() {
                @Override
                public void onSuccess() {
                    tint.setBackgroundResource(R.color.blue_1);
                }

                @Override
                public void onError(Exception e) {
                    e.printStackTrace();
                }
            });
        } else {
            background.setBackgroundResource(R.color.blue_1);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.iv_share:
                String content = getShareContent();
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                startActivity(Intent.createChooser(sharingIntent, "Share using..."));
        }
    }

    private String getShareContent() {
        if (model.getTitle() != null)
            return model.getTitle();
        else {
            if (model.getShortText() != null)
                return model.getShortText();
        }
        return getResources().getString(R.string.empty);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_white;
    }



    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(this.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


}
