package com.dubaiisc.disc.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.model.notification.NotificationApiResponse;
import com.dubaiisc.disc.model.notification.NotificationModel;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.itemDecorators.CustomDividerItemDecorator;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class NotificationActivity extends AppBaseActivity {

    private RecyclerView rv_notificationList;

    BaseRecycleAdapter<NotificationModel> mNotificationAdapter;
    private List<NotificationModel> mNotificationList = new ArrayList<>();

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        initToolBar("");
        initViews();
        setNotificationAdapter();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAllNotifications();

    }

    private void initViews() {
        rv_notificationList = findViewById(R.id.rv_notificationList);
        setOnClickListener(R.id.ib_registerNow);
    }

    private void setNotificationAdapter() {
        mNotificationAdapter = new BaseRecycleAdapter<>(this, mNotificationList);
        rv_notificationList.setAdapter(mNotificationAdapter);
        CustomDividerItemDecorator divider = new CustomDividerItemDecorator(this, R.drawable.divider);
        rv_notificationList.setLayoutManager(new LinearLayoutManager(this));
        rv_notificationList.addItemDecoration(divider);
    }

    private void updateNotificationAdapter(List<NotificationModel> notificationList) {
        if (CollectionUtils.isNotEmpty(notificationList)) {
            mNotificationList.clear();
            mNotificationList.addAll(notificationList);
            mNotificationAdapter.notifyDataSetChanged();
        }
    }

    private void getAllNotifications() {
        executeTask(FrameworkConstants.TASK_CODES.NOTIFICATIONS, ApiRequestGenerator.onGetNotification());
    }

    private void resetUnreadNotificationCount() {
        Preferences.saveData(AppConstants.STRING_CONST.UNREAD_NOTIFICATION_COUNT, 0);
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.NOTIFICATIONS:
                onGetAllNotificationResponse(response);
                break;

        }
    }

    private void onGetAllNotificationResponse(Object response) {
        NotificationApiResponse apiResponse = (NotificationApiResponse) response;
        if (apiResponse.getError()) {
            StyleableToast.makeText(NotificationActivity.this, apiResponse.getMessage(), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }

        //If all notification successfully received....
        resetUnreadNotificationCount();
        updateNotificationAdapter(apiResponse.getNotification().getNotificationList());
    }


}
