package com.dubaiisc.disc.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.StringUtility;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.GestureDetection;

import static com.dubaiisc.disc.utilities.StringUtility.extractYTId;

public class MyVideoViewActivity extends AppBaseActivity implements GestureDetection.SimpleGestureListener {

    private String mediaPath = "";
    private LinearLayout lin_view;
    private ProgressBar Pbar;
    private VideoView play_video;
    private WebView wv;
    private AudioManager audioManager;
    private GestureDetection detector;
    private int currentPosition;
    private int currentVolume;
    private MediaController mediaController;
    private static ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_video_view);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mediaPath = extras.getString(FrameworkConstants.BUNDLE_KEYS.MEDIA_PATH);
            play_video = findViewById(R.id.play_video);
            wv = findViewById(R.id.video_web_view);
            Pbar = findViewById(R.id.progress);
            lin_view = findViewById(R.id.lin_view);
            if (StringUtility.isYoutubeUrl(mediaPath)) youTubVideoPlay(extractYTId(mediaPath));
            else videoPlay();
        } else onBackPressed();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void youTubVideoPlay(String pathId) {
        if (pathId != null) {
            play_video.setVisibility(View.GONE);
            lin_view.setVisibility(View.VISIBLE);
            wv.getSettings().setSupportZoom(true);
            wv.getSettings().setBuiltInZoomControls(true);
            wv.getSettings().setDisplayZoomControls(true);
            wv.getSettings().setJavaScriptEnabled(true);
            wv.getSettings().setPluginState(WebSettings.PluginState.ON);
            wv.setBackgroundColor(0x919191);
            wv.loadDataWithBaseURL("", getHTML(pathId), "text/html", "UTF-8", "");
            wv.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    Pbar.setProgress(progress * 100);
                    if (progress < 100 && Pbar.getVisibility() == ProgressBar.GONE) {
                        Pbar.setVisibility(ProgressBar.VISIBLE);
                    }
                    Pbar.setProgress(progress);
                    if (progress == 100) {
                        Pbar.setVisibility(ProgressBar.GONE);
                    }
                }
            });
            wv.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    showToast("Error play video!");
                }
            });
            wv.setDownloadListener(new DownloadListener() {
                public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }
            });
        } else {
            showToast("Invalid url!");
            onBackPressed();
        }
    }


    private String getHTML(String id) {
        String html1 = "<iframe class=\"youtube-player\" style=\"border: 0; width: 100%; height: 95%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src=\"http://www.youtube.com/embed/"
                + id
                + "?fs=0\" frameborder=\"0\">\n"
                + "</iframe>\n";
        return html1;
    }


    private void videoPlay() {
        lin_view.setVisibility(View.GONE);
        play_video.setVisibility(View.VISIBLE);
        Uri video = Uri.parse(mediaPath);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        detector = new GestureDetection(this, this);
        play_video.setVideoURI(video);
        play_video.seekTo(1);
        mediaController = new MediaController(this);


        /*if(!TextUtils.isEmpty(mediaPath)){
            if (!mediaPath.contains("http")){
                retriever = new MediaMetadataRetriever();
                retriever.setDataSource("" + mediaPath);
                int ori = Integer.valueOf(retriever.extractMetadata
                        (MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION));
                retriever.release();

                if (ori == 90) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);

                }
            }
        }*/


//        mediaController.setAnchorView(play_video);
        mediaController.setMediaPlayer(play_video);
        play_video.setMediaController(mediaController);
        play_video.requestFocus();
        progressDialog = ProgressDialog.show(this, "", "Loading...", true);
        play_video.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                progressDialog.dismiss();
                showToast("Video Error");
                return false;
            }
        });

        play_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressDialog.dismiss();
                play_video.start();

                mp.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                    }
                });

//                mp.setOnErrorListener(new MediaPlayer.OnErrorListener() {
//                    @Override
//                    public boolean onError(MediaPlayer mp, int what, int extra) {
//                        progressDialog.dismiss();
//                        showToast("Video Error");
//                        mp.stop();
//                        finish();
//                        return false;
//                    }
//                });

                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        finish();
                    }
                });


            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent me) {
        // Call onTouchEvent of SimpleGestureFilter class
        if (this.detector != null) this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }

    @Override
    public void onSwipe(int direction) {
        // TODO Auto-generated method stub

        switch (direction) {

            case GestureDetection.SWIPE_LEFT:

                currentPosition = play_video.getCurrentPosition();
                currentPosition = play_video.getCurrentPosition() + 5000;
                play_video.seekTo(currentPosition);
                break;

            case GestureDetection.SWIPE_RIGHT:

                currentPosition = play_video.getCurrentPosition();
                currentPosition = play_video.getCurrentPosition() - 5000;
                play_video.seekTo(currentPosition);
                break;

            case GestureDetection.SWIPE_DOWN:

                currentVolume = audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        currentVolume - 1, 0);
                break;
            case GestureDetection.SWIPE_UP:

                currentVolume = audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        currentVolume + 1, 0);
                break;
        }
    }
}
