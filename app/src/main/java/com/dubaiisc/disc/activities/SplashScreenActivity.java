package com.dubaiisc.disc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;

import com.dubaiisc.disc.R;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import simplifii.framework.utility.Preferences;

public class SplashScreenActivity extends AppBaseActivity {

    private GifImageView mGifIvSplashScreen;
    private ImageView mIvLogo;
    private GifDrawable mGifDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        initViews();
        initTimerForStartNextActivity();
        setStatusBarColor(getResources().getColor(R.color.white));
    }

    private void initViews() {
        mGifIvSplashScreen = findViewById(R.id.giv_gifSplashScreen);
        mIvLogo = findViewById(R.id.iv_logo);
        try {
            mGifDrawable = new GifDrawable(getResources(), R.drawable.gif_splash_screen);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initTimerForStartNextActivity() {
        long splashScreenGifImageDurationInMills = mGifDrawable.getDuration();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Preferences.isFirstTimeUser()) {
                    hideGifView();
                    ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashScreenActivity.this, mIvLogo, "anim");
                    ActivityCompat.startActivity(SplashScreenActivity.this, new Intent(SplashScreenActivity.this, LanguageSetupActivity.class), optionsCompat.toBundle());
                    finish();
                } else {
                    startNextActivityAndFinishCurrent(MainActivity.class);
                }
            }
        }, splashScreenGifImageDurationInMills);
    }

    private void hideGifView() {
        mGifIvSplashScreen.setVisibility(View.GONE);
        mIvLogo.setVisibility(View.VISIBLE);
    }
}
