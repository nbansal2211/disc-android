package com.dubaiisc.disc.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.dubaiisc.disc.fragments.TermsAndConditionsFragment;
import com.dubaiisc.disc.fragments.agenda.AgendaDetailsFragment;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.R;
import simplifii.framework.utility.FrameworkConstants;

public class ActivityContainer extends AppBaseActivity {

    public static void startActivity(Context context, int fragmentType, Bundle extraBundle) {
        Intent i = new Intent(context, ActivityContainer.class);
        if (extraBundle != null) {
            i.putExtras(extraBundle);
        }
        i.putExtra(FrameworkConstants.BUNDLE_KEYS.FRAGMENT_TYPE, fragmentType);
        context.startActivity(i);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        Fragment f = getFragment(getIntent().getExtras().getInt(FrameworkConstants.BUNDLE_KEYS.FRAGMENT_TYPE));
        f.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_fragmentContainer, f).commit();
        initToolBar("");
    }

    private Fragment getFragment(int fragmentType) {
        Fragment fragment = null;
        switch (fragmentType) {
            case AppConstants.FRAGMENT_TYPE.TERMS_AND_CONDITIONS:
                fragment = TermsAndConditionsFragment.newInstance();
                break;
            case AppConstants.FRAGMENT_TYPE.AGENDA_DETAILS:
                fragment = AgendaDetailsFragment.newInstance();
                break;

        }
        if (fragment != null) {
            fragment.setArguments(getIntent().getExtras());
        }
        return fragment;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
