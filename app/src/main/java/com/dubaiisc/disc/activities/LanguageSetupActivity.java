package com.dubaiisc.disc.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;
import simplifii.framework.utility.Preferences;

public class LanguageSetupActivity extends AppBaseActivity {

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_setup);
        setOnClickListener(R.id.vw_english, R.id.vw_arabic, R.id.vw_french, R.id.vw_spenish);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.vw_english:
                Preferences.setFirstTimeUser(false);
                LocalHelperUtility.setLocale(LanguageSetupActivity.this, FrameworkConstants.LANGUAGE.EN);
                startNextActivityAndFinishCurrent(MainActivity.class);
                break;
            case R.id.vw_arabic:
                Preferences.setFirstTimeUser(false);
                LocalHelperUtility.setLocale(LanguageSetupActivity.this, FrameworkConstants.LANGUAGE.AR);
                startNextActivityAndFinishCurrent(MainActivity.class);
                break;
            case R.id.vw_french:
                Preferences.setFirstTimeUser(false);
                LocalHelperUtility.setLocale(LanguageSetupActivity.this, FrameworkConstants.LANGUAGE.FR);
                startNextActivityAndFinishCurrent(MainActivity.class);
                break;
            case R.id.vw_spenish:
                Preferences.setFirstTimeUser(false);
                LocalHelperUtility.setLocale(LanguageSetupActivity.this, FrameworkConstants.LANGUAGE.ES);
                startNextActivityAndFinishCurrent(MainActivity.class);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
