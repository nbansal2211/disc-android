package com.dubaiisc.disc.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.dubaiisc.disc.model.speakers.SpeakerModel;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;

public class SpeakerDetailsActivity extends AppBaseActivity {

    private SpeakerModel mSpeakerModel;

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker_details);
        getSpeakerModelFromBundle(getIntent());

        initViews();
        setOnClickListener(R.id.ib_back, R.id.ib_registerNow);
    }

    private void getSpeakerModelFromBundle(Intent intent) {
        mSpeakerModel = intent.getParcelableExtra(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL);
    }

    private void initViews() {
        initDefaultDataToViews();
    }

    private void initDefaultDataToViews() {
        Picasso.get().load(mSpeakerModel.getImageURL()).fit().into((ImageView) findViewById(R.id.iv_speakerThumb));
        setText(mSpeakerModel.getName(), R.id.tv_speakerName);
        setText(mSpeakerModel.getDesignation(), R.id.tv_speakerDesignation);
        setText(mSpeakerModel.getInfo(), R.id.tv_speakerInfo);
        if (CollectionUtils.isNotEmpty(mSpeakerModel.getAgendaStrings())) {
            final int size = mSpeakerModel.getAgendaStrings().size();
            StringBuilder agendas = new StringBuilder();
            for (int i = 0; i < size; i++) {
                agendas.append(mSpeakerModel.getAgendaStrings().get(i));
                if (i < size - 1)
                    agendas.append("\n");
            }
            setText(agendas.toString(), R.id.tv_speakerAgendaStrings);
        } else
            hideVisibility(R.id.tv_speakerAgendaStrings, R.id.v_divider_1, R.id.v_divider_2);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.ib_back:
                finish();
                break;
        }
    }
}
