package com.dubaiisc.disc.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.adapter.BaseRecycleAdapter;
import com.dubaiisc.disc.model.gallery.Item;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

import static com.dubaiisc.disc.utilities.AppConstants.VIEW_CODES.GALLERY_IMAGE_DIALOG;
import static simplifii.framework.utility.FrameworkConstants.BUNDLE_KEYS.GALLERY_ITEM;
import static simplifii.framework.utility.FrameworkConstants.BUNDLE_KEYS.POSITION;

public class ImageGalleryActivity extends AppBaseActivity {

    private List<Item> list;
    private TextView image_count;
    private ImageView rightArrow, leftArrow;
    private LinearLayoutManager linearLayoutManager;
    private int lastScrollPosition = 0;
    private Boolean isArabic = false;

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_image_gallery);
        setOnClickListener(R.id.cl_notification);
        isArabic = LocalHelperUtility.getLanguage().equalsIgnoreCase(FrameworkConstants.LANGUAGE.AR);
        initToolBar("");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            bundle.getInt(POSITION);
            list = bundle.getParcelableArrayList(GALLERY_ITEM);
            if (CollectionUtils.isNotEmpty(list)) initializer();
        }

    }

    private void initializer() {
        image_count = findViewById(R.id.image_count);
        rightArrow = findViewById(R.id.right_arrow);
        leftArrow = findViewById(R.id.left_arrow);
        RecyclerView recycler_pager = findViewById(R.id.recycler_pager);
        BaseRecycleAdapter<Item> recyclerPagerAdapter = new BaseRecycleAdapter<>(this, filterAndSetType("IMAGE"));
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycler_pager.setLayoutManager(linearLayoutManager);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recycler_pager);
        recycler_pager.setAdapter(recyclerPagerAdapter);
        recycler_pager.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastScrollPosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastScrollPosition != -1) {
                    if (isArabic) {
                        leftArrow.setRotation(-180);
                        rightArrow.setRotation(-180);
                        image_count.setText(list.size() + "/" + (lastScrollPosition + 1));
                    } else image_count.setText((lastScrollPosition + 1) + "/" + list.size());
                }

            }
        });
        recycler_pager.scrollToPosition(lastScrollPosition);
    }

    private List<Item> filterAndSetType(String type) {
        List<Item> newList = new ArrayList<>();
        for (Item i : list) {
            if (i.getType().toUpperCase().equalsIgnoreCase(type)) {
                i.setTypeChange(GALLERY_IMAGE_DIALOG);
                newList.add(i);
            }
        }
        return newList;
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;
        }

    }
}
