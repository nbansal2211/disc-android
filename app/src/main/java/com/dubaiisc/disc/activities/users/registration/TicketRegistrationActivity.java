package com.dubaiisc.disc.activities.users.registration;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.NotificationActivity;
import com.dubaiisc.disc.model.tickets.TicketApiResponse;
import com.dubaiisc.disc.model.tickets.TicketModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.ValidationHelper;
import simplifii.framework.exceptionhandler.RestException;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontEditText;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

public class TicketRegistrationActivity extends AppBaseActivity {

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_registration);

        initToolBar("");
        setOnClickListener(R.id.vw_purchase, R.id.btn_continue, R.id.cl_notification);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.vw_purchase:
                startWebViewActivity();
                break;

            case R.id.btn_continue:
                if (isTicketValid())
                    onTicketVerification(getEditText(R.id.edtTxt_ticketNumber));
                break;

            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;
        }
    }

    private boolean isTicketValid() {
        if (ValidationHelper.isEmpty(getEditText(R.id.edtTxt_ticketNumber))) {
            ((CustomFontEditText) findViewById(R.id.edtTxt_ticketNumber)).setError(getString(R.string.enter_ticket_number));//This field cannot be blank
            return false;
        }

        return true;
    }

    private void onTicketVerification(String ticketNumber) {
        executeTask(FrameworkConstants.TASK_CODES.TICKET_VERIFICATION, ApiRequestGenerator.onGetTicketVerification(ticketNumber));
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.TICKET_VERIFICATION:
                onTicketVerificationResponse(response);
                break;
        }
    }

    @Override
    public void onBackgroundError(RestException re, Exception e, int taskCode, Object... params) {
        super.onBackgroundError(re, e, taskCode, params);
//        showToast(re != null ? re.getMessage() : getString(R.string.background_error));
        StyleableToast.makeText(TicketRegistrationActivity.this, re != null ? re.getMessage() : getString(R.string.background_error), Toast.LENGTH_LONG, R.style.ErrorToast);
    }

    private void onTicketVerificationResponse(Object response) {
        TicketApiResponse apiResponse = (TicketApiResponse) response;
        if (apiResponse.getError()
                || apiResponse.getStatusCode().equalsIgnoreCase(FrameworkConstants.API_STATUS_CODE.TICKET_NOT_FOUND)) {

            StyleableToast.makeText(TicketRegistrationActivity.this, apiResponse.getMessage(), Toast.LENGTH_LONG, R.style.ErrorToast).show();
            return;
        }

        //If all things are verified..
        onHandleTicketStatus(apiResponse);
    }

    private void onHandleTicketStatus(TicketApiResponse apiResponse) {
        TicketModel ticketModel = apiResponse.getTicketModelList().get(0);
        switch (ticketModel.getStatus()) {
            case AppConstants.STRING_CONST.ADDED:
                startRegistrationActivity(ticketModel);
                break;
            case AppConstants.STRING_CONST.ALLOTTED:
//                showToast("Already allotted"); //TODO show proper message..
                StyleableToast.makeText(TicketRegistrationActivity.this, "Already Allotted", Toast.LENGTH_LONG, R.style.ErrorToast).show();
                break;
        }
    }

    private void startRegistrationActivity(TicketModel ticketModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(FrameworkConstants.BUNDLE_KEYS.EXTRA_MODEL, ticketModel);
        startNextActivityAndFinishCurrent(bundle, RegistrationActivity.class);
    }

    private void startWebViewActivity() {
        Bundle bundle = new Bundle();
        bundle.putString(FrameworkConstants.BUNDLE_KEYS.EXTRA_VALUE, AppConstants.EXTRAS.URL_TICKET_PURCHASE);
        startNextActivity(bundle, WebViewActivity.class);
    }


}
