package com.dubaiisc.disc.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.dubaiisc.disc.model.networking.NetworkingPersonModel;
import com.squareup.picasso.Picasso;

import com.dubaiisc.disc.R;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.widgets.CustomFontTextView;

public class NetworkingDetailsActivity extends AppBaseActivity {

    private TabLayout mTabFragContainer;
    private ViewFlipper mVfFragContainer;
    private NetworkingPersonModel model;
    private ImageView personThumb;
    private TextView personName;
    private TextView personDesignation;
    private TextView personCompany;
    private TextView personCityCountry;

    private TextView personAbout;
    private TextView personEmail;
    private TextView personWebsite;
    private TextView personLinkedIn;
    private TextView personFacebook;
    private TextView personTwitter;
    private TextView personLink1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_networking_details);
        getBundleModel();

        initToolBar("");

        initViews();
        initViewFlipperTabs();
        setTabLayoutChangeListener();
        initViewData();
        setOnClickListener(R.id.ib_registerNow);
    }

    private void initViews() {
        mVfFragContainer = findViewById(R.id.vf_networkDetailsFragContainer);
        mTabFragContainer = findViewById(R.id.tab_fragContainer);

        // TODO Add drawable right
        personName = findViewById(R.id.tv_personName);
        personDesignation = findViewById(R.id.tv_personDesignation);
        personCompany = findViewById(R.id.tv_personCompany);
        personCityCountry = findViewById(R.id.tv_personCityCountry);

        personThumb = findViewById(R.id.iv_personThumb);
        personAbout = findViewById(R.id.tv_personAbout);
        personEmail = findViewById(R.id.tv_personEmail);
        personWebsite = findViewById(R.id.tv_personWebsite);
        personLinkedIn = findViewById(R.id.tv_personLinkedIn);
        personFacebook = findViewById(R.id.tv_personFacebook);
        personTwitter = findViewById(R.id.tv_personTwitter);
        personLink1 = findViewById(R.id.tv_personLink1);

        setOnClickListener(R.id.ib_registerNow);
    }

    private void initViewFlipperTabs() {
        CustomFontTextView tabOne = (CustomFontTextView) LayoutInflater.from(NetworkingDetailsActivity.this).inflate(R.layout.tabs_heading, null);
        tabOne.setText(R.string.about);
        tabOne.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_selected_profile_about, 0, 0);
        mTabFragContainer.getTabAt(0).setCustomView(tabOne);

        CustomFontTextView tabTwo = (CustomFontTextView) LayoutInflater.from(NetworkingDetailsActivity.this).inflate(R.layout.tabs_heading, null);
        tabTwo.setText(R.string.connect);
        tabTwo.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_unselected_profile_connect, 0, 0);
        mTabFragContainer.getTabAt(1).setCustomView(tabTwo);

    }

    private void setTabLayoutChangeListener() {
        mTabFragContainer.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        onAboutTabSelected();
                        mVfFragContainer.showPrevious();
                        break;
                    case 1:
                        onConnectTabSelected();
                        mVfFragContainer.showNext();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void getBundleModel() {
        model = getIntent().getParcelableExtra(FrameworkConstants.BUNDLE_KEYS.NETWORK_MODEL);
    }

    private void initViewData() {
        StringBuilder fullName = new StringBuilder(model.getFirstName());
        fullName.append(" ").append(TextUtils.isEmpty(model.getMiddleNamee()) ? "" : model.getMiddleNamee());
        fullName.append(" ").append(TextUtils.isEmpty(model.getLastName()) ? "" : model.getLastName());

        StringBuilder cityCountry = new StringBuilder();
        cityCountry.append(model.getCity()).append(", ").append(model.getCountry());

        if (TextUtils.isEmpty(model.getProfilePicLink()))
            Picasso.get().load(R.drawable.ic_default_profile_picture).placeholder(R.drawable.ic_default_profile_picture).into(personThumb);
        else
            Picasso.get().load(model.getProfilePicLink()).placeholder(R.drawable.ic_default_profile_picture).fit().into(personThumb);

        personName.setText(fullName);
        personDesignation.setText(model.getDesignation());
        personCompany.setText(model.getOrganization());
        personCityCountry.setText(cityCountry);

        personAbout.setText(model.getAbout());
        personEmail.setText(model.getEmail());
        personWebsite.setText(model.getWebsiteLink());
        personLinkedIn.setText(model.getLinkedInLink());
        personFacebook.setText(model.getFacebookLink());
        personTwitter.setText(model.getTwitterLink());
        personLink1.setText(model.getAdditionalLink());
    }

    private void onAboutTabSelected() {
        CustomFontTextView tabOne = (CustomFontTextView) mTabFragContainer.getTabAt(0).getCustomView();
        tabOne.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_selected_profile_about, 0, 0);
        //
        CustomFontTextView tabTwo = (CustomFontTextView) mTabFragContainer.getTabAt(1).getCustomView();
        tabTwo.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_unselected_profile_connect, 0, 0);
    }

    private void onConnectTabSelected() {
        CustomFontTextView tabOne = (CustomFontTextView) mTabFragContainer.getTabAt(0).getCustomView();
        tabOne.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_unselected_profile_about, 0, 0);
        //
        CustomFontTextView tabTwo = (CustomFontTextView) mTabFragContainer.getTabAt(1).getCustomView();
        tabTwo.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_selected_profile_connect, 0, 0);
    }

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }
}
