package com.dubaiisc.disc.activities.users;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.dubaiisc.disc.activities.AppBaseActivity;
import com.dubaiisc.disc.activities.NotificationActivity;
import com.dubaiisc.disc.adapter.ViewPagerAdapter;
import com.dubaiisc.disc.fragments.myProfile.BaseMyProfileFragment;
import com.dubaiisc.disc.fragments.myProfile.ConnectFragment;
import com.dubaiisc.disc.fragments.myProfile.PasswordFragment;
import com.dubaiisc.disc.fragments.myProfile.UserAboutFragment;
import com.dubaiisc.disc.model.ProfilePictureUploadApiResponse;
import com.dubaiisc.disc.model.ProfileUpdateApiResponse;
import com.dubaiisc.disc.model.country.CountryApiResponse;
import com.dubaiisc.disc.model.country.CountryModel;
import com.dubaiisc.disc.model.user.UserApiResponse;
import com.dubaiisc.disc.model.user.UserModel;
import com.dubaiisc.disc.networking.ApiRequestGenerator;
import com.dubaiisc.disc.utilities.AppConstants;
import com.dubaiisc.disc.utilities.ValidationHelper;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.google.gson.JsonParseException;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dubaiisc.disc.CustomImagePicker;
import com.dubaiisc.disc.R;
import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.JsonUtil;
import simplifii.framework.utility.Preferences;
import simplifii.framework.widgets.CustomFontTextView;
import simplifii.framework.widgets.styleabletoast.StyleableToast;

import static simplifii.framework.utility.Preferences.USER_DATA_KEY;

public class ProfileActivity extends AppBaseActivity {

    private TabLayout mTabFragContainer;
    private ViewPager mVpFragContainer;

    private List<ViewPagerAdapter.FragmentModelHolder> mListFragmentHolder = new ArrayList<>();

    private UserModel mUserModel;
    private AppCompatImageView profilePic;

    private File imageFile;
    private ArrayList<CountryModel> list;

    @Override
    protected int getHomeIcon() {
        return R.drawable.ic_back_brown;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initToolBar("");
        initViews();
        initViewPagerFragments();
        initViewPager();
        initViewPagerTabs();
        setTabLayoutChangeListener();
        setOnClickListener(R.id.cl_notification, R.id.tv_editMyProfile, R.id.iv_myProfile);
    }

    private void initViews() {
        mTabFragContainer = findViewById(R.id.tab_fragContainer);
        mVpFragContainer = findViewById(R.id.vp_myProfileFragContainer);
        mVpFragContainer.setRotationY((getResources().getBoolean(R.bool.isRightToLeft) ? 180 : 0)); //right to left
        mVpFragContainer.setOffscreenPageLimit(3); //Must be according to view pager size, or app will crash...
        profilePic = findViewById(R.id.iv_myProfile);
    }

    private void initViewPagerFragments() {
        mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(UserAboutFragment.newInstance(), getString(R.string.about)));
        mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(ConnectFragment.newInstance(), getString(R.string.connect)));
        mListFragmentHolder.add(new ViewPagerAdapter.FragmentModelHolder(PasswordFragment.newInstance(), getString(R.string.password)));
    }

    private void initViewPagerTabs() {
        CustomFontTextView tabOne = (CustomFontTextView) LayoutInflater.from(ProfileActivity.this).inflate(R.layout.tabs_heading, null);
        tabOne.setText(R.string.about_en);
        tabOne.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_selected_profile_about, 0, 0);
        mTabFragContainer.getTabAt(0).setCustomView(tabOne);

        CustomFontTextView tabTwo = (CustomFontTextView) LayoutInflater.from(ProfileActivity.this).inflate(R.layout.tabs_heading, null);
        tabTwo.setText(R.string.connect_en);
        tabTwo.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_unselected_profile_connect, 0, 0);
        mTabFragContainer.getTabAt(1).setCustomView(tabTwo);

        CustomFontTextView tabThree = (CustomFontTextView) LayoutInflater.from(ProfileActivity.this).inflate(R.layout.tabs_heading, null);
        tabThree.setText(R.string.password_en);
        tabThree.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabThree, 0, R.drawable.ic_tabs_unselected_profile_password, 0, 0);
        mTabFragContainer.getTabAt(2).setCustomView(tabThree);
    }

    private void initViewPager() {
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            pagerAdapter.addFragments(holder);
        }
        mVpFragContainer.setAdapter(pagerAdapter);
        mTabFragContainer.setupWithViewPager(mVpFragContainer);

    }

    private void setTabLayoutChangeListener() {
        mTabFragContainer.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        onAboutTabSelected();
                        break;
                    case 1:
                        onConnectTabSelected();
                        break;
                    case 2:
                        onPasswordTabSelected();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.cl_notification:
                startNextActivity(NotificationActivity.class);
                break;

            case R.id.btn_save_connect_changes:
                updateProfileChanges();
                break;

            case R.id.iv_myProfile:
            case R.id.tv_editMyProfile:
                onEditProfilePicClick();
                break;
        }

    }

    private void updateProfileChanges() {
        boolean shouldUpdate = true;
        for (int i = 0; i < mListFragmentHolder.size(); i++) {
            if (((BaseMyProfileFragment) mListFragmentHolder.get(i).getFragment()).isValid()) {
                ((BaseMyProfileFragment) mListFragmentHolder.get(i).getFragment()).apply(mUserModel);
            } else {
                ((BaseMyProfileFragment) mListFragmentHolder.get(i).getFragment()).showInvalidFieldError();
                shouldUpdate = false;
            }
        }

        if (shouldUpdate) {
            if (imageFile != null)
                executeTask(FrameworkConstants.TASK_CODES.PROFILE_PIC_UPLOAD, ApiRequestGenerator.onPostUserProfilePic(imageFile, imageFile.getName()));
            else
                executeTask(FrameworkConstants.TASK_CODES.PROFILE_UPDATE, ApiRequestGenerator.onPutUserProfile(mUserModel));
        }
        return;
    }

    private void onAboutTabSelected() {
        CustomFontTextView tabOne = (CustomFontTextView) mTabFragContainer.getTabAt(0).getCustomView();
        tabOne.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_selected_profile_about, 0, 0);
        //
        CustomFontTextView tabTwo = (CustomFontTextView) mTabFragContainer.getTabAt(1).getCustomView();
        tabTwo.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_unselected_profile_connect, 0, 0);
        //
        CustomFontTextView tabThree = (CustomFontTextView) mTabFragContainer.getTabAt(2).getCustomView();
        tabThree.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabThree, 0, R.drawable.ic_tabs_unselected_profile_password, 0, 0);
    }

    private void onConnectTabSelected() {
        CustomFontTextView tabOne = (CustomFontTextView) mTabFragContainer.getTabAt(0).getCustomView();
        tabOne.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_unselected_profile_about, 0, 0);
        //
        CustomFontTextView tabTwo = (CustomFontTextView) mTabFragContainer.getTabAt(1).getCustomView();
        tabTwo.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_selected_profile_connect, 0, 0);
        //
        CustomFontTextView tabThree = (CustomFontTextView) mTabFragContainer.getTabAt(2).getCustomView();
        tabThree.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabThree, 0, R.drawable.ic_tabs_unselected_profile_password, 0, 0);
    }

    private void onPasswordTabSelected() {
        CustomFontTextView tabOne = (CustomFontTextView) mTabFragContainer.getTabAt(0).getCustomView();
        tabOne.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabOne, 0, R.drawable.ic_tabs_unselected_profile_about, 0, 0);
        //
        CustomFontTextView tabTwo = (CustomFontTextView) mTabFragContainer.getTabAt(1).getCustomView();
        tabTwo.setTextColor(getResourceColor(R.color.grey_1));
        addVectorDrawableOnTextView(tabTwo, 0, R.drawable.ic_tabs_unselected_profile_connect, 0, 0);
        //
        CustomFontTextView tabThree = (CustomFontTextView) mTabFragContainer.getTabAt(2).getCustomView();
        tabThree.setTextColor(getResourceColor(R.color.brown_1));
        addVectorDrawableOnTextView(tabThree, 0, R.drawable.ic_tabs_selected_profile_password, 0, 0);
    }

    private void onEditProfilePicClick() {
        CustomImagePicker.openImagePicker(this, AppConstants.REQUEST_TYPE.SELECT_PROFILE_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppConstants.REQUEST_TYPE.SELECT_PROFILE_IMAGE:
                if (resultCode == RESULT_OK) {

                    List<Image> images = ImagePicker.getImages(data);
                    if (CollectionUtils.isNotEmpty(images)) {
                        imageFile = new File(images.get(0).getPath());
                        if (ValidationHelper.isFileSizeValid(imageFile)) {
                            Picasso.get().load(imageFile).fit().into(profilePic);
                        } else {
                            Toast.makeText(this, R.string.image_size_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getUserProfileById();
        getAllCountries();
    }

    private void getUserProfileById() {
        try {
            UserModel model = (UserModel) JsonUtil.parseJson(Preferences.getData(USER_DATA_KEY, ""), UserModel.class);
            executeTask(FrameworkConstants.TASK_CODES.PROFILE, ApiRequestGenerator.onGetUserProfile(model.getId()));
        } catch (NullPointerException | JsonParseException e) {
            e.printStackTrace();
        }

    }

    private void getAllCountries() {
        if (CollectionUtils.isEmpty(list))
            executeTask(FrameworkConstants.TASK_CODES.COUNTRY_LIST, ApiRequestGenerator.onGetCountries());
    }

    @Override
    public void onPostExecute(Object response, int taskCode, Object... params) {
        super.onPostExecute(response, taskCode, params);
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.PROFILE:
                onGetUserProfileResponse(response);
                break;
            case FrameworkConstants.TASK_CODES.PROFILE_UPDATE:
                onPutUserProfileResponse(response);
                break;
            case FrameworkConstants.TASK_CODES.PROFILE_PIC_UPLOAD:
                onPostUserProfilePictureResponse(response);
                break;
            case FrameworkConstants.TASK_CODES.COUNTRY_LIST:
                onGetCountryListResponse((CountryApiResponse) response);
        }
    }

    private void onGetCountryListResponse(CountryApiResponse response) {
        CountryApiResponse apiResponse = response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }
        list = apiResponse.getData();
        Collections.sort(list);
        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            if (holder.getFragment() instanceof BaseMyProfileFragment) {
                BaseMyProfileFragment baseMyProfileFragment = (BaseMyProfileFragment) (holder.getFragment());
                baseMyProfileFragment.setClickListenerCallback(this);
                baseMyProfileFragment.onCountryListFetched(list);
            }
        }
    }

    private void onPostUserProfilePictureResponse(Object response) {
        ProfilePictureUploadApiResponse apiResponse = (ProfilePictureUploadApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }
        mUserModel.setProfilePicLink(apiResponse.getData().getUrls().get(0));
        executeTask(FrameworkConstants.TASK_CODES.PROFILE_UPDATE, ApiRequestGenerator.onPutUserProfile(mUserModel));
    }

    private void onGetUserProfileResponse(Object response) {
        UserApiResponse apiResponse = (UserApiResponse) response;
        if (apiResponse.getError()) {
            showToast(apiResponse.getMessage());
            return;
        }
        mUserModel = apiResponse.getData();
        showUserDetails(mUserModel);
    }

    private void onPutUserProfileResponse(Object response) {
        //TODO need to correct this services...
        ProfileUpdateApiResponse apiResponse = (ProfileUpdateApiResponse) response;
        if (apiResponse.getError() || !apiResponse.getStatusCode().equalsIgnoreCase(FrameworkConstants.API_STATUS_CODE.OK)) {
            StyleableToast.makeText(ProfileActivity.this, apiResponse.getMessage(), Toast.LENGTH_SHORT, R.style.ErrorToast).show();
            return;
        }
        Preferences.saveData(Preferences.USER_DATA_KEY, JsonUtil.toJson(mUserModel));
        StyleableToast.makeText(ProfileActivity.this, getString(R.string.profile_details_updated), Toast.LENGTH_SHORT, R.style.SuccessToast).show();
    }

    private void showUserDetails(UserModel model) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            Picasso.get().load(mUserModel.getProfilePicLink()).placeholder(R.drawable.ic_default_profile_picture_normal).fit().into(profilePic);
        else
            Picasso.get().load(mUserModel.getProfilePicLink()).placeholder(R.drawable.ic_default_profile_picture).fit().into(profilePic);

        for (ViewPagerAdapter.FragmentModelHolder holder : mListFragmentHolder) {
            if (holder.getFragment() instanceof BaseMyProfileFragment) {
                BaseMyProfileFragment baseMyProfileFragment = (BaseMyProfileFragment) (holder.getFragment());
                baseMyProfileFragment.setClickListenerCallback(this);
                baseMyProfileFragment.setModelToViews(model);
            }
        }
    }


}
