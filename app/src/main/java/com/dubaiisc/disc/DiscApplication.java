package com.dubaiisc.disc;

import android.app.Application;

import simplifii.framework.utility.Preferences;

public class DiscApplication extends Application {

    private static DiscApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Preferences.initSharedPreferences(this);
    }
}
