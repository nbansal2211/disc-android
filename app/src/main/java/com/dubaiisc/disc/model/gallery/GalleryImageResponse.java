
package com.dubaiisc.disc.model.gallery;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GalleryImageResponse extends BaseApiResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<GalleryImageData> data = null;

    public List<GalleryImageData> getData() {
        return data;
    }

    public void setData(List<GalleryImageData> data) {
        this.data = data;
    }

}
