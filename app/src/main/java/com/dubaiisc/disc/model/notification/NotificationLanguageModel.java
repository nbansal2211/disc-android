package com.dubaiisc.disc.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class NotificationLanguageModel {

    @SerializedName("en")
    @Expose
    private List<NotificationModel> englishList;

    @SerializedName("ar")
    @Expose
    private List<NotificationModel> arabicList;

    @SerializedName("fr")
    @Expose
    private List<NotificationModel> frenchList;

    @SerializedName("es")
    @Expose
    private List<NotificationModel> spanishList;

    public List<NotificationModel> getNotificationList() {
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getArabicList();
            case FrameworkConstants.LANGUAGE.EN:
                return getEnglishList();
            case FrameworkConstants.LANGUAGE.FR:
                return getFrenchList();
            case FrameworkConstants.LANGUAGE.ES:
                return getSpanishList();
        }
        return getEnglishList();
    }

    private List<NotificationModel> getArabicList() {
        return arabicList;
    }

    private List<NotificationModel> getEnglishList() {
        return englishList;
    }

    public List<NotificationModel> getFrenchList() {
        return frenchList;
    }

    public List<NotificationModel> getSpanishList() {
        return spanishList;
    }
}
