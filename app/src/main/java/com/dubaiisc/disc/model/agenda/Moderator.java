
package com.dubaiisc.disc.model.agenda;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Moderator implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("agendaStrings")
    @Expose
    private List<String> agendaStrings;

    @SerializedName("designation")
    @Expose
    private String designation;

    @SerializedName("info")
    @Expose
    private String info;

    @SerializedName("imageURL")
    @Expose
    private String imageURL;

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public Moderator() {
    }

    private Moderator(Parcel in) {
        name = in.readString();
        agendaStrings = in.createStringArrayList();
        designation = in.readString();
        info = in.readString();
        imageURL = in.readString();
        id = in.readLong();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<Moderator> CREATOR = new Creator<Moderator>() {
        @Override
        public Moderator createFromParcel(Parcel in) {
            return new Moderator(in);
        }

        @Override
        public Moderator[] newArray(int size) {
            return new Moderator[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAgendaStrings() {
        return agendaStrings;
    }

    public void setAgendaStrings(List<String> agendaStrings) {
        this.agendaStrings = agendaStrings;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeStringList(agendaStrings);
        dest.writeString(designation);
        dest.writeString(info);
        dest.writeString(imageURL);
        dest.writeLong(id);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }
}
