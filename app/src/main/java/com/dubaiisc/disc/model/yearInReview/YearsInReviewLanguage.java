package com.dubaiisc.disc.model.yearInReview;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class YearsInReviewLanguage {

    @SerializedName("en")
    @Expose
    private List<YearsInReviewModel> englishList;

    @SerializedName("ar")
    @Expose
    private List<YearsInReviewModel> arabicList;

    @SerializedName("fr")
    @Expose
    private List<YearsInReviewModel> frenchList;

    @SerializedName("es")
    @Expose
    private List<YearsInReviewModel> spanishList;

    private List<YearsInReviewModel> getEnglishList() {
        return englishList;
    }

    private List<YearsInReviewModel> getArabicList() {
        return arabicList;
    }

    public List<YearsInReviewModel> getFrenchList() {
        return frenchList;
    }

    public List<YearsInReviewModel> getSpanishList() {
        return spanishList;
    }

    public List<YearsInReviewModel> getYearsInReviewList() {
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getArabicList();
            case FrameworkConstants.LANGUAGE.EN:
                return getEnglishList();
            case FrameworkConstants.LANGUAGE.FR:
                return getFrenchList();
            case FrameworkConstants.LANGUAGE.ES:
                return getSpanishList();
        }
        return getEnglishList();
    }

    public List<YearsInReviewModel> getYearsInReviewListEn() {
        return getEnglishList();
    }

}
