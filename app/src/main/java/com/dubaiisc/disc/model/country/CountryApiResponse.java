package com.dubaiisc.disc.model.country;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CountryApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<CountryModel> data;

    public ArrayList<CountryModel> getData() {
        return data;
    }

    public void setData(ArrayList<CountryModel> data) {
        this.data = data;
    }
}
