package com.dubaiisc.disc.model.agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class AgendaModel {

    @SerializedName("en")
    @Expose
    private List<Event> eventEnglish = null;
    @SerializedName("ar")
    @Expose
    private List<Event> eventArabic = null;
    @SerializedName("es")
    @Expose
    private List<Event> eventSpanish = null;
    @SerializedName("fr")
    @Expose
    private List<Event> eventFrench = null;

    private List<Event> getEventEnglish() {
        return eventEnglish;
    }

    public void setEventEnglish(List<Event> data) {
        this.eventEnglish = data;
    }

    private List<Event> getEventArabic() {
        return eventArabic;
    }

    public List<Event> getEventSpanish() {
        return eventSpanish;
    }

    public void setEventSpanish(List<Event> eventSpanish) {
        this.eventSpanish = eventSpanish;
    }

    public List<Event> getEventFrench() {
        return eventFrench;
    }

    public void setEventFrench(List<Event> eventFrench) {
        this.eventFrench = eventFrench;
    }

    public List<Event> getEvents() {
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getEventArabic();
            case FrameworkConstants.LANGUAGE.EN:
                return getEventEnglish();
            case FrameworkConstants.LANGUAGE.FR:
                return getEventFrench();
            case FrameworkConstants.LANGUAGE.ES:
                return getEventSpanish();
        }
        return getEventEnglish();
    }

}
