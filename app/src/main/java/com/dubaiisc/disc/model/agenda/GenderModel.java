package com.dubaiisc.disc.model.agenda;

import com.dubaiisc.disc.model.BaseAdapterModel;

public class GenderModel extends BaseAdapterModel {

    @Override
    public int getViewType() {
        return 0;
    }

    private String genderText;
    private String genderTextApi;

    public GenderModel(String genderText, String genderTextApi) {
        this.genderText = genderText;
        this.genderTextApi = genderTextApi;
    }

    public String getGenderText() {
        return genderText;
    }

    public void setGenderText(String genderText) {
        this.genderText = genderText;
    }

    public String getGenderTextApi() {
        return genderTextApi;
    }

    public void setGenderTextApi(String genderTextApi) {
        this.genderTextApi = genderTextApi.toUpperCase();
    }
}
