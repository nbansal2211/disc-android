package com.dubaiisc.disc.model;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveUserDeviceResponse extends BaseApiResponse {
    @SerializedName("data")
    @Expose
    private SaveUserDeviceInfoData data;

    public SaveUserDeviceInfoData getData() {
        return data;
    }

    public void setData(SaveUserDeviceInfoData data) {
        this.data = data;
    }
}
