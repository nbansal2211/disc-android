package com.dubaiisc.disc.model.termsAndConditions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class TermsAndConditionLanguageModel {

    @SerializedName("en")
    @Expose
    private List<TermsAndConditionModel> englishTcList;

    @SerializedName("ar")
    @Expose
    private List<TermsAndConditionModel> arabicTcList;

    @SerializedName("fr")
    @Expose
    private List<TermsAndConditionModel> frenchTcList;

    @SerializedName("es")
    @Expose
    private List<TermsAndConditionModel> spanishTcList;


    private List<TermsAndConditionModel> getEnglishTcList() {
        return englishTcList;
    }

    private List<TermsAndConditionModel> getArabicTcList() {
        return arabicTcList;
    }

    public List<TermsAndConditionModel> getFrenchTcList() {
        return frenchTcList;
    }

    public List<TermsAndConditionModel> getSpanishTcList() {
        return spanishTcList;
    }

    public List<TermsAndConditionModel> getTcList() {
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getArabicTcList();
            case FrameworkConstants.LANGUAGE.EN:
                return getEnglishTcList();
            case FrameworkConstants.LANGUAGE.FR:
                return getFrenchTcList();
            case FrameworkConstants.LANGUAGE.ES:
                return getSpanishTcList();
        }
        return getEnglishTcList();
    }

}
