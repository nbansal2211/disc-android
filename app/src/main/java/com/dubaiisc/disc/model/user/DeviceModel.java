package com.dubaiisc.disc.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceModel {

    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    @SerializedName("platform")
    @Expose
    private String platform;

    @SerializedName("pushToken")
    @Expose
    private String pushToken;

    @SerializedName("userId")
    @Expose
    private String userId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(String pushToken) {
        this.pushToken = pushToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
