package com.dubaiisc.disc.model.faqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class FaqsModel {

    @SerializedName("en")
    @Expose
    private LinkedHashMap<String, List<QuesAnsModel>> en;
    @SerializedName("ar")
    @Expose
    private LinkedHashMap<String, List<QuesAnsModel>> ar;

    @SerializedName("fr")
    @Expose
    private LinkedHashMap<String, List<QuesAnsModel>> fr;
    @SerializedName("es")
    @Expose
    private LinkedHashMap<String, List<QuesAnsModel>> es;

    private LinkedHashMap<String, List<QuesAnsModel>> getEn() {
        return en;
    }

    public void setEn(LinkedHashMap<String, List<QuesAnsModel>> en) {
        this.en = en;
    }

    private LinkedHashMap<String, List<QuesAnsModel>> getAr() {
        return ar;
    }

    public void setAr(LinkedHashMap<String, List<QuesAnsModel>> ar) {
        this.ar = ar;
    }

    public LinkedHashMap<String, List<QuesAnsModel>> getMap() {
        // TODO Return Map based on locale
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getAr();
            case FrameworkConstants.LANGUAGE.EN:
                return getEn();
            case FrameworkConstants.LANGUAGE.FR:
                return getFr();
            case FrameworkConstants.LANGUAGE.ES:
                return getEs();
        }
        return getEn();
    }

    public LinkedHashMap<String, List<QuesAnsModel>> getFr() {
        return fr;
    }

    public LinkedHashMap<String, List<QuesAnsModel>> getEs() {
        return es;
    }
}
