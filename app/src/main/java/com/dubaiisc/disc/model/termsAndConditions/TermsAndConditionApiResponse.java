package com.dubaiisc.disc.model.termsAndConditions;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TermsAndConditionApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private TermsAndConditionLanguageModel termsAndCondition;


    public TermsAndConditionLanguageModel getTermsAndCondition() {
        return termsAndCondition;
    }
}
