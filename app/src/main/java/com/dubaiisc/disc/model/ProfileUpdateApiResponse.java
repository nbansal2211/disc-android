package com.dubaiisc.disc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.dubaiisc.disc.model.user.UserModel;

public class ProfileUpdateApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private UserModel data;


    public UserModel getData() {
        return data;
    }
}
