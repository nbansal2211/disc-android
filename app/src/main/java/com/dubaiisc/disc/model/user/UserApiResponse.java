package com.dubaiisc.disc.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class UserApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private UserModel data;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}
