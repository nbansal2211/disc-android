package com.dubaiisc.disc.model.drawer;

import java.util.List;

public class DrawerMainData {

    private String title;
    private int eventType;
    private List<DrawerChildData> childData;

    public DrawerMainData(String title, int eventType, List<DrawerChildData> childData) {
        this.title = title;
        this.eventType = eventType;
        this.childData = childData;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public List<DrawerChildData> getChildData() {
        return childData;
    }

    public void setChildData(List<DrawerChildData> childData) {
        this.childData = childData;
    }
}
