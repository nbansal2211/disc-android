package com.dubaiisc.disc.model.sponsors;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class Translation {

@SerializedName("langCode")
@Expose
private String langCode;
@SerializedName("translatedType")
@Expose
private String translatedType;

public String getLangCode() {
return langCode;
}

public void setLangCode(String langCode) {
this.langCode = langCode;
}

public String getTranslatedType() {
return translatedType;
}

public void setTranslatedType(String translatedType) {
this.translatedType = translatedType;
}

}