package com.dubaiisc.disc.model.media;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class MediaApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private MediaModel data;

    public MediaModel getData() {
        return data;
    }

    public void setData(MediaModel data) {
        this.data = data;
    }
}
