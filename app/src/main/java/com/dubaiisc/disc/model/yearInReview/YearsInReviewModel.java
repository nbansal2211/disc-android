package com.dubaiisc.disc.model.yearInReview;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearsInReviewModel extends BaseAdapterModel {

    @SerializedName("langCode")
    @Expose
    private String langCode;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dateDisplayString")
    @Expose
    private String dateDisplayString;
    @SerializedName("attendeesDisplayString")
    @Expose
    private String attendeesDisplayString;
    @SerializedName("speakersDisplayString")
    @Expose
    private String speakersDisplayString;
    @SerializedName("topicsDisplayString")
    @Expose
    private String topicsDisplayString;
    @SerializedName("recommendationsDisplayString")
    @Expose
    private String recommendationsDisplayString;
    @SerializedName("attendeesCount")
    @Expose
    private long attendeesCount;
    @SerializedName("speakersCount")
    @Expose
    private long speakersCount;
    @SerializedName("topicsCount")
    @Expose
    private long topicsCount;
    @SerializedName("recomendationsCount")
    @Expose
    private long recomendationsCount;
    @SerializedName("brochureLink")
    @Expose
    private String brochureLink;
    @SerializedName("edition")
    @Expose
    private String edition;
    @SerializedName("editionTranslation")
    @Expose
    private String editionTranslation;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    private boolean isVisible;
    private Integer typeChange = null;

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateDisplayString() {
        return dateDisplayString;
    }

    public void setDateDisplayString(String dateDisplayString) {
        this.dateDisplayString = dateDisplayString;
    }

    public String getAttendeesDisplayString() {
        return attendeesDisplayString;
    }

    public void setAttendeesDisplayString(String attendeesDisplayString) {
        this.attendeesDisplayString = attendeesDisplayString;
    }

    public String getSpeakersDisplayString() {
        return speakersDisplayString;
    }

    public void setSpeakersDisplayString(String speakersDisplayString) {
        this.speakersDisplayString = speakersDisplayString;
    }

    public String getTopicsDisplayString() {
        return topicsDisplayString;
    }

    public void setTopicsDisplayString(String topicsDisplayString) {
        this.topicsDisplayString = topicsDisplayString;
    }

    public String getRecommendationsDisplayString() {
        return recommendationsDisplayString;
    }

    public void setRecommendationsDisplayString(String recommendationsDisplayString) {
        this.recommendationsDisplayString = recommendationsDisplayString;
    }

    public long getAttendeesCount() {
        return attendeesCount;
    }

    public void setAttendeesCount(long attendeesCount) {
        this.attendeesCount = attendeesCount;
    }

    public long getSpeakersCount() {
        return speakersCount;
    }

    public void setSpeakersCount(long speakersCount) {
        this.speakersCount = speakersCount;
    }

    public long getTopicsCount() {
        return topicsCount;
    }

    public void setTopicsCount(long topicsCount) {
        this.topicsCount = topicsCount;
    }

    public long getRecomendationsCount() {
        return recomendationsCount;
    }

    public void setRecomendationsCount(long recomendationsCount) {
        this.recomendationsCount = recomendationsCount;
    }

    public String getBrochureLink() {
        return brochureLink;
    }

    public void setBrochureLink(String brochureLink) {
        this.brochureLink = brochureLink;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getEditionTranslation() {
        return editionTranslation;
    }

    public void setEditionTranslation(String editionTranslation) {
        this.editionTranslation = editionTranslation;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    @Override
    public int getViewType() {
        return (typeChange != null) ? typeChange : AppConstants.VIEW_CODES.YEAR_IN_REVIEW;
    }


    public void setTypeChange(Integer typeChange) {
        this.typeChange = typeChange;
    }
}
