package com.dubaiisc.disc.model.agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class AgendaApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private AgendaModel data;

    public AgendaModel getData() {
        return data;
    }

    public void setData(AgendaModel data) {
        this.data = data;
    }
}
