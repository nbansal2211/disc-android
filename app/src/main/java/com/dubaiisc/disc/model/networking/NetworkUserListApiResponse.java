package com.dubaiisc.disc.model.networking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class NetworkUserListApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<NetworkingPersonModel> data;

    public ArrayList<NetworkingPersonModel> getData() {
        return data;
    }

    public void setData(ArrayList<NetworkingPersonModel> data) {
        this.data = data;
    }
}
