package com.dubaiisc.disc.model.sponsors;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.dubaiisc.disc.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;
import java.util.List;

import simplifii.framework.utility.CollectionUtils;
import simplifii.framework.utility.LocalHelperUtility;

public class SponsorModel extends BaseAdapterModel implements Parcelable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("websiteURL")
    @Expose
    private String websiteURL;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("translations")
    @Expose
    private List<Translation> translations = null;

    protected SponsorModel(Parcel in) {
        type = in.readString();
        websiteURL = in.readString();
        imageUrl = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<SponsorModel> CREATOR = new Creator<SponsorModel>() {
        @Override
        public SponsorModel createFromParcel(Parcel in) {
            return new SponsorModel(in);
        }

        @Override
        public SponsorModel[] newArray(int size) {
            return new SponsorModel[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_CODES.SPONSOR;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(websiteURL);
        dest.writeString(imageUrl);
        dest.writeString(name);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    public String getLocaleTranslatedType(Context context){
        if (CollectionUtils.isNotEmpty(this.translations)){
            for (Translation t:this.translations){
                if (!TextUtils.isEmpty(t.getLangCode()) && t.getLangCode().equalsIgnoreCase(LocalHelperUtility.getLanguage())){
                   return t.getTranslatedType();
                }
            }
        }
        return context.getString(R.string.others);
    }
}
