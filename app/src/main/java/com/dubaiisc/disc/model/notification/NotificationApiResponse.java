package com.dubaiisc.disc.model.notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class NotificationApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private NotificationLanguageModel notification;


    public NotificationLanguageModel getNotification() {
        return notification;
    }
}
