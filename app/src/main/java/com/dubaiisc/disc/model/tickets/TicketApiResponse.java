package com.dubaiisc.disc.model.tickets;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TicketApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private List<TicketModel> ticketModelList = null;

    public List<TicketModel> getTicketModelList() {
        return ticketModelList;
    }

    public void setTicketModelList(List<TicketModel> ticketModelList) {
        this.ticketModelList = ticketModelList;
    }
}
