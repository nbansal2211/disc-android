package com.dubaiisc.disc.model.baseApi;

/**
 * Created by chetan on 13/11/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseApiResponse {

    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("code")
    @Expose
    private String statusCode;
    @SerializedName("message")
    @Expose
    private String message;

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
