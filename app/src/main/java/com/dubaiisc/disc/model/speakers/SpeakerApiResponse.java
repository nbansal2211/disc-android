package com.dubaiisc.disc.model.speakers;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpeakerApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private Speaker speaker;


    public Speaker getSpeaker() {
        return speaker;
    }

    public void setSpeaker(Speaker speaker) {
        this.speaker = speaker;
    }
}
