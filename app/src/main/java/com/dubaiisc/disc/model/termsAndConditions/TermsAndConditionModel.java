package com.dubaiisc.disc.model.termsAndConditions;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class TermsAndConditionModel extends BaseAdapterModel {

    @SerializedName("heading")
    @Expose
    private String title;

    @SerializedName("desc")
    @Expose
    private String desc;

    @SerializedName("sub_desc")
    @Expose
    private List<String> subDesc;

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public List<String> getSubDesc() {
        return subDesc;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_CODES.TERMS_CONDITION;
    }
}
