package com.dubaiisc.disc.model;

import java.io.Serializable;

public abstract class BaseAdapterModel implements Serializable {
    public abstract int getViewType();
}
