package com.dubaiisc.disc.model.drawer;

public class DrawerChildData {

    private String title;
    private int eventType;

    public DrawerChildData(String title, int eventType) {
        this.title = title;
        this.eventType = eventType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }
}
