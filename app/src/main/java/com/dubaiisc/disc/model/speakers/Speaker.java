package com.dubaiisc.disc.model.speakers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class Speaker {

    @SerializedName("en")
    @Expose
    private List<SpeakerModel> englishList;

    @SerializedName("ar")
    @Expose
    private List<SpeakerModel> arabicList;

    @SerializedName("fr")
    @Expose
    private List<SpeakerModel> frenchList;

    @SerializedName("es")
    @Expose
    private List<SpeakerModel> spanishList;

    public List<SpeakerModel> getSpeakersList() {
        switch (LocalHelperUtility.getLanguage()){
            case FrameworkConstants.LANGUAGE.AR:
                return getArabicList();
            case FrameworkConstants.LANGUAGE.EN:
                return getEnglishList();
            case FrameworkConstants.LANGUAGE.FR:
                return getFrenchList();
            case FrameworkConstants.LANGUAGE.ES:
                return getSpanishList();
        }
        return getEnglishList();
    }

    public List<SpeakerModel> getEnglishList() {
        return englishList;
    }

    public List<SpeakerModel> getArabicList() {
        return arabicList;
    }

    public List<SpeakerModel> getFrenchList() {
        return frenchList;
    }

    public List<SpeakerModel> getSpanishList() {
        return spanishList;
    }
}
