package com.dubaiisc.disc.model.faqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class FaqsApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private FaqsModel data;

    public FaqsModel getData() {
        return data;
    }

    public void setData(FaqsModel data) {
        this.data = data;
    }
}
