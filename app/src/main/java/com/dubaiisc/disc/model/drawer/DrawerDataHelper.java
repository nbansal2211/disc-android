package com.dubaiisc.disc.model.drawer;

import android.content.Context;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.utilities.AppConstants;

import java.util.ArrayList;
import java.util.List;

import simplifii.framework.utility.Preferences;

public class DrawerDataHelper {

    public List<DrawerMainData> getSlideNavigationDrawerItemList(Context context) {
        List<DrawerMainData> drawerMainDataList = new ArrayList<>();
        if (drawerMainDataList.isEmpty()) {
            drawerMainDataList.add(getHomeData(context));
            drawerMainDataList.add(getConferenceData(context));
            drawerMainDataList.add(getSponsorsData(context));
//            drawerMainDataList.add(getNetworkingData(context));

//            if (Preferences.isUserLoggerIn()) {
//                UserModel userModel = (UserModel) JsonUtil.parseJson(Preferences.getGalleryData(Preferences.USER_DATA_KEY, ""), UserModel.class);
//                if (userModel.isConsentForNetworking())
//                    drawerMainDataList.add(getNetworkingData(context));
//            }
            drawerMainDataList.add(getMediaData(context));
            drawerMainDataList.add(getInfoData(context));
            drawerMainDataList.add(getContactUsData(context));

            if (Preferences.isUserLoggerIn())
                drawerMainDataList.add(getMyProfileData(context));
        }
        return drawerMainDataList;
    }

    private DrawerMainData getHomeData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.home), AppConstants.DRAWER_GROUP_EVENT_TYPE.HOME, null);
        return mainData;
    }

    private DrawerMainData getConferenceData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.conference), AppConstants.DRAWER_GROUP_EVENT_TYPE.CONFERENCE, getConferenceChildData(context));
        return mainData;
    }

    private DrawerMainData getSponsorsData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.sponsors), AppConstants.DRAWER_GROUP_EVENT_TYPE.SPONSOR, null);
        return mainData;
    }

//    private DrawerMainData getNetworkingData(Context context) {
//        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.networking), AppConstants.DRAWER_GROUP_EVENT_TYPE.NETWORKING, null);
//        return mainData;
//    }

    private DrawerMainData getMediaData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.media), AppConstants.DRAWER_GROUP_EVENT_TYPE.MEDIA, null);
        return mainData;
    }

    private DrawerMainData getInfoData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.info), AppConstants.DRAWER_GROUP_EVENT_TYPE.INFO, getInfoChildData(context));
        return mainData;
    }

    private DrawerMainData getContactUsData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.contact_us), AppConstants.DRAWER_GROUP_EVENT_TYPE.CONTACT_US, null);
        return mainData;
    }

    private DrawerMainData getMyProfileData(Context context) {
        DrawerMainData mainData = new DrawerMainData(context.getString(R.string.my_profile), AppConstants.DRAWER_GROUP_EVENT_TYPE.MY_PROFILE, null);
        return mainData;
    }

    private List<DrawerChildData> getConferenceChildData(Context context) {
        List<DrawerChildData> childDataList = new ArrayList<>();
        childDataList.add(new DrawerChildData(context.getString(R.string.about), AppConstants.DRAWER_CHILD_EVENT_TYPE.ABOUT));
        childDataList.add(new DrawerChildData(context.getString(R.string.why_attend), AppConstants.DRAWER_CHILD_EVENT_TYPE.WHY_ATTEND));
        childDataList.add(new DrawerChildData(context.getString(R.string.agenda), AppConstants.DRAWER_CHILD_EVENT_TYPE.AGENDA));
        childDataList.add(new DrawerChildData(context.getString(R.string.speakers), AppConstants.DRAWER_CHILD_EVENT_TYPE.SPEAKERS));
        childDataList.add(new DrawerChildData(context.getString(R.string.gallery), AppConstants.DRAWER_CHILD_EVENT_TYPE.GALLERY));
        childDataList.add(new DrawerChildData(context.getString(R.string.year_in_review), AppConstants.DRAWER_CHILD_EVENT_TYPE.YEARS_IN_REVIEW));
        childDataList.add(new DrawerChildData(context.getString(R.string.how_to_get_there), AppConstants.DRAWER_CHILD_EVENT_TYPE.HOW_TO_GET_THERE));
        return childDataList;
    }

    private List<DrawerChildData> getInfoChildData(Context context) {
        List<DrawerChildData> childDataList = new ArrayList<>();
        childDataList.add(new DrawerChildData(context.getString(R.string.while_in_dubai), AppConstants.DRAWER_CHILD_EVENT_TYPE.WHILE_IN_DUBAI));
        //childDataList.add(new DrawerChildData(context.getString(R.string.faq), AppConstants.DRAWER_CHILD_EVENT_TYPE.FAQS));
        return childDataList;
    }

}
