
package com.dubaiisc.disc.model.gallery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GalleryLang implements Serializable {

    @SerializedName("langCode")
    @Expose
    private String langCode;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("dateDisplayString")
    @Expose
    private String dateDisplayString;
    @SerializedName("attendeesDisplayString")
    @Expose
    private String attendeesDisplayString;
    @SerializedName("speakersDisplayString")
    @Expose
    private String speakersDisplayString;
    @SerializedName("topicsDisplayString")
    @Expose
    private String topicsDisplayString;
    @SerializedName("recommendationsDisplayString")
    @Expose
    private String recommendationsDisplayString;
    @SerializedName("attendeesCount")
    @Expose
    private Integer attendeesCount;
    @SerializedName("speakersCount")
    @Expose
    private Integer speakersCount;
    @SerializedName("topicsCount")
    @Expose
    private Integer topicsCount;
    @SerializedName("recomendationsCount")
    @Expose
    private Integer recomendationsCount;
    @SerializedName("brochureLink")
    @Expose
    private String brochureLink;
    @SerializedName("edition")
    @Expose
    private String edition;
    @SerializedName("editionTranslation")
    @Expose
    private String editionTranslation;
    @SerializedName("orderSequence")
    @Expose
    private Integer orderSequence;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateDisplayString() {
        return dateDisplayString;
    }

    public void setDateDisplayString(String dateDisplayString) {
        this.dateDisplayString = dateDisplayString;
    }

    public String getAttendeesDisplayString() {
        return attendeesDisplayString;
    }

    public void setAttendeesDisplayString(String attendeesDisplayString) {
        this.attendeesDisplayString = attendeesDisplayString;
    }

    public String getSpeakersDisplayString() {
        return speakersDisplayString;
    }

    public void setSpeakersDisplayString(String speakersDisplayString) {
        this.speakersDisplayString = speakersDisplayString;
    }

    public String getTopicsDisplayString() {
        return topicsDisplayString;
    }

    public void setTopicsDisplayString(String topicsDisplayString) {
        this.topicsDisplayString = topicsDisplayString;
    }

    public String getRecommendationsDisplayString() {
        return recommendationsDisplayString;
    }

    public void setRecommendationsDisplayString(String recommendationsDisplayString) {
        this.recommendationsDisplayString = recommendationsDisplayString;
    }

    public Integer getAttendeesCount() {
        return attendeesCount;
    }

    public void setAttendeesCount(Integer attendeesCount) {
        this.attendeesCount = attendeesCount;
    }

    public Integer getSpeakersCount() {
        return speakersCount;
    }

    public void setSpeakersCount(Integer speakersCount) {
        this.speakersCount = speakersCount;
    }

    public Integer getTopicsCount() {
        return topicsCount;
    }

    public void setTopicsCount(Integer topicsCount) {
        this.topicsCount = topicsCount;
    }

    public Integer getRecomendationsCount() {
        return recomendationsCount;
    }

    public void setRecomendationsCount(Integer recomendationsCount) {
        this.recomendationsCount = recomendationsCount;
    }

    public String getBrochureLink() {
        return brochureLink;
    }

    public void setBrochureLink(String brochureLink) {
        this.brochureLink = brochureLink;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getEditionTranslation() {
        return editionTranslation;
    }

    public void setEditionTranslation(String editionTranslation) {
        this.editionTranslation = editionTranslation;
    }

    public Integer getOrderSequence() {
        return orderSequence;
    }

    public void setOrderSequence(Integer orderSequence) {
        this.orderSequence = orderSequence;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
