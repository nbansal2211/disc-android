package com.dubaiisc.disc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class ProfilePictureUploadApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private UrlModel data;

    public UrlModel getData() {
        return data;
    }

    public void setData(UrlModel data) {
        this.data = data;
    }
}
