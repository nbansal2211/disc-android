package com.dubaiisc.disc.model.faqs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class QuesAnsModel extends BaseAdapterModel {

    @SerializedName("ques")
    @Expose
    private String ques;

    @SerializedName("ans")
    @Expose
    private String ans;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    private boolean isExapanded;

    @Override
    public int getViewType() {
        return AppConstants.VIEW_CODES.FAQS;
    }

    public String getQues() {
        return ques;
    }

    public void setQues(String ques) {
        this.ques = ques;
    }

    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isExapanded() {
        return isExapanded;
    }

    public void setExapanded(boolean exapanded) {
        isExapanded = exapanded;
    }
}
