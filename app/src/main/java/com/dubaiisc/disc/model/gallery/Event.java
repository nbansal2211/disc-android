
package com.dubaiisc.disc.model.gallery;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Event implements Serializable {

    @SerializedName("en")
    @Expose
    private GalleryLang en;
    @SerializedName("ar")
    @Expose
    private GalleryLang ar;
    @SerializedName("es")
    @Expose
    private GalleryLang es;
    @SerializedName("fr")
    @Expose
    private GalleryLang fr;

    public GalleryLang getEn() {
        return en;
    }

    public void setEn(GalleryLang en) {
        this.en = en;
    }

    public GalleryLang getAr() {
        return ar;
    }

    public void setAr(GalleryLang ar) {
        this.ar = ar;
    }

    public GalleryLang getEs() {
        return es;
    }

    public void setEs(GalleryLang es) {
        this.es = es;
    }

    public GalleryLang getFr() {
        return fr;
    }

    public void setFr(GalleryLang fr) {
        this.fr = fr;
    }
}
