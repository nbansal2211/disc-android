package com.dubaiisc.disc.model.sponsors;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;

public class SponsorApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private Map<String, List<SponsorModel>> map;

    public Map<String, List<SponsorModel>> getMap() {
        return map;
    }

    public void setMap(Map<String, List<SponsorModel>> map) {
        this.map = map;
    }
}
