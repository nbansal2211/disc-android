package com.dubaiisc.disc.model.yearInReview;

import com.dubaiisc.disc.model.baseApi.BaseApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearsInReviewApiResponse extends BaseApiResponse {

    @SerializedName("data")
    @Expose
    private YearsInReviewLanguage yearsInReview;


    public YearsInReviewLanguage getYearsInReview() {
        return yearsInReview;
    }
}
