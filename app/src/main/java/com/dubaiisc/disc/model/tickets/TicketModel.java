package com.dubaiisc.disc.model.tickets;

import android.os.Parcel;
import android.os.Parcelable;

import com.dubaiisc.disc.model.user.UserModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TicketModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("user")
    @Expose
    private UserModel user;

    @SerializedName("number")
    @Expose
    private String number;

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public TicketModel() {
    }

    private TicketModel(Parcel in) {
        status = in.readString();
        number = in.readString();
        id = in.readLong();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<TicketModel> CREATOR = new Creator<TicketModel>() {
        @Override
        public TicketModel createFromParcel(Parcel in) {
            return new TicketModel(in);
        }

        @Override
        public TicketModel[] newArray(int size) {
            return new TicketModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(number);
        dest.writeLong(id);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
