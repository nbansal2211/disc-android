package com.dubaiisc.disc.model.media;

import com.dubaiisc.disc.model.notification.NotificationModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedHashMap;
import java.util.List;

import simplifii.framework.utility.FrameworkConstants;
import simplifii.framework.utility.LocalHelperUtility;

public class MediaModel {

    @SerializedName("en")
    @Expose
    private LinkedHashMap<String, List<FeedModel>> en;
    @SerializedName("ar")
    @Expose
    private LinkedHashMap<String, List<FeedModel>> ar;

    @SerializedName("fr")
    @Expose
    private LinkedHashMap<String, List<FeedModel>> fr;
    @SerializedName("es")
    @Expose
    private LinkedHashMap<String, List<FeedModel>> es;

    public LinkedHashMap<String, List<FeedModel>> getMap() {
            switch (LocalHelperUtility.getLanguage()){
                case FrameworkConstants.LANGUAGE.AR:
                    return getAr();
                case FrameworkConstants.LANGUAGE.EN:
                    return getEn();
                case FrameworkConstants.LANGUAGE.FR:
                    return getFr();
                case FrameworkConstants.LANGUAGE.ES:
                    return getEs();
            }
            return getEn();
    }

    private LinkedHashMap<String, List<FeedModel>> getEn() {
        return en;
    }

    private LinkedHashMap<String, List<FeedModel>> getAr() {
        return ar;
    }

    public LinkedHashMap<String, List<FeedModel>> getFr() {
        return fr;
    }

    public LinkedHashMap<String, List<FeedModel>> getEs() {
        return es;
    }
}
