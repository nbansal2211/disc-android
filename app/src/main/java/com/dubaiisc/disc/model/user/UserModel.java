package com.dubaiisc.disc.model.user;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.File;

public class UserModel implements Parcelable {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("ticketNumber")
    @Expose
    private String ticketNumber;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("middleName")
    @Expose
    private String middleName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("designation")
    @Expose
    private String designation;

    @SerializedName("organization")
    @Expose
    private String organization;

    @SerializedName("mobileNumberPrecode")
    @Expose
    private String mobileCode;

    @SerializedName("mobileNumber")
    @Expose
    private String mobile;

    @SerializedName("wokPhoneNumberPreCode")
    @Expose
    private String phoneCode;

    @SerializedName("workPhoneNumber")
    @Expose
    private String work;

    @SerializedName("workPhoneNumberExtension")
    @Expose
    private String workExt;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("profilePicLink")
    @Expose
    private String profilePicLink;

    @SerializedName("websiteLink")
    @Expose
    private String websiteLink;

    @SerializedName("facebookLink")
    @Expose
    private String facebookLink;

    @SerializedName("linkedInLink")
    @Expose
    private String linkedInLink;

    @SerializedName("twitterLink")
    @Expose
    private String twitterLink;

    @SerializedName("additionalLink")
    @Expose
    private String additionalLink;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("consentForNetworking")
    @Expose
    private boolean ConsentForNetworking;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;


    private File imageFile;

    public UserModel() {
    }

    protected UserModel(Parcel in) {
        token = in.readString();
        status = in.readString();
        email = in.readString();
        password = in.readString();
        ticketNumber = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        designation = in.readString();
        organization = in.readString();
        mobileCode = in.readString();
        mobile = in.readString();
        phoneCode = in.readString();
        work = in.readString();
        workExt = in.readString();
        city = in.readString();
        country = in.readString();
        profilePicLink = in.readString();
        websiteLink = in.readString();
        facebookLink = in.readString();
        linkedInLink = in.readString();
        twitterLink = in.readString();
        additionalLink = in.readString();
        about = in.readString();
        id = in.readLong();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isConsentForNetworking() {
        return ConsentForNetworking;
    }

    public void setConsentForNetworking(boolean consentForNetworking) {
        ConsentForNetworking = consentForNetworking;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getWorkExt() {
        return workExt;
    }

    public void setWorkExt(String workExt) {
        this.workExt = workExt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String getProfilePicLink() {
        return profilePicLink;
    }

    public void setProfilePicLink(String profilePicLink) {
        this.profilePicLink = profilePicLink;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getLinkedInLink() {
        return linkedInLink;
    }

    public void setLinkedInLink(String linkedInLink) {
        this.linkedInLink = linkedInLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getAdditionalLink() {
        return additionalLink;
    }

    public void setAdditionalLink(String additionalLink) {
        this.additionalLink = additionalLink;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(status);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(ticketNumber);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(designation);
        dest.writeString(organization);
        dest.writeString(mobileCode);
        dest.writeString(mobile);
        dest.writeString(phoneCode);
        dest.writeString(work);
        dest.writeString(workExt);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(profilePicLink);
        dest.writeString(websiteLink);
        dest.writeString(facebookLink);
        dest.writeString(linkedInLink);
        dest.writeString(twitterLink);
        dest.writeString(additionalLink);
        dest.writeString(about);
        dest.writeLong(id);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }
}








