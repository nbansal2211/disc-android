
package com.dubaiisc.disc.model.agenda;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import simplifii.framework.utility.CollectionUtils;

public class Event {

    private boolean isSessionAvailable;

    @SerializedName("sessions")
    @Expose
    private List<SessionModel> sessions = null;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("displayDate")
    @Expose
    private String displayDate;

    @SerializedName("displayDayNumber")
    @Expose
    private String displayDayNumber;

    @SerializedName("id")
    @Expose
    private long id;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;

    public boolean isSessionAvailable() {
        return CollectionUtils.isNotEmpty(sessions);
    }

    public List<SessionModel> getSessions() {
        return sessions;
    }

    public void setSessions(List<SessionModel> sessions) {
        this.sessions = sessions;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDisplayDate() {
        return displayDate;
    }

    public void setDisplayDate(String displayDate) {
        this.displayDate = displayDate;
    }

    public String getDisplayDayNumber() {
        return displayDayNumber;
    }

    public void setDisplayDayNumber(String displayDayNumber) {
        this.displayDayNumber = displayDayNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


}
