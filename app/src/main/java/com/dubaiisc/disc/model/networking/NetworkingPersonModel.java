package com.dubaiisc.disc.model.networking;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.dubaiisc.disc.model.BaseAdapterModel;
import com.dubaiisc.disc.utilities.AppConstants;

public class NetworkingPersonModel extends BaseAdapterModel implements Parcelable, Comparable {

    public static final Creator<NetworkingPersonModel> CREATOR = new Creator<NetworkingPersonModel>() {
        @Override
        public NetworkingPersonModel createFromParcel(Parcel in) {
            return new NetworkingPersonModel(in);
        }

        @Override
        public NetworkingPersonModel[] newArray(int size) {
            return new NetworkingPersonModel[size];
        }
    };
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("middleName")
    @Expose
    private String middleNamee;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("organization")
    @Expose
    private String organization;
    @SerializedName("mobileNumber")
    @Expose
    private String mobile;
    @SerializedName("workPhoneNumber")
    @Expose
    private String work;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("profilePicLink")
    @Expose
    private String profilePicLink;
    @SerializedName("websiteLink")
    @Expose
    private String websiteLink;
    @SerializedName("facebookLink")
    @Expose
    private String facebookLink;
    @SerializedName("linkedInLink")
    @Expose
    private String linkedInLink;
    @SerializedName("twitterLink")
    @Expose
    private String twitterLink;
    @SerializedName("additionalLink")
    @Expose
    private String additionalLink;
    @SerializedName("about")
    @Expose
    private String about;
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    private Uri profilePicUri;

    protected NetworkingPersonModel(Parcel in) {
        token = in.readString();
        status = in.readString();
        email = in.readString();
        password = in.readString();
        ticketNumber = in.readString();
        firstName = in.readString();
        middleNamee = in.readString();
        lastName = in.readString();
        designation = in.readString();
        organization = in.readString();
        mobile = in.readString();
        work = in.readString();
        city = in.readString();
        country = in.readString();
        profilePicLink = in.readString();
        websiteLink = in.readString();
        facebookLink = in.readString();
        linkedInLink = in.readString();
        twitterLink = in.readString();
        additionalLink = in.readString();
        about = in.readString();
        id = in.readLong();
        createdAt = in.readString();
        updatedAt = in.readString();
        profilePicUri = in.readParcelable(Uri.class.getClassLoader());
    }

    public Uri getProfilePicUri() {
        return profilePicUri;
    }

    public void setProfilePicUri(Uri profilePicUri) {
        this.profilePicUri = profilePicUri;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleNamee() {
        return middleNamee;
    }

    public void setMiddleNamee(String middleNamee) {
        this.middleNamee = middleNamee;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String getProfilePicLink() {
        return profilePicLink;
    }

    public void setProfilePicLink(String profilePicLink) {
        this.profilePicLink = profilePicLink;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getLinkedInLink() {
        return linkedInLink;
    }

    public void setLinkedInLink(String linkedInLink) {
        this.linkedInLink = linkedInLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getAdditionalLink() {
        return additionalLink;
    }

    public void setAdditionalLink(String additionalLink) {
        this.additionalLink = additionalLink;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_CODES.NETWORKING_PERSON;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(token);
        dest.writeString(status);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(ticketNumber);
        dest.writeString(firstName);
        dest.writeString(middleNamee);
        dest.writeString(lastName);
        dest.writeString(designation);
        dest.writeString(organization);
        dest.writeString(mobile);
        dest.writeString(work);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(profilePicLink);
        dest.writeString(websiteLink);
        dest.writeString(facebookLink);
        dest.writeString(linkedInLink);
        dest.writeString(twitterLink);
        dest.writeString(additionalLink);
        dest.writeString(about);
        dest.writeLong(id);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeParcelable(profilePicUri, flags);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return this.getFirstName().compareTo(((NetworkingPersonModel) o).getFirstName());
    }
}
