package com.dubaiisc.disc.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.dubaiisc.disc.utilities.AppConstants;

public class SectionHeaderModel extends BaseAdapterModel implements Parcelable {

    private String title;

    public SectionHeaderModel(String title) {
        this.title = title;
    }

    protected SectionHeaderModel(Parcel in) {
        title = in.readString();
    }

    public static final Creator<SectionHeaderModel> CREATOR = new Creator<SectionHeaderModel>() {
        @Override
        public SectionHeaderModel createFromParcel(Parcel in) {
            return new SectionHeaderModel(in);
        }

        @Override
        public SectionHeaderModel[] newArray(int size) {
            return new SectionHeaderModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int getViewType() {
        return AppConstants.VIEW_CODES.SECTION_HEADER;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
    }
}
