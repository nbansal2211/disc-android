package com.dubaiisc.disc.utilities;

/**
 * Created by chetan on 11/12/17.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.dubaiisc.disc.R;
import com.dubaiisc.disc.activities.SplashScreenActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

// ariq dpDQFChmicg:APA91bFhi1V8quTgs0XVPpVilP6Xh1UoFtT00795jb0p6YiFPOcyaHwzwcwGvyhFe_yCLY8dMAOyEqs3G_9fBta_XOuPBWX7jKnJPgfzCos6cSEJgJbSnjUCXqDq1frpbBKzLYqMMk42
//rajnikant ev4rv3cf3kk:APA91bFUl8LYVby6xhZoXyuS08Y0DJ15oRaDqKmHZO6_GosIfz0XZPbIfS5bit1WwMpxwGpxuKRba2QzIuSR9ccsSt1R1ZYx-hQJKUX6mXJIw33pIZgwmYKQgSdXXWg9xb0VEYqNvHI5

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String channelId="1";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. GalleryData messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. GalleryData messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            sendNotification(remoteMessage.getData());
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
//        Job myJob = dispatcher.newJobBuilder()
//                .setService(MyJobService.class)
//                .setTag("my-job-tag")
//                .build();
//        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }
    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(Map<String, String> messageBody) {
        Bundle b = new Bundle();

        for(Map.Entry<String, String> entry: messageBody.entrySet()){
            b.putString(entry.getKey(), entry.getValue());
        }
//        b.putBoolean(AppConstants.BUNDLE_KEYS.IS_FROM_PUSH, true);
        Intent intent = new Intent(this, SplashScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtras(b);
        int notificationId = (int)System.currentTimeMillis()/1000;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notificationId /* Request code*/ , intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            channelId = context.getString(R.string.default_notification_channel_id);
            NotificationChannel channel = new NotificationChannel(channelId,   messageBody.get("title"), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(messageBody.get("message"));
            notificationManager.createNotificationChannel(channel);
            notificationManager.notify(1 /* ID of notification*/ , getBuilder(messageBody.get("title"), messageBody.get("message"), pendingIntent).setChannelId(channelId).build());
        }else {
            notificationManager.notify(notificationId /* ID of notification*/ , getBuilder(messageBody.get("title"), messageBody.get("message"), pendingIntent).build());
        }
    }

    private Notification.Builder getBuilder(String title , String message, PendingIntent pendingIntent){
        final Notification.Builder builder = new Notification.Builder(this);
        builder.setStyle(new Notification.BigTextStyle(builder)
                .bigText(message)
                .setBigContentTitle(title)
                .setSummaryText(message))
                .setContentTitle(title)
                .setContentText(message).setContentIntent(pendingIntent).setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);
        return builder;
    }
}
