package com.dubaiisc.disc.utilities;

public interface AppConstants {

    interface DRAWER_GROUP_EVENT_TYPE {
        int HOME = 0;
        int CONFERENCE = 1;
        int SPONSOR = 2;
        int NETWORKING = 3;
        int MEDIA = 4;
        int INFO = 5;
        int CONTACT_US = 6;
        int MY_PROFILE = 7;
    }

    interface DRAWER_CHILD_EVENT_TYPE {
        int ABOUT = 0;
        int WHY_ATTEND = 1;
        int AGENDA = 2;
        int SPEAKERS = 3;
        int YEARS_IN_REVIEW = 4;
        int HOW_TO_GET_THERE = 5;
        int WHILE_IN_DUBAI = 6;
        int FAQS = 7;
        int GALLERY = 8;
    }


    interface VIEW_CODES {
        int SECTION_HEADER = 1;
        int SPEAKER = 2;
        int SPONSOR = 3;
        int AGENDA_SESSION_TIMELINE = 4;
        int NETWORKING_PERSON = 5;
        int NOTIFICATION = 6;
        int NEWS = 7;
        int FEEDS = 8;
        int FAQS = 9;
        int COUNTRY_ISD = 10;
        int COUNTRY_NAME = 11;
        int YEAR_IN_REVIEW = 12;
        int GENDER_TYPE = 13;
        int TERMS_CONDITION = 14;
        int GALLERY_YEAR_REVIEW = 15;
        int GALLERY_IMAGE = 16;
        int GALLERY_IMAGE_DIALOG = 17;
    }

    interface ACTION_TYPE {
        int SELECT_SPEAKER = 1;
        int SELECT_PERSON = 2;
        int SHARE_NEWS = 3;
        int SELECT_MEDIA = 4;
        int FAQS = 5;
        int SELECT_COUNTRY_ISD = 6;
        int SELECT_COUNTRY_NAME = 7;
        int SELECT_GENDER = 8;
        int SELECT_AGENDA_SPEAKER_FEED = 9;
        int SELECT_AGENDA_MODERATOR_FEED = 10;
        int SELECT_SPONSOR = 11;
        int GALLERY_ACTION = 12;
        int GALLERY_IMAGE_ACTION = 13;
    }

    interface STRING_CONST {
        // TODO Change API key
        String KEY = "AIzaSyB_myAbod5IgtG9VYfvh4f4N239TfGjN5I";
        String SEMINAR = "SEMINAR";
        String OTHERS = "OTHERS";
        String GOLD = "Gold";
        String PLATINUM = "Platinum";
        String SILVER = "Silver";
        String latitude = "25.134347";
        String longitude = "55.1871138";

        String ALLOTTED = "ALLOTED";
        String ADDED = "ADDED";
        String NEWS = "NEWS_PR";
        String FACEBOOK = "FB";
        String TWITTER = "TWITTER";
        String INSTAGRAM = "INSTAGRAM";
        String LINKEDIN = "LINKED_IN";
        String GENERAL = "GENERAL";
        String REGISTRATION = "REGISTRATION";
        String FILE_UPLOAD_KEY = "files";
        String COUNTRY_CODE_MOBILE = "COUNTRY_CODE_MOBILE";
        String COUNTRY_CODE_WORK = "COUNTRY_CODE_WORK";
        String NAME = "name";
        String COUNTRY = "country";
        String DESIGNATION = "designation";
        String ORGANIZATION = "organization";
        String GENDER = "gender";
        String FCM_TOKEN = "fcm_token";
        String TITLE = "title";
        String BODY = "body";
        String MESSAGE = "message";
        String DEVICE_INFO_UPLOADED = "device_info_uploaded";
        String UNREAD_NOTIFICATION_COUNT = "unread_notification_count";
        String TYPE_SPEAKERS = "type_speakers";
        String TYPE_MODERATOR = "type_moderator";
    }

    interface FRAGMENT_TYPE {
        int YEARS_IN_REVIEW = 0;
        int TERMS_AND_CONDITIONS = 1;
        int AGENDA_DETAILS = 2;
    }

    interface REQUEST_TYPE {
        int SELECT_PROFILE_IMAGE = 1;
    }

    interface EXTRAS {
        String URL_TICKET_PURCHASE = "https://www.ticketmaster.ae/artist/dubai-international-sports-conference-tickets/1006467?int_cmp_name=Dubai-International-Sports-Conference&int_cmp_id=AE-Home-909&int_cmp_creative=Home-main-3&tm_link=tm_ccp_Home_main_Dubai-International-Sports-Conference";
        String REGISTER_NOW = "http://bit.ly/35ywp7c";
    }
}
