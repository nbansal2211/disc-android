package com.dubaiisc.disc.utilities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.util.HashMap;

public class ThumbnailExtractAsyncNewTask extends AsyncTask<String, Void, Bitmap> {

    private final String videoUrl;
    private final ImageView mThumbnail;
    private final boolean mIsVideo;
    private MediaMetadataRetriever mmr;

    public ThumbnailExtractAsyncNewTask(String videoUrl, ImageView mThumbnail, boolean mIsVideo) {
        this.videoUrl = videoUrl;
        this.mThumbnail = mThumbnail;
        this.mIsVideo = mIsVideo;
        if (mIsVideo) {
            mmr = new MediaMetadataRetriever();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            if (!mIsVideo) {
                return getBitmap(videoUrl);
            } else {
                mmr.setDataSource(this.videoUrl, new HashMap<String, String>());
                return mmr.getFrameAtTime(5000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
            }
        } catch (Exception e) {
            System.out.printf(e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap thumb) {
        if (thumb != null) {
            mThumbnail.setImageBitmap(thumb);
        }
    }

    private Bitmap getBitmap(String fileUrl) {
        mmr.setDataSource(fileUrl);
        byte[] data = mmr.getEmbeddedPicture();
        Bitmap bitmap = null;
        // convert the byte array to a bitmap
        if (data != null) {
            bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

        }
        return bitmap;
    }
}
