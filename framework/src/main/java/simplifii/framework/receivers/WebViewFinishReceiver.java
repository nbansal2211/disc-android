package simplifii.framework.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

/**
 * Created by nitin on 04/12/15.
 */
public class WebViewFinishReceiver extends BroadcastReceiver {

    public ArrayList<WebViewFinishedListner> listeners = new ArrayList<>();
    private WebViewFinishedListner listener;
    public static final String ACTION_WEBVIEW_FINISHED = "WEBVIEW_FINISHED";

    public WebViewFinishReceiver(WebViewFinishedListner listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION_WEBVIEW_FINISHED)) {
            if(listener != null){
                listener.onReceive(intent);
            }
        }
    }

    public void removeListener(WebViewFinishedListner listener){
        this.listener = null;
    }

    public static interface WebViewFinishedListner {
        public void onReceive(Intent intent);
    }

    public static void sendBroadcast(Context context) {
        Intent intent = new Intent(ACTION_WEBVIEW_FINISHED);
        context.sendBroadcast(intent);
    }
}
