package simplifii.framework.asyncmanager;

import android.content.Context;

import simplifii.framework.utility.FrameworkConstants;


public class ServiceFactory {

    public static Service getInstance(Context context, int taskCode) {
        Service service = null;
        switch (taskCode) {
            case FrameworkConstants.TASK_CODES.UPLOAD_IMAGE:
            case FrameworkConstants.TASK_CODES.USER_UPLOAD_FILE:
            case FrameworkConstants.TASK_CODES.PROFILE_PIC_UPLOAD:
                service = new FileUploadService();
                break;
            default:
                //service = new HttpRestService();
                service = new OKHttpService();
                break;

        }
        return service;
    }

}
