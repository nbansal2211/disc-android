package simplifii.framework.ListAdapters;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public interface CustomExpListInterface<C> {
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View v, ViewGroup parent) ;
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent);
    public List<C> getChildList(int groupPosition);
}
