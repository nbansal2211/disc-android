package simplifii.framework.ListAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;


public class CustomListAdapter<T> extends ArrayAdapter<T> {
    private List<T> list;
    private int rowLayoutId;
    private LayoutInflater inflater;
    private CustomListAdapterInterface ref;

    public CustomListAdapter(Context context, int resource, List<T> objects, CustomListAdapterInterface ref) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        this.list = objects;
        this.ref = ref;
        this.rowLayoutId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return ref.getView(position, convertView, parent, rowLayoutId, inflater);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return ref.getView(position, convertView, parent, rowLayoutId, inflater);
    }
}
