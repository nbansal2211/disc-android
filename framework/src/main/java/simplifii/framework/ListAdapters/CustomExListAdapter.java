package simplifii.framework.ListAdapters;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import java.util.List;

import simplifii.framework.utility.CollectionUtils;

public class CustomExListAdapter<T, C> extends BaseExpandableListAdapter {

    private List<T> listDataHeader;
    private CustomExpListInterface<C> expListInterface;

    public CustomExListAdapter(List<T> listDataHeader, CustomExpListInterface<C> expListInterface) {
        this.listDataHeader = listDataHeader;
        this.expListInterface = expListInterface;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (CollectionUtils.isNotEmpty(getChildList(groupPosition))){
            return getChildList(groupPosition).get(childPosition);
        }
        return null;
    }

    public List<C> getChildList(int groupPosition){
        return this.expListInterface.getChildList(groupPosition);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View v, ViewGroup parent) {
        return this.expListInterface.getChildView(groupPosition, childPosition, isLastChild, v, parent);
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount = 0;
        if (CollectionUtils.isNotEmpty(getChildList(groupPosition))) {
            childCount = getChildList(groupPosition).size();
        }
        Log.d("ConfirmationAdapter", childCount+": ChildCount");
        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("ResourceType")
    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded, View convertView, ViewGroup parent) {
        return this.expListInterface.getGroupView(groupPosition, isExpanded, convertView, parent);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


}
