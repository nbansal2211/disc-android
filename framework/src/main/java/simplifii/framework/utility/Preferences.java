package simplifii.framework.utility;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;


/**
 * Created by nitin on 24/08/15.
 */
public class Preferences {

    private static final String PREF_NAME = "Pioneer_Prefs";
    public static final String LOGIN_KEY = "isUserLoggedIn";
    public static final String USER_DATA_KEY = "userData";
    public static final String PHYSICIAN_DATA = "data";
    public static final String SAVE_USER_DEVICE_INFO = "saveUserDeviceInfo";
    public static final String CHECKBOX_SHOWCASE = "checkBocShowcase";
    private static final String FIRST_TIME = "first_time";

    private static SharedPreferences xebiaSharedPrefs;
    private static SharedPreferences.Editor editor;
    private static Preferences sharedPreferenceUtil;

    public static void initSharedPreferences(Context context) {
        sharedPreferenceUtil = new Preferences();
        sharedPreferenceUtil.xebiaSharedPrefs = context.getSharedPreferences(
                PREF_NAME, Activity.MODE_PRIVATE);
        sharedPreferenceUtil.editor = sharedPreferenceUtil.xebiaSharedPrefs
                .edit();
    }

    public static Preferences getInstance() {

        return sharedPreferenceUtil;
    }

    private Preferences() {
        // TODO Auto-generated constructor stub
    }

    public static synchronized boolean saveData(String key, String value) {
        editor.putString(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, Set<String> value) {
        editor.putStringSet(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, boolean value) {
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, long value) {
        editor.putLong(key, value);
        return editor.commit();
    }


    public static synchronized boolean saveData(String key, float value) {
        editor.putFloat(key, value);
        return editor.commit();
    }

    public static synchronized boolean saveData(String key, int value) {
        editor.putInt(key, value);
        return editor.commit();
    }

    public static boolean isFirstTimeUser() {
        return getData(FIRST_TIME, true);
    }

    public static void setFirstTimeUser(boolean status) {
        saveData(FIRST_TIME, status);
    }

    public static boolean isUserLoggerIn() {
        return getData(LOGIN_KEY, false);
    }

    public static boolean isUserDeviceInfoSaveed() {
        return getData(SAVE_USER_DEVICE_INFO, false);
    }

    public static boolean isWhisListSelected() {
        return getData(CHECKBOX_SHOWCASE, false);
    }

    /*
     * public synchronized boolean saveData(String key, Set<String> value) {
     * //editor.putStringSet(key, value); return editor.commit(); }
     */

    public static synchronized boolean removeData(String key) {
        editor.remove(key);
        return editor.commit();
    }

    public static synchronized Boolean getData(String key, boolean defaultValue) {
        return xebiaSharedPrefs.getBoolean(key, defaultValue);
    }

    public static synchronized String getData(String key, String defaultValue) {
        return xebiaSharedPrefs.getString(key, defaultValue);
    }

    public static synchronized Set<String> getData(String key, Set<String> defaultValue) {
        return xebiaSharedPrefs.getStringSet(key, defaultValue);
    }

    public static synchronized float getData(String key, float defaultValue) {

        return xebiaSharedPrefs.getFloat(key, defaultValue);
    }

    public static synchronized int getData(String key, int defaultValue) {
        return xebiaSharedPrefs.getInt(key, defaultValue);
    }

    public static synchronized long getData(String key, long defaultValue) {
        return xebiaSharedPrefs.getLong(key, defaultValue);
    }

    /*
     * public synchronized Set<String> getData(String key, Set<String> defValue)
     * {
     *
     * // return naukriSharedPreferences.getStringSet(key, defValue); return
     * null; }
     */

    public static synchronized void deleteAllData() {

        sharedPreferenceUtil = null;
        editor.clear();
        editor.commit();
    }

    public static double getLat() {
        return Double.parseDouble(Preferences.getData(FrameworkConstants.PARAMS.LAT, "0.0"));
    }

    public static double getLng() {
        return Double.parseDouble(Preferences.getData(FrameworkConstants.PARAMS.LAT, "0.0"));
    }


//    public static String getAppLink() {
//        String link = getData(FrameworkConstants.PREF_KEYS.APP_LINK, null);
//        if (TextUtils.isEmpty(link)) {
//            link = FrameworkConstants.APP_LINK;
//        }
//        return link;
//    }


}
