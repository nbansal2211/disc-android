package simplifii.framework.utility.notificationUtility;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

/**
 * @author Ajay Rawat
 * @version 0.1(Beta) still in development, have to add lots of other features.
 * <p>
 * I create this class to reduce the over-head of creating notification in android
 * @see EasyNotificationBuilder class is builder class for EasyNotification
 */
public class EasyNotification {

    private EasyNotificationBuilder builder;

    private EasyNotification(EasyNotificationBuilder builder) {
        this.builder = builder;
    }

    private Notification buildNotification() {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(builder.context, builder.channelId)
                .setSmallIcon(builder.smallIcon)
                .setContentTitle(builder.title)
                .setContentText(builder.content)
                .setAutoCancel(builder.isAutoCancel)
                .setContentIntent(builder.pendingIntent);
        if (builder.soundUri != null)
            notificationBuilder = notificationBuilder.setSound(builder.soundUri);

        return notificationBuilder.build();
    }

    private Notification buildBigTextNotification() {
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(builder.context, builder.channelId);
        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle(notificationBuilder).bigText(builder.bigMessage).setBigContentTitle(builder.title).setSummaryText(builder.bigMessage))
                .setSmallIcon(builder.smallIcon)
                .setContentTitle(builder.title)
                .setContentText(builder.content)
                .setAutoCancel(builder.isAutoCancel)
                .setContentIntent(builder.pendingIntent);
        if (builder.soundUri != null)
            notificationBuilder = notificationBuilder.setSound(builder.soundUri);

        return notificationBuilder.build();
    }

    public void sendNotification() {
        NotificationManager notificationManager = (NotificationManager) builder.context.getSystemService(Context.NOTIFICATION_SERVICE);
//         Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(builder.channelId, TextUtils.isEmpty(builder.channelName) ? "Channel" : builder.channelName, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(builder.notificationId, buildNotification());
    }

    public void sendBigTextNotification() {
        NotificationManager notificationManager = (NotificationManager) builder.context.getSystemService(Context.NOTIFICATION_SERVICE);
//         Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(builder.channelId, TextUtils.isEmpty(builder.channelName) ? "Channel" : builder.channelName, NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(builder.notificationId, buildBigTextNotification());
    }

    /**
     * This class is builder class for EaseNotification
     */
    public static class EasyNotificationBuilder {
        private Context context;
        private PendingIntent pendingIntent;

        private int notificationId;
        private String channelId;
        private String channelName;
        private String title;
        private String content;
        private String bigMessage;
        private boolean isAutoCancel; //optional

        private int smallIcon;
        private Uri soundUri; //optional

        public EasyNotificationBuilder(Context context, PendingIntent pendingIntent) {
            this.context = context;
            this.pendingIntent = pendingIntent;
        }

        public EasyNotificationBuilder setNotificationId(int notificationId) {
            this.notificationId = notificationId;
            return this;
        }

        public EasyNotificationBuilder setChannelId(String channelId) {
            this.channelId = channelId;
            return this;
        }

        public EasyNotificationBuilder setChannelName(String channelName) {
            this.channelName = channelName;
            return this;
        }

        public EasyNotificationBuilder setTitle(String title) {
            this.title = title;
            return this;
        }

        public EasyNotificationBuilder setContent(String content) {
            this.content = content;
            return this;
        }

        public EasyNotificationBuilder setBigMessage(String bigMessage) {
            this.bigMessage = bigMessage;
            return this;
        }

        public EasyNotificationBuilder setAutoCancel(boolean autoCancel) {
            isAutoCancel = autoCancel;
            return this;
        }

        public EasyNotificationBuilder setSmallIcon(int smallIcon) {
            this.smallIcon = smallIcon;
            return this;
        }

        public EasyNotificationBuilder setSoundUri(Uri soundUri) {
            this.soundUri = soundUri;
            return this;
        }

        public EasyNotification build() {
            return new EasyNotification(this);
        }

    }
}
