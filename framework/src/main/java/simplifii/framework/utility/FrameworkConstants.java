package simplifii.framework.utility;

public interface FrameworkConstants {

    String DEF_REGULAR_FONT = "OpenSans-Regular.ttf";
    String APP_LINK = "https://drive.google.com/file/d/0B8wKJnD6sONHeXlUbm5pOTk4dGM/view?usp=sharing";
    String USER_TOKEN = "userToken";
    String URL_SHORTNER_KEY = "AIzaSyBaV9FY4E6P2s_aPzSqQv5QBnJ6sIojWQQ";


    interface DEVICE_TYPE {
        String ANDROID = "Android";
        String IOS = "Ios";
    }


    interface PARAMS {
        String LAT = "latitude";
        String LNG = "longitude";
    }

    interface VALIDATIONS {
        String EMPTY = "empty";
        String EMAIL = "email";
        String MOBILE = "mobile";
    }

    interface ERROR_CODES {
        int UNKNOWN_ERROR = 0;
        int NO_INTERNET_ERROR = 1;
        int NETWORK_SLOW_ERROR = 2;
        int URL_INVALID = 3;
        int DEVELOPMENT_ERROR = 4;
        String DATA_NOT_FOUND = "NOT_FOUND";
    }


    interface BUNDLE_KEYS {
        String FRAGMENT_TYPE = "FRAGMENT_TYPE";
        String EXTRA_BUNDLE = "bundle";
        String EXTRA_MODEL = "model";
        String TITLE = "title";
        String FEED = "feed";
        String NETWORK_MODEL = "network_model";
        String COUNTRY_LIST = "country_list";
        String MODEL_TYPE = "model_type";
        String EXTRA_VALUE = "extra_value";
        String MEDIA_PATH = "mediaPath";
        String POSITION = "position";
        String GALLERY_ITEM = "gallery_item";
    }

    interface TASK_CODES {
        int UPLOAD_IMAGE = 1;
        int USER_UPLOAD_FILE = 2;
        int PROGRESS_BAR = 3;
        int REGISTRATION = 4;
        int SIGN_IN = 5;
        int FORGET_PASSWORD = 6;
        int PROFILE = 7;
        int EVENT_DAYS = 8;
        int SPEAKERS = 9;
        int SPONSOR = 10;
        int TICKET_VERIFICATION = 11;
        int FAQS = 12;
        int MEDIA = 13;
        int COUNTRY_LIST = 14;
        int PROFILE_UPDATE = 15;
        int USER_LIST = 16;
        int PROFILE_PIC_UPLOAD = 17;
        int NETWORK_SEARCH = 18;
        int NOTIFICATIONS = 19;
        int YEAR_IN_REVIEW = 20;
        int DEVICE_INFO = 21;
        int EVENT_DAYS_BY_ID = 22;
        int SEND_USER_DEVICE_TOKEN = 23;
        int GALLERY = 24;
    }

    interface MEDIA_TYPES {
        String IMAGE = "img";
        String AUDIO = "audio";
        String VIDEO = "video";
        String GIF = "gif";
    }

    interface API_STATUS_CODE {
        String OK = "Ok";
        String TICKET_NOT_FOUND = "NF_TKT";
    }

    interface SHARED_PREF_KEYS {
        String USER_DATA = "user_data";
        String IS_USER_SIGN_IN = "is_user_sign_in";
    }

    interface LANGUAGE {
        String EN = "en";
        String AR = "ar";
        String FR = "fr";
        String ES = "es";
    }

}
